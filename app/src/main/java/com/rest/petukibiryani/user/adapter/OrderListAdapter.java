package com.rest.petukibiryani.user.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

import com.rest.petukibiryani.user.R;
import com.rest.petukibiryani.user.activity.FeedBackActivity;
import com.rest.petukibiryani.user.activity.OrderDetails;
import com.rest.petukibiryani.user.helper.AppConstants;
import com.rest.petukibiryani.user.responseModel.OrderListResponse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.OrderViewHolder> {

    private Context context;
    private List<OrderListResponse.Datum> orderListResponseslist;
    private Date date;


    public OrderListAdapter(Context context, List<OrderListResponse.Datum> orderListResponseArrayList) {
        this.context = context;
        this.orderListResponseslist = orderListResponseArrayList;
    }

    @NonNull
    @Override
    public OrderViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.order_item_layout, viewGroup, false);
        return new OrderViewHolder(layoutView);
    }

    @SuppressLint("SimpleDateFormat")
    @Override
    public void onBindViewHolder(@NonNull OrderViewHolder holder, int postion) {

        holder.tv_order_no.setText(orderListResponseslist.get(postion).getOrderNo());
        holder.tv_bill_amount.setText(("₹ " + orderListResponseslist.get(postion).getGrandTotal()));
        holder.tv_delivery_adds.setText(orderListResponseslist.get(postion).getDeliveryAddress());

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            date = dateFormat.parse(orderListResponseslist.get(postion).getCreatedOn());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("KK:mm a, dd MMM yyyy");
        holder.tv_order_placed.setText(simpleDateFormat.format(date));
        if (orderListResponseslist.get(postion).getOrderStatus().equals("4")) {
            if (orderListResponseslist.get(postion).getFeedbackId() == null) {
                holder.bt_feedback.setVisibility(View.VISIBLE);
                holder.ratingBar.setVisibility(View.GONE);
            } else {
                holder.bt_feedback.setVisibility(View.GONE);
                holder.ratingBar.setVisibility(View.VISIBLE);
                holder.ratingBar.setRating(Float.parseFloat(orderListResponseslist.get(postion).getFeedbackRating()));
            }
        } else {
            holder.bt_feedback.setVisibility(View.INVISIBLE);
            holder.ratingBar.setVisibility(View.GONE);
        }

        holder.bt_order_details.setOnClickListener(v -> {
            Intent intent = new Intent(context, OrderDetails.class);
            intent.putExtra(AppConstants.ORDER_NO, orderListResponseslist.get(postion).getOrderNo());
            intent.putExtra(AppConstants.ORDER_ID, orderListResponseslist.get(postion).getId());
            context.startActivity(intent);
        });

        holder.bt_feedback.setOnClickListener(v -> {
            Intent intent = new Intent(context, FeedBackActivity.class);
            intent.putExtra(AppConstants.ORDER_NO, orderListResponseslist.get(postion).getOrderNo());
            intent.putExtra(AppConstants.ORDER_ID, orderListResponseslist.get(postion).getId());
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return orderListResponseslist.size();
    }

    class OrderViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_order_no)
        TextView tv_order_no;
        @BindView(R.id.tv_order_placed)
        TextView tv_order_placed;
        @BindView(R.id.tv_bill_amount)
        TextView tv_bill_amount;
        @BindView(R.id.tv_delivery_adds)
        TextView tv_delivery_adds;
        @BindView(R.id.bt_order_details)
        Button bt_order_details;
        @BindView(R.id.bt_feedback)
        Button bt_feedback;
        @BindView(R.id.ratingbar)
        RatingBar ratingBar;

        OrderViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
