package com.rest.petukibiryani.user.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;

import android.widget.TextView;
import android.widget.Toast;

import com.rest.petukibiryani.user.R;
import com.rest.petukibiryani.user.activity.MenuActivity;
import com.rest.petukibiryani.user.helper.AppDatabases;
import com.rest.petukibiryani.user.listeners.OrderItemsDAO;
import com.rest.petukibiryani.user.models.OrderItems;
import com.rest.petukibiryani.user.network.ServiceGenerator;
import com.rest.petukibiryani.user.responseModel.OffersResponse;
import com.rest.petukibiryani.user.responseModel.TabMenuResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TabMenuAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<TabMenuResponse.Datum> tabMenuItems;
    private ArrayList<OffersResponse.Datum> offerItems;
    private int tabPosition;
    private OrderItemsDAO orderItemsDAO;
    private int height, weight;

    public TabMenuAdapter(Context context, ArrayList<TabMenuResponse.Datum> tabMenuItems, ArrayList<OffersResponse.Datum> offerItems, int tabPosition, int height, int weight) {
        this.context = context;
        this.tabMenuItems = tabMenuItems;
        this.offerItems = offerItems;
        this.tabPosition = tabPosition;
        this.height = height;
        this.weight = weight;
        orderItemsDAO = AppDatabases.getOrderItemsDAO(context);
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.tab_menu_layout, viewGroup, false);
        if (tabPosition == 0) {
            return new OffersHolder(view);
        } else {
            return new TabMenuHolder(view);

        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (tabPosition == 0) {
            ((OffersHolder) viewHolder).offerOptions(position);

        } else {
            ((TabMenuHolder) viewHolder).tabOptions(position);
        }
    }

    @Override
    public int getItemCount() {
        if (tabPosition == 0)
            return offerItems.size();
        else
            return tabMenuItems.size();
    }

    class TabMenuHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_image)
        ImageView item_image;
        @BindView(R.id.item_icon)
        ImageView item_icon_image;
        @BindView(R.id.iv_add)
        ImageView iv_add;
        @BindView(R.id.iv_minus)
        ImageView iv_minus;
        @BindView(R.id.item_name)
        TextView item_name;
        @BindView(R.id.item_price_half)
        TextView item_half_price;
        @BindView(R.id.item_price_full)
        TextView item_full_price;
        @BindView(R.id.item_price_extra)
        TextView item_extra_price;
        @BindView(R.id.item_description)
        TextView item_description;
        @BindView(R.id.counter)
        TextView tv_counter;
        @BindView(R.id.RL_slashed)
        FrameLayout Rl_slashed;
        @BindView(R.id.add_button)
        Button add_button;
        @BindView(R.id.add_counter)
        LinearLayout counterLL;
        @BindView(R.id.regularLL)
        LinearLayout regularLL;

        TabMenuHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void tabOptions(int position) {
            //zero=true;
            counterLL.setVisibility(View.GONE);
            add_button.setVisibility(View.VISIBLE);
            regularLL.setVisibility(View.VISIBLE);
            item_description.setText(tabMenuItems.get(position).getDescription());
            item_name.setText(tabMenuItems.get(position).getSubCategoryName());
            if (tabMenuItems.get(position).getCategoryName().equals("Combo")) {
                item_image.getLayoutParams().height = height;
                item_image.getLayoutParams().width = weight;
                item_image.requestLayout();
                Rl_slashed.setVisibility(View.VISIBLE);
                item_full_price.setVisibility(View.GONE);
                item_extra_price.setText(tabMenuItems.get(position).getExtraPrice());
                item_half_price.setText(tabMenuItems.get(position).getBasePrice());
                item_half_price.setTextColor(ContextCompat.getColor(context, R.color.color_brown));

            } else if (tabMenuItems.get(position).getIsVariant().equals("0")) {
                item_full_price.setText(("₹  " + tabMenuItems.get(position).getBasePrice()));
                item_half_price.setVisibility(View.INVISIBLE);
                item_full_price.setVisibility(View.VISIBLE);
            } else {
                item_half_price.setText(("Half - ₹ " + tabMenuItems.get(position).getHalfPrice()));
                item_full_price.setText(("Full - ₹ " + tabMenuItems.get(position).getFullPrice()));
                item_half_price.setVisibility(View.VISIBLE);
                item_full_price.setVisibility(View.VISIBLE);
            }

            if(tabMenuItems.get(position).getIsOutOfStock().equals("0"))
                Picasso.with(context)
                    .load(ServiceGenerator.BASE_URL + tabMenuItems.get(position).getFile())
                    .into(item_image);
            else{
                item_image.setImageResource(R.drawable.out_of_stock);
                add_button.setVisibility(View.GONE);
                counterLL.setVisibility(View.GONE);
                regularLL.setBackgroundColor(context.getResources().getColor(R.color.color_gry));

                List<OrderItems> orderItems = orderItemsDAO.getOrderItemWithId(tabMenuItems.get(position).getId());
                for(OrderItems oI:orderItems)
                    orderItemsDAO.delete(oI);
                setCounterOfCart();
            }


            if (tabMenuItems.get(position).getType().equals("1"))
                item_icon_image.setImageResource(R.drawable.veg_icon);
            else
                item_icon_image.setImageResource(R.drawable.nonveg_icon);

            checkDatabaseWithList(position);


            add_button.setOnClickListener(v ->
            {
                if (tabMenuItems.get(position).getIsVariant().equals("0")) {
                    add_button.setVisibility(View.GONE);
                    counterLL.setVisibility(View.VISIBLE);
                    tv_counter.setText(("" + 1));
                    addItemToDatabase(tabMenuItems.get(position).getId(), tabMenuItems.get(position).getSubCategoryName(),
                            tabMenuItems.get(position).getDescription(), tabMenuItems.get(position).getCategoryName(), Integer.parseInt(tabMenuItems.get(position).getBasePrice()));
                } else {
                    alertDialog(position);
                }
            });

            iv_add.setOnClickListener(v1 ->

            {
                if (tabMenuItems.get(position).getIsVariant().equals("0")) {

                    int counter = Integer.parseInt(tv_counter.getText().toString());
                    if (counter == 2 && (tabMenuItems.get(position).getSubCategoryName().equalsIgnoreCase(context.getResources().getString(R.string.cold_drinks))
                            || tabMenuItems.get(position).getSubCategoryName().equalsIgnoreCase(context.getResources().getString(R.string.mineral_bottles)))
                            ) {
                        Toast.makeText(context, context.getString(R.string.this_item_cannot_be_added_more_than_two_times), Toast.LENGTH_SHORT).show();
                    } else {
                        counter++;
                        tv_counter.setText(("" + counter));
                        addItemToDatabase(tabMenuItems.get(position).getId(), tabMenuItems.get(position).getSubCategoryName(),
                                tabMenuItems.get(position).getDescription(), tabMenuItems.get(position).getCategoryName(), Integer.parseInt(tabMenuItems.get(position).getBasePrice()));
                    }
                } else {
                    alertDialog(position);

                }
            });

            iv_minus.setOnClickListener(v12 ->

            {

                if (tabMenuItems.get(position).getIsVariant().equals("0")) {
                    int counter = Integer.parseInt(tv_counter.getText().toString());
                    if (counter > 1) {
                        counter--;
                        tv_counter.setText(("" + counter));
                        removeItemFromDatabase(tabMenuItems.get(position).getId());
                    } else if (counter == 1) {
                        counterLL.setVisibility(View.GONE);
                        add_button.setVisibility(View.VISIBLE);
                        removeItemFromDatabase(tabMenuItems.get(position).getId());
                    }
                } else {
                    alertDialog(position);
                }
            });
        }

        private void checkDatabaseWithList(int position) {

            TabMenuResponse.Datum currentItem = tabMenuItems.get(position);
            if (orderItemsDAO.isItemAlreadyExist(currentItem.getId()) > 0) {
                add_button.setVisibility(View.GONE);
                counterLL.setVisibility(View.VISIBLE);
                tv_counter.setText(("" + orderItemsDAO.getCounterOfPerItem(currentItem.getId())));
            }
            setCounterOfCart();
        }
    }

    class OffersHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.offerImage)
        ImageView offerImage;
        @BindView(R.id.offerName)
        TextView offerName;
        @BindView(R.id.offerDescription)
        TextView offerDescription;
        @BindView(R.id.offerLL)
        LinearLayout offerLL;

        OffersHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void offerOptions(int position) {
            offerLL.setVisibility(View.VISIBLE);
            Picasso.with(context).load(ServiceGenerator.BASE_URL + offerItems.get(position).getBanner()).into(offerImage);
            offerName.setText(offerItems.get(position).getTitle());
            offerDescription.setText(offerItems.get(position).getDescription());
        }


    }

    private void alertDialog(int itemPosition) {

        TabMenuResponse.Datum currentItem = tabMenuItems.get(itemPosition);
        final String itemId = currentItem.getId();
        final String itemName = currentItem.getSubCategoryName();
        final String halfPrice = currentItem.getHalfPrice();
        final String fullPrice = currentItem.getFullPrice();
        final String description = currentItem.getDescription();
        final String categoryName = currentItem.getCategoryName();

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.counter_popup_layout);
        dialog.setCancelable(false);

        TextView item_name = dialog.findViewById(R.id.item_name);
        TextView item_halfPrice = dialog.findViewById(R.id.halfPrice);
        ImageView iv_add_halfPrice = dialog.findViewById(R.id.iv_add_half);
        ImageView iv_minus_halfPrice = dialog.findViewById(R.id.iv_minus_half);
        TextView add_counter_half = dialog.findViewById(R.id.add_counter_half);
        TextView item_FullPrice = dialog.findViewById(R.id.fullPrice);
        ImageView iv_add_fullPrice = dialog.findViewById(R.id.iv_add_full);
        ImageView iv_minus_fullPrice = dialog.findViewById(R.id.iv_minus_full);
        TextView add_counter_full = dialog.findViewById(R.id.add_counter_full);
        TextView item_description = dialog.findViewById(R.id.item_description);
        Button close_dialog = dialog.findViewById(R.id.close_dialog);


        item_name.setText(itemName);
        item_description.setText(description);
        item_halfPrice.setText(("Half" + "\n₹ " + halfPrice));
        item_FullPrice.setText(("Full" + "\n₹ " + fullPrice));

        add_counter_half.setText(getAddedItemCounts(itemId, "Half"));
        add_counter_full.setText(getAddedItemCounts(itemId, "Full"));

        iv_add_halfPrice.setOnClickListener(v -> {
            int counter;
            counter = Integer.parseInt(add_counter_half.getText().toString());
            counter++;
            add_counter_half.setText(("" + counter));
        });

        iv_minus_halfPrice.setOnClickListener(v -> {
            int counter;
            counter = Integer.parseInt(add_counter_half.getText().toString());
            if (counter > 1) {
                counter--;
                add_counter_half.setText(("" + counter));
            } else if (counter == 1) {
                add_counter_half.setText(("" + 0));
            }
        });

        iv_add_fullPrice.setOnClickListener(v -> {
            int counter;
            counter = Integer.parseInt(add_counter_full.getText().toString());
            counter++;
            add_counter_full.setText(("" + counter));
        });

        iv_minus_fullPrice.setOnClickListener(v -> {
            int counter;
            counter = Integer.parseInt(add_counter_full.getText().toString());
            if (counter > 1) {
                counter--;
                add_counter_full.setText(("" + counter));
            } else if (counter == 1) {
                add_counter_full.setText(String.valueOf(0));
            }
        });

        dialog.findViewById(R.id.add_item_button).setOnClickListener(v -> {
            resetItemsInDatabase(itemId, itemName, description, "Half", categoryName,
                    Integer.parseInt(halfPrice),
                    Integer.parseInt(add_counter_half.getText().toString()));
            resetItemsInDatabase(itemId, itemName, description, "Full", categoryName,
                    Integer.parseInt(fullPrice),
                    Integer.parseInt(add_counter_full.getText().toString()));
            notifyDataSetChanged();
            setCounterOfCart();
            dialog.dismiss();
        });


        close_dialog.setOnClickListener(v -> dialog.dismiss());
        dialog.show();

    }

    private void addItemToDatabase(String itemId, String subCategoryName, String description, String categoryName, int basePrice) {

        if (orderItemsDAO.isItemAlreadyAdded(itemId, "normal") == 1) {
            OrderItems orderItems = orderItemsDAO.getOrderItemWithId(itemId, "normal");
            orderItems.setCounter(orderItems.getCounter() + 1);
            orderItemsDAO.update(orderItems);
        } else {
            OrderItems orderItems = new OrderItems(itemId, subCategoryName, description, "normal", categoryName, basePrice, 1);
            orderItemsDAO.insert(orderItems);
        }
        setCounterOfCart();
    }

    private void removeItemFromDatabase(String itemId) {

        if (orderItemsDAO.isItemAlreadyAdded(itemId, "normal") == 1 &&
                orderItemsDAO.getCounterOfItems(itemId, "normal") == 1) {
            OrderItems orderItems = orderItemsDAO.getOrderItemWithId(itemId, "normal");
            orderItemsDAO.delete(orderItems);
        } else if (orderItemsDAO.isItemAlreadyAdded(itemId, "normal") == 1) {
            OrderItems orderItems = orderItemsDAO.getOrderItemWithId(itemId, "normal");
            orderItems.setCounter(orderItems.getCounter() - 1);
            orderItemsDAO.update(orderItems);
        }
        setCounterOfCart();
    }

    private void resetItemsInDatabase(String itemId, String itemName, String description, String plate, String categoryName, int basePrice, int counter) {

        if (orderItemsDAO.isItemAlreadyAdded(itemId, plate) == 1 && counter > 0) {
            OrderItems orderItems = orderItemsDAO.getOrderItemWithId(itemId, plate);
            orderItems.setCounter(counter);
            orderItems.setPrice(basePrice);
            orderItemsDAO.update(orderItems);
        } else if (orderItemsDAO.isItemAlreadyAdded(itemId, plate) == 1 && counter == 0) {
            orderItemsDAO.delete(orderItemsDAO.getOrderItemWithId(itemId, plate));
        } else if (counter > 0) {
            orderItemsDAO.insert(new OrderItems(itemId, itemName, description, plate, categoryName, basePrice, counter));
        }

    }

    private void setCounterOfCart() {

        MenuActivity.setCartCounter(orderItemsDAO.getTotalOrderItems());
    }

    private String getAddedItemCounts(String itemId, String plate) {

        if (orderItemsDAO.isItemAlreadyAdded(itemId, plate) > 0) {
            return "" + orderItemsDAO.getCounterOfItems(itemId, plate);
        } else return "0";
    }
}

