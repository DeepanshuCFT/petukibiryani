package com.rest.petukibiryani.user.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.rest.petukibiryani.user.R;
import com.rest.petukibiryani.user.helper.AppPreferences;
import com.rest.petukibiryani.user.network.ApiInterface;
import com.rest.petukibiryani.user.network.ServiceGenerator;
import com.rest.petukibiryani.user.responseModel.UserProfileResponse;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends BaseActivity {

    @BindView(R.id.profile_username)
    TextView profileUsername;
    @BindView(R.id.profile_phone_no)
    TextView profilePhoneNo;
    @BindView(R.id.profile_email_id)
    TextView profileEmailId;
    /*@BindView(R.id.profile_preferences)
    TextView profilePreferences;*/
    @BindView(R.id.profile_birthday)
    TextView profileBirthday;
    @BindView(R.id.profile_anniversary)
    TextView profileAnniversary;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        init();
    }

    private void init() {
        ButterKnife.bind(this);
        initializeToolbar(getResources().getString(R.string.profile), R.drawable.back);
        findViewById(R.id.edit_profile).setVisibility(View.GONE);
        findViewById(R.id.edit_profile).setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putString("name",getTrimmedString(profileUsername));
            bundle.putString("mobile",getTrimmedString(profilePhoneNo));
            bundle.putString("email",getTrimmedString(profileEmailId));
            /*bundle.putString("preferences",getTrimmedString(profilePreferences));*/
            bundle.putString("birthday",getTrimmedString(profileBirthday));
            bundle.putString("anniversary",getTrimmedString(profileAnniversary));
            switchActivity(EditProfileActivity.class,bundle);
        });
    }

    private String getTrimmedString(TextView textView) {
        return textView.getText().toString().trim();
    }

    private void setProfileDetails() {
        showProgressDialog(getString(R.string.please_wait),getString(R.string.fetching_details));
        profileAnniversary.setText("");
        String user_id = AppPreferences.getUser_id(this);
        ApiInterface apiInterface = ServiceGenerator.createLoginRequest(ApiInterface.class);
        Call<UserProfileResponse> call = apiInterface.getUserProfile(user_id);
        call.enqueue(new Callback<UserProfileResponse>() {
            @Override
            public void onResponse(@NonNull Call<UserProfileResponse> call, @NonNull Response<UserProfileResponse> response) {
                hideProgressDialog();
                UserProfileResponse userProfileResponse = response.body();

                if (userProfileResponse != null && userProfileResponse.getStatus()) {
                    List<UserProfileResponse.Datum> datumList = userProfileResponse.getUserDetails();

                    profileUsername.setText(datumList.get(0).getName());
                    profilePhoneNo.setText(datumList.get(0).getMobile());
                    if (datumList.get(0).getEmail() != null)
                        profileEmailId.setText(datumList.get(0).getEmail());
                    /*if (datumList.get(0).getPreferences() != null) {
                        String prefs[] = datumList.get(0).getPreferences().split(",");
                        if (prefs.length == 1)
                            profilePreferences.setText(prefs[0]);
                        else
                            profilePreferences.setText((prefs[0] + " & " + prefs[1]));
                    }*/
                    if (datumList.get(0).getBirthday() != null)
                        profileBirthday.setText(formatDate(datumList.get(0).getBirthday()));
                    if (datumList.get(0).getAnniversary() != null && !datumList.get(0).getAnniversary().equalsIgnoreCase("0000-00-00"))
                        profileAnniversary.setText(formatDate(datumList.get(0).getAnniversary()));
                    findViewById(R.id.edit_profile).setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(@NonNull Call<UserProfileResponse> call, @NonNull Throwable t) {
                hideProgressDialog();
                displayToast("Rejected");
            }
        });


    }

    @SuppressLint("SimpleDateFormat")
    private String formatDate(String date) {
        String newDate = null;
        try {
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date oldDate = formatter.parse(date);

            SimpleDateFormat nFormat = new SimpleDateFormat("dd-MMM-yyyy");
            newDate = nFormat.format(oldDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDate;
    }

    @Override
    protected void onResume() {
        super.onResume();
        setProfileDetails();
    }
}
