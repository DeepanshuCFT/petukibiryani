package com.rest.petukibiryani.user.utilities;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.rest.petukibiryani.user.listeners.OrderItemsDAO;
import com.rest.petukibiryani.user.models.OrderItems;

@Database(entities = {OrderItems.class}, version = 3, exportSchema = false)
public abstract class OrderItemsDatabase extends RoomDatabase {
    public abstract OrderItemsDAO getOrderItemsDAO();
}
