package com.rest.petukibiryani.user.models;

public class Locations {

    private String address;
    private Double latitude;
    private Double longitude;

    public Locations(String address, Double latitude, Double longitude) {
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getAddress() {
        return address;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }
}
