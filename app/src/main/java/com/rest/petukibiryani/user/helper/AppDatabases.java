package com.rest.petukibiryani.user.helper;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.rest.petukibiryani.user.listeners.LocationsListDAO;
import com.rest.petukibiryani.user.listeners.OrderItemsDAO;
import com.rest.petukibiryani.user.utilities.LocationsListDatabase;
import com.rest.petukibiryani.user.utilities.OrderItemsDatabase;

public class AppDatabases {

    public static OrderItemsDAO getOrderItemsDAO(Context context){
        OrderItemsDatabase orderItemsDatabase = Room.databaseBuilder(context, OrderItemsDatabase.class, "db-orderitems")
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()//Allows room to do operation on main thread
                .build();
        return orderItemsDatabase.getOrderItemsDAO();
    }


    public static LocationsListDAO getLocationsListDAO(Context context){

        LocationsListDatabase locationsListDatabase = Room.databaseBuilder(context,LocationsListDatabase.class,"db-locations")
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build();
        return locationsListDatabase.getLocationsListDAO();
    }
}
