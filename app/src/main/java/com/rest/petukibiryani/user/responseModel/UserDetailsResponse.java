package com.rest.petukibiryani.user.responseModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserDetailsResponse {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("user_details")
    @Expose
    private List<UserDetailsResponse.Datum> userDetails = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<UserDetailsResponse.Datum> getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(List<UserDetailsResponse.Datum> userDetails) {
        this.userDetails = userDetails;
    }

    public class Datum {
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("password")
        @Expose
        private String password;
        @SerializedName("mobile")
        @Expose
        private String mobile;
        @SerializedName("whatsapp")
        @Expose
        private String email;
        @SerializedName("verification_code")
        @Expose
        private String verificationCode;
        @SerializedName("is_verify")
        @Expose
        private String isVerify;
        @SerializedName("auth_token")
        @Expose
        private String auth_token;
        @SerializedName("device_token")
        @Expose
        private String device_token;
        @SerializedName("points")
        @Expose
        private String points;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("is_block")
        @Expose
        private String isBlock;
        @SerializedName("created_on")
        @Expose
        private String created_on;
        @SerializedName("updated_on")
        @Expose
        private String updated_on;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getVerificationCode() {
            return verificationCode;
        }

        public void setVerificationCode(String verificationCode) {
            this.verificationCode = verificationCode;
        }

        public String getIsVerify() {
            return isVerify;
        }

        public void setIsVerify(String isVerify) {
            this.isVerify = isVerify;
        }

        public String getAuth_token() {
            return auth_token;
        }

        public void setAuth_token(String auth_token) {
            this.auth_token = auth_token;
        }

        public String getDevice_token() {
            return device_token;
        }

        public void setDevice_token(String device_token) {
            this.device_token = device_token;
        }

        public String getPoints() {
            return points;
        }

        public void setPoints(String points) {
            this.points = points;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getIsBlock() {
            return isBlock;
        }

        public void setIsBlock(String isBlock) {
            this.isBlock = isBlock;
        }

        public String getCreated_on() {
            return created_on;
        }

        public void setCreated_on(String created_on) {
            this.created_on = created_on;
        }

        public String getUpdated_on() {
            return updated_on;
        }

        public void setUpdated_on(String updated_on) {
            this.updated_on = updated_on;
        }
    }
}
