package com.rest.petukibiryani.user.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rest.petukibiryani.user.R;
import com.rest.petukibiryani.user.listeners.CheckVisibleListener;
import com.rest.petukibiryani.user.responseModel.NotificationResponseModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationHolder> {

    private List<NotificationResponseModel.Datum> mNotificationResponseModel;
    private Context mContext;
    private CheckVisibleListener checkVisibleListener;

    public NotificationAdapter(Context mContext, List<NotificationResponseModel.Datum> notificationList,CheckVisibleListener checkVisibleListener ) {
        this.mContext = mContext;
        this.mNotificationResponseModel = notificationList;
        this.checkVisibleListener = checkVisibleListener;
    }


    @NonNull
    @Override
    public NotificationHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(mContext)
                .inflate(R.layout.layout_notifications_item, parent, false);
        return new NotificationHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(@NonNull final NotificationHolder holder, final int position) {
        holder.body.setText(mNotificationResponseModel.get(position).getMessage());
        holder.date.setText(mNotificationResponseModel.get(position).getCreatedOn());

        if(mNotificationResponseModel.size()==0)
            checkVisibleListener.setVisibility();
    }

    @Override
    public int getItemCount() {
        return mNotificationResponseModel.size();
    }

    class NotificationHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_content)
        TextView body;
        @BindView(R.id.tv_date)
        TextView date;

        private NotificationHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
        }
    }
}
