package com.rest.petukibiryani.user.listeners;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.rest.petukibiryani.user.models.OrderItems;

import java.util.List;

@Dao
public interface OrderItemsDAO {

    @Insert
    void insert(OrderItems... orderItems);

    @Update
    void update(OrderItems... orderItems);

    @Delete
    void delete(OrderItems orderItems);

    @Query("SELECT COUNT(*) FROM OrderItems WHERE itemId = :itemId AND plate = :plate")
    int isItemAlreadyAdded(String itemId, String plate);

    @Query("SELECT COUNT(*) FROM OrderItems WHERE itemId = :itemId")
    int isItemAlreadyExist(String itemId);

    @Query("SELECT * FROM OrderItems WHERE itemId = :itemId AND plate = :plate")
    OrderItems getOrderItemWithId(String itemId, String plate);

    @Query("SELECT * FROM OrderItems WHERE itemId = :itemId")
    List<OrderItems> getOrderItemWithId(String itemId);

    @Query("SELECT * FROM OrderItems")
    List<OrderItems> getOrderedItems();

    @Query("SELECT counter FROM OrderItems WHERE itemId = :itemId AND plate = :plate")
    int getCounterOfItems(String itemId, String plate);

    @Query("SELECT SUM(counter) FROM OrderItems WHERE itemId = :itemId ")
    int getCounterOfPerItem(String itemId);

    @Query("SELECT SUM(counter) FROM OrderItems")
    int getTotalOrderItems();

    @Query("DELETE FROM orderitems")
    void deleteAll();





}
