package com.rest.petukibiryani.user.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rest.petukibiryani.user.models.ExpandedMenuModel;
import com.rest.petukibiryani.user.R;

import java.util.List;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context mContext;
    private List<ExpandedMenuModel> mListDataHeader;
    private String points;

    public ExpandableListAdapter(Context context, List<ExpandedMenuModel> listDataHeader, String points) {
        this.mContext = context;
        this.mListDataHeader = listDataHeader;
        this.points = points;
    }

    @Override
    public int getGroupCount() {
        return this.mListDataHeader.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 0;
    }


    @Override
    public Object getGroup(int groupPosition) {
        return this.mListDataHeader.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }


    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }


    @Override
    public boolean hasStableIds() {
        return false;
    }

    @SuppressLint({"SetTextI18n", "InflateParams"})
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        ExpandedMenuModel headerTitle = (ExpandedMenuModel) getGroup(groupPosition);

        if (convertView == null) {
            LayoutInflater inflateInflater = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflateInflater.inflate(R.layout.drawer_item_list, parent,false);
        }

        TextView lblListHeader = convertView.findViewById(R.id.submenu);
        ImageView headerIcon = convertView.findViewById(R.id.iconimage);
        TextView tv_pkbwallet = convertView.findViewById(R.id.pkbWallet);
        LinearLayout fssai_ll = convertView.findViewById(R.id.fssai_layout);
        LinearLayout drawer_item = convertView.findViewById(R.id.drawer_item);
        ImageView iv_rupee = convertView.findViewById(R.id.iv_rupee);

        lblListHeader.setText(headerTitle.getIconName());
        headerIcon.setImageResource(headerTitle.getIconImg());

        if (groupPosition == 3) {
            tv_pkbwallet.setVisibility(View.VISIBLE);
            tv_pkbwallet.setText(("₹ " + points));
            fssai_ll.setVisibility(View.GONE);
            drawer_item.setVisibility(View.VISIBLE);
        }
        else if(groupPosition == 4){
            iv_rupee.setVisibility(View.VISIBLE);
            fssai_ll.setVisibility(View.GONE);
            drawer_item.setVisibility(View.VISIBLE);
        }
        else if (mListDataHeader.size()-1 == groupPosition){
            fssai_ll.setVisibility(View.VISIBLE);
            drawer_item.setVisibility(View.GONE);
        }
        else {
            tv_pkbwallet.setVisibility(View.GONE);
            fssai_ll.setVisibility(View.GONE);
            iv_rupee.setVisibility(View.GONE);
            drawer_item.setVisibility(View.VISIBLE);
        }

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        return null;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }


}
