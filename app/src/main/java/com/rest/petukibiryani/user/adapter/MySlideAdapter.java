package com.rest.petukibiryani.user.adapter;


import com.rest.petukibiryani.user.models.ImageSlider;
import com.rest.petukibiryani.user.network.ServiceGenerator;
import com.rest.petukibiryani.user.R;

import java.util.List;

import ss.com.bannerslider.adapters.SliderAdapter;
import ss.com.bannerslider.viewholder.ImageSlideViewHolder;

public class MySlideAdapter  extends SliderAdapter {


    private List<ImageSlider> hotDealList;

    public MySlideAdapter(List<ImageSlider> hotDealList) {
        this.hotDealList = hotDealList;
    }

    @Override
    public int getItemCount() {
        return hotDealList.size();
    }

    @Override
    public void onBindImageSlide(int position, ImageSlideViewHolder imageSlideViewHolder) {

        for (int i= 0 ; hotDealList.size()> i ;i++){
            ImageSlider imageSlider = hotDealList.get(position);
            imageSlideViewHolder.bindImageSlide(ServiceGenerator.BASE_URL+ imageSlider.getResId(),R.drawable.defult_image,R.drawable.defult_image);
        }
    }

}
