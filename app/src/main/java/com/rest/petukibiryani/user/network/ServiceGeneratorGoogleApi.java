package com.rest.petukibiryani.user.network;

import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static java.sql.Types.TIME;

public class ServiceGeneratorGoogleApi {

    private static final String BASE_URL = "https://maps.googleapis.com/maps/";

    private static Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()));

/*    private static void createClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        httpClient.connectTimeout(TIME, TimeUnit.SECONDS);
        httpClient.readTimeout(TIME, TimeUnit.SECONDS);
        httpClient.writeTimeout(TIME,TimeUnit.SECONDS);
    }*/

    public static <S> S createClient(Class<S> serviceClass) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        httpClient.connectTimeout(TIME, TimeUnit.SECONDS);
        httpClient.readTimeout(TIME, TimeUnit.SECONDS);
        httpClient.writeTimeout(TIME, TimeUnit.SECONDS);
        OkHttpClient okHttpClient = httpClient.build();
        Retrofit retrofit = builder.client(okHttpClient).build();
        return retrofit.create(serviceClass);
    }

   /* private static Retrofit retrofit = null;

    public static Retrofit getResponse() {
        if (retrofit ==null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }*/


}
