package com.rest.petukibiryani.user.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.rest.petukibiryani.user.R;
import com.rest.petukibiryani.user.listeners.OnConnectionChangeListener;
import com.rest.petukibiryani.user.network.ApiInterface;
import com.rest.petukibiryani.user.network.ServiceGenerator;
import com.rest.petukibiryani.user.responseModel.TermsaNdCondition;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TermsConditions extends BaseActivity implements OnConnectionChangeListener {

    @BindView(R.id.tv_service) TextView tv_service;
    private List<TermsaNdCondition.Datum> termsCondition;
    private String service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term_condition);
        ButterKnife.bind(this);
        initializeToolbar(getResources().getString(R.string.terms_condition),R.drawable.back);
        Objects.requireNonNull(noConnectionLayout).setOnRefreshListener(() -> {
            findViewById(R.id.layout_progressBar).setVisibility(View.VISIBLE);
            new CheckInternetConnection(this, getApplicationContext()).execute();
            noConnectionLayout.setRefreshing(false);
        });
    }


    @Override
    public void onConnectionChange(boolean isConnected) {

        if(isConnected){
            termsCondition();
            Objects.requireNonNull(noConnectionLayout).setVisibility(View.GONE);
        }
        else {
            Objects.requireNonNull(noConnectionLayout).setVisibility(View.VISIBLE);
            findViewById(R.id.layout_progressBar).setVisibility(View.GONE);
        }

    }

    public void termsCondition() {
        //findViewById(R.id.layout_progressBar).setVisibility(View.VISIBLE);
        ApiInterface apiInterface = ServiceGenerator.createLoginRequest(ApiInterface.class);
        Call<TermsaNdCondition> call = apiInterface.getTermsCondition();
        call.enqueue(new Callback<TermsaNdCondition>() {
            @Override
            public void onResponse(@NonNull Call<TermsaNdCondition> call, @NonNull Response<TermsaNdCondition> response) {
                findViewById(R.id.layout_progressBar).setVisibility(View.GONE);
                TermsaNdCondition termsaNdCondition = response.body();
                if(termsaNdCondition!=null && termsaNdCondition.getStatus()){
                    termsaNdCondition = response.body();
                    if (termsaNdCondition != null && termsaNdCondition.getStatus()) {

                        for (int i =0; i<termsaNdCondition.getData().size(); i++){
                            termsCondition = Objects.requireNonNull(response.body()).getData();
                            service = termsCondition.get(i).getContent();
                            tv_service.setText(Html.fromHtml(service));
                        }
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<TermsaNdCondition> call, @NonNull Throwable t) {
                findViewById(R.id.layout_progressBar).setVisibility(View.GONE);
                displayToast(R.string.some_thing_wrong);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        findViewById(R.id.layout_progressBar).setVisibility(View.VISIBLE);
        new CheckInternetConnection(this,getApplicationContext()).execute();
    }
}
