package com.rest.petukibiryani.user.models;

public class FirebaseNotificationModel {

    private String body, time;
        private int id;

        public FirebaseNotificationModel() {


        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public FirebaseNotificationModel(int id, String mbody, String mtime ) {
            this.id = id;
            this.body = mbody;

            this.time = mtime;
        }
        public FirebaseNotificationModel(String mbody, String mtime ) {

            this.body = mbody;

            this.time = mtime;
        }

        public String getbody() {
            return body;
        }

        public void setbody(String body) {
            this.body = body;
        }

        public String getDate() {
            return time;
        }

        public void setDate(String Date) {
            this.time = Date;
        }
}
