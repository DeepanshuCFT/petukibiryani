package com.rest.petukibiryani.user.listeners;

public interface BillNotify {
    void cartNotify(float subTotal);
}
