package com.rest.petukibiryani.user.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.rest.petukibiryani.user.R;
import com.rest.petukibiryani.user.activity.BaseActivity;
import com.rest.petukibiryani.user.activity.OtpActivity;
import com.rest.petukibiryani.user.activity.SignUpActivity;
import com.rest.petukibiryani.user.network.ApiInterface;
import com.rest.petukibiryani.user.network.ServiceGenerator;
import com.rest.petukibiryani.user.responseModel.LoginResponseModel;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginFragment extends Fragment {

    @BindView(R.id.et_phoneNo) EditText et_phone;
    private String mobile;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initViews();
    }

    private void initViews() {
        View view = Objects.requireNonNull(getView());
        ButterKnife.bind(this, view);
        view.findViewById(R.id.bt_submit).setOnClickListener(v -> {
            mobile = et_phone.getText().toString();
            if (!mobile.isEmpty() && mobile.length() == 10) {
                userLogin();
            } else {
                ((BaseActivity) Objects.requireNonNull(getActivity())).displayToast(R.string.enter_your_phone_number_here);
            }
        });
    }



    private void userLogin() {
        ((BaseActivity) Objects.requireNonNull(getActivity())).showProgressDialog(getString(R.string.loading),getString(R.string.please_wait));
        ApiInterface apiInterface = ServiceGenerator.createLoginRequest(ApiInterface.class);
        Call<LoginResponseModel> call = apiInterface.loginUser(mobile);
        call.enqueue(new Callback<LoginResponseModel>() {
            @Override
            public void onResponse(@NonNull Call<LoginResponseModel> call, @NonNull Response<LoginResponseModel> response) {
                // if view not created method returned
                if (getView() == null)
                    return;
                 ((BaseActivity) Objects.requireNonNull(getActivity())).hideProgressDialog();
                LoginResponseModel loginResponse = response.body();
                if (loginResponse != null) {
                    if (!loginResponse.getStatus()) {     // if status is not true it means its a new user
                        Intent i = new Intent(getActivity(), SignUpActivity.class);
                        i.putExtra("mobile", mobile);
                        startActivity(i);
                        ((BaseActivity) Objects.requireNonNull(getActivity())).displayToast(loginResponse.getMsg());
                        getActivity().finish();

                    } else if (loginResponse.getStatus()) {     // if status is true it means user exists need to generate OTP
                        Intent intent = new Intent(getActivity(), OtpActivity.class);
                        intent.putExtra("user_id", loginResponse.getUser_id());
                        intent.putExtra("mobile", mobile);
                        startActivity(intent);
                        Objects.requireNonNull(getActivity()).finish();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<LoginResponseModel> call, @NonNull Throwable t) {
                if (getView() == null)
                    return;
                ((BaseActivity) Objects.requireNonNull(getActivity())).hideProgressDialog();
                ((BaseActivity) Objects.requireNonNull(getActivity())).displayToast(R.string.some_thing_wrong);
            }
        });
    }
}
