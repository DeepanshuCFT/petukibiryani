package com.rest.petukibiryani.user.listeners;

public interface CheckVisibleListener {
    void setVisibility();
}
