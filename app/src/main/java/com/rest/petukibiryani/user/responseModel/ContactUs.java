package com.rest.petukibiryani.user.responseModel;

public class ContactUs {
    private boolean status;

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Datum getData() {
        return data;
    }

    public void setData(Datum data) {
        this.data = data;
    }

    private String msg;
    private Datum data;

    public class Datum {
        private String address;
        private String id;
        private String timing;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        private String email;

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTiming() {
            return timing;
        }

        public void setTiming(String timing) {
            this.timing = timing;
        }

        public String getWa_number() {
            return whatsapp_number;
        }

        public void setWa_number(String wa_number) {
            this.whatsapp_number = wa_number;
        }

        public String getPaytm_number() {
            return paytm_number;
        }

        public void setPaytm_number(String paytm_number) {
            this.paytm_number = paytm_number;
        }

        private String whatsapp_number;
        private String paytm_number;


    }
}
