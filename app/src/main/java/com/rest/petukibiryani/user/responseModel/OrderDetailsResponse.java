package com.rest.petukibiryani.user.responseModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderDetailsResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("data")
    @Expose
    private Data data;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("order_no")
        @Expose
        private String orderNo;
        @SerializedName("purchase_method")
        @Expose
        private String purchaseMethod;
        @SerializedName("delivery_address")
        @Expose
        private String deliveryAddress;
        @SerializedName("delivery_type")
        @Expose
        private String deliveryType;
        @SerializedName("points")
        @Expose
        private String points;
        @SerializedName("delivery_charge")
        @Expose
        private String deliveryCharge;
        @SerializedName("total_price")
        @Expose
        private String totalPrice;
        @SerializedName("payable_price")
        @Expose
        private String payablePrice;
        @SerializedName("grand_total")
        @Expose
        private String grandTotal;
        @SerializedName("earn_points")
        @Expose
        private String earnPoints;
        @SerializedName("discount")
        @Expose
        private String discount;
        @SerializedName("cgst")
        @Expose
        private String cgst;
        @SerializedName("sgst")
        @Expose
        private String sgst;
        @SerializedName("special_note")
        @Expose
        private String specialNote;
        @SerializedName("order_status")
        @Expose
        private String orderStatus;
        @SerializedName("discount_by")
        @Expose
        private Object discountBy;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("mobile")
        @Expose
        private String mobile;
        @SerializedName("ordered_items")
        @Expose
        private List<OrderedItem> orderedItems = null;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getOrderNo() {
            return orderNo;
        }

        public void setOrderNo(String orderNo) {
            this.orderNo = orderNo;
        }

        public String getPurchaseMethod() {
            return purchaseMethod;
        }

        public void setPurchaseMethod(String purchaseMethod) {
            this.purchaseMethod = purchaseMethod;
        }

        public String getDeliveryAddress() {
            return deliveryAddress;
        }

        public void setDeliveryAddress(String deliveryAddress) {
            this.deliveryAddress = deliveryAddress;
        }

        public String getDeliveryType() {
            return deliveryType;
        }

        public void setDeliveryType(String deliveryType) {
            this.deliveryType = deliveryType;
        }

        public String getPoints() {
            return points;
        }

        public void setPoints(String points) {
            this.points = points;
        }

        public String getDeliveryCharge() {
            return deliveryCharge;
        }

        public void setDeliveryCharge(String deliveryCharge) {
            this.deliveryCharge = deliveryCharge;
        }

        public String getTotalPrice() {
            return totalPrice;
        }

        public void setTotalPrice(String totalPrice) {
            this.totalPrice = totalPrice;
        }

        public String getPayablePrice() {
            return payablePrice;
        }

        public void setPayablePrice(String payablePrice) {
            this.payablePrice = payablePrice;
        }

        public String getGrandTotal() {
            return grandTotal;
        }

        public void setGrandTotal(String grandTotal) {
            this.grandTotal = grandTotal;
        }

        public String getEarnPoints() {
            return earnPoints;
        }

        public void setEarnPoints(String earnPoints) {
            this.earnPoints = earnPoints;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public String getCgst() {
            return cgst;
        }

        public void setCgst(String cgst) {
            this.cgst = cgst;
        }

        public String getSgst() {
            return sgst;
        }

        public void setSgst(String sgst) {
            this.sgst = sgst;
        }

        public String getSpecialNote() {
            return specialNote;
        }

        public void setSpecialNote(String specialNote) {
            this.specialNote = specialNote;
        }

        public String getOrderStatus() {
            return orderStatus;
        }

        public void setOrderStatus(String orderStatus) {
            this.orderStatus = orderStatus;
        }

        public Object getDiscountBy() {
            return discountBy;
        }

        public void setDiscountBy(Object discountBy) {
            this.discountBy = discountBy;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public List<OrderedItem> getOrderedItems() {
            return orderedItems;
        }

        public void setOrderedItems(List<OrderedItem> orderedItems) {
            this.orderedItems = orderedItems;
        }

        public class OrderedItem {

            @SerializedName("quantity")
            @Expose
            private String quantity;
            @SerializedName("item_id")
            @Expose
            private String itemId;
            @SerializedName("total_price")
            @Expose
            private String totalPrice;
            @SerializedName("half_price")
            @Expose
            private String halfPrice;
            @SerializedName("full_price")
            @Expose
            private String fullPrice;
            @SerializedName("sub_category_name")
            @Expose
            private String subCategoryName;
            @SerializedName("quantity_type")
            @Expose
            private String quantityType;
            @SerializedName("unit_price")
            @Expose
            private String unitPrice;
            @SerializedName("base_price")
            @Expose
            private String basePrice;
            @SerializedName("is_variant")
            @Expose
            private String isVariant;
            @SerializedName("description")
            @Expose
            private String description;

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }
            public String getQuantity() {
                return quantity;
            }

            public void setQuantity(String quantity) {
                this.quantity = quantity;
            }

            public String getItemId() {
                return itemId;
            }

            public void setItemId(String itemId) {
                this.itemId = itemId;
            }

            public String getTotalPrice() {
                return totalPrice;
            }

            public void setTotalPrice(String totalPrice) {
                this.totalPrice = totalPrice;
            }

            public String getHalfPrice() {
                return halfPrice;
            }

            public void setHalfPrice(String halfPrice) {
                this.halfPrice = halfPrice;
            }

            public String getFullPrice() {
                return fullPrice;
            }

            public void setFullPrice(String fullPrice) {
                this.fullPrice = fullPrice;
            }

            public String getSubCategoryName() {
                return subCategoryName;
            }

            public void setSubCategoryName(String subCategoryName) {
                this.subCategoryName = subCategoryName;
            }

            public String getQuantityType() {
                return quantityType;
            }

            public void setQuantityType(String quantityType) {
                this.quantityType = quantityType;
            }

            public String getUnitPrice() {
                return unitPrice;
            }

            public void setUnitPrice(String unitPrice) {
                this.unitPrice = unitPrice;
            }

            public String getBasePrice() {
                return basePrice;
            }

            public void setBasePrice(String basePrice) {
                this.basePrice = basePrice;
            }

            public String getIsVariant() {
                return isVariant;
            }

            public void setIsVariant(String isVariant) {
                this.isVariant = isVariant;
            }
        }

    }

}
