package com.rest.petukibiryani.user.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.rest.petukibiryani.user.R;
import com.rest.petukibiryani.user.listeners.OnConnectionChangeListener;
import com.rest.petukibiryani.user.network.ApiInterface;
import com.rest.petukibiryani.user.network.ServiceGenerator;
import com.rest.petukibiryani.user.responseModel.AboutUs;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AboutPKBActivity extends BaseActivity implements OnConnectionChangeListener {

    @BindView(R.id.tv_success_steps)
    TextView tv_success_steps;
    private List<AboutUs.Datum> aboutUsList;
    private String success;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        ButterKnife.bind(this);
        initializeToolbar(getResources().getString(R.string.aboutUs), R.drawable.back);
        Objects.requireNonNull(noConnectionLayout).setOnRefreshListener(() -> {
            findViewById(R.id.layout_progressBar).setVisibility(View.VISIBLE);
            new CheckInternetConnection(this, getApplicationContext()).execute();
            noConnectionLayout.setRefreshing(false);
        });
    }

    /***
     * Fetching data from About us api and setting as text
     */

    private void aboutUs() {
        //findViewById(R.id.layout_progressBar).setVisibility(View.VISIBLE);
        ApiInterface apiInterface = ServiceGenerator.createLoginRequest(ApiInterface.class);
        Call<AboutUs> call = apiInterface.getAboutUs();
        call.enqueue(new Callback<AboutUs>() {
            @Override
            public void onResponse(@NonNull Call<AboutUs> call, @NonNull Response<AboutUs> response) {
                findViewById(R.id.layout_progressBar).setVisibility(View.GONE);
                AboutUs aboutUs = response.body();
                if (aboutUs != null && aboutUs.getStatus()) {
                    aboutUs = response.body();
                    if (aboutUs != null && aboutUs.getStatus()) {

                        for (int i = 0; i < aboutUs.getData().size(); i++) {
                            aboutUsList = Objects.requireNonNull(response.body()).getData();
                            success = aboutUsList.get(i).getContent();
                            tv_success_steps.setText(Html.fromHtml(success));
                        }
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<AboutUs> call, @NonNull Throwable t) {
                displayToast(R.string.some_thing_wrong);
                findViewById(R.id.layout_progressBar).setVisibility(View.GONE);
            }
        });
    }


    @Override
    public void onConnectionChange(boolean isConnected) {

        if (noConnectionLayout != null) {
            if (isConnected) {
                aboutUs();
                noConnectionLayout.setVisibility(View.GONE);
            } else {
                noConnectionLayout.setVisibility(View.VISIBLE);
                findViewById(R.id.layout_progressBar).setVisibility(View.GONE);
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        findViewById(R.id.layout_progressBar).setVisibility(View.VISIBLE);
        new CheckInternetConnection(this, getApplicationContext()).execute();
    }
}