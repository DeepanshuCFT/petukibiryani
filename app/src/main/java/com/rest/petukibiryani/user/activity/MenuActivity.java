package com.rest.petukibiryani.user.activity;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.rest.petukibiryani.user.R;
import com.rest.petukibiryani.user.adapter.MenuViewPagerAdapter;
import com.rest.petukibiryani.user.helper.AppDatabases;
import com.rest.petukibiryani.user.listeners.OnConnectionChangeListener;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressLint("StaticFieldLeak")
public class MenuActivity extends BaseActivity implements OnConnectionChangeListener {

    @BindView(R.id.menu_toolbar)
    Toolbar menu_toolbar;
    @BindView(R.id.menu_viewpager)
    ViewPager menu_viewPager;
    @BindView(R.id.menu_tabLayout)
    TabLayout menu_tabLayout;

    private ArrayList<String> categoryList = new ArrayList<>();
    private static TextView tv_cart_counter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        init();
        findViewById(R.id.iv_cart).setOnClickListener(v -> switchActivity(CartActivity.class));
    }

    /***
     * Initializing all the views
     */
    private void init() {
        ButterKnife.bind(this);
        setSupportActionBar(menu_toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        menu_toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back));
        menu_toolbar.setNavigationOnClickListener(v -> super.onBackPressed());
        tv_cart_counter = findViewById(R.id.tv_cart_counter);
        setCartCounter(AppDatabases.getOrderItemsDAO(getApplicationContext()).getTotalOrderItems());
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            categoryList = bundle.getStringArrayList("category_list");
        }
        MenuViewPagerAdapter menuViewPagerAdapter = new MenuViewPagerAdapter(getSupportFragmentManager(), categoryList);

        menu_viewPager.setAdapter(menuViewPagerAdapter);
        if (bundle != null) {
            menu_viewPager.setCurrentItem(bundle.getInt("item_position"));
        }
        menu_tabLayout.setupWithViewPager(menu_viewPager);
        Objects.requireNonNull(menu_tabLayout.getTabAt(0)).setCustomView(R.layout.layout_zigzag);
        blink();

        new CheckInternetConnection(this, getApplicationContext()).execute();
    }

    private void blink(){
        final Handler handler = new Handler();
        /*new Thread(() -> {
            int timeToBlink = 500; //in milliseconds
            try{Thread.sleep(timeToBlink);}catch (Exception ignored) {}
            handler.post(() -> {
                TextView tv_offers = findViewById(R.id.blink_offers);
                TextView tv_txt = findViewById(R.id.blink_text);
                if(tv_offers.getVisibility() == View.VISIBLE){
                    tv_offers.setVisibility(View.INVISIBLE);
                    tv_txt.setTextColor(getResources().getColor(R.color.color_black));
                }else{
                    tv_offers.setVisibility(View.VISIBLE);
                    tv_txt.setTextColor(getResources().getColor(R.color.color_white));
                }
                blink();
            });
        }).start();
        */

        handler.postDelayed(() -> {
            TextView tv_offers = findViewById(R.id.blink_offers);
            TextView tv_txt = findViewById(R.id.blink_text);
            if(tv_offers.getVisibility() == View.VISIBLE){
                tv_offers.setVisibility(View.INVISIBLE);
                tv_txt.setTextColor(getResources().getColor(R.color.color_black));
            }else{
                tv_offers.setVisibility(View.VISIBLE);
                tv_txt.setTextColor(getResources().getColor(R.color.color_white));
            }
            blink();
        }, 500);
    }

    /***
     * Set the value of the cart counter
     * @param totalItems contains total no. of items
     */
    public static void setCartCounter(int totalItems) {
        if (totalItems == 0) {
            tv_cart_counter.setVisibility(View.GONE);
        } else {
            tv_cart_counter.setVisibility(View.VISIBLE);
            tv_cart_counter.setText(String.valueOf(totalItems));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setCartCounter(AppDatabases.getOrderItemsDAO(getApplicationContext()).getTotalOrderItems());
    }

    @Override
    public void onConnectionChange(boolean isConnected) {

        if (!isConnected) {
            Snackbar snackbar = Snackbar.make(findViewById(R.id.menu_layout), getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }
    
    
}
