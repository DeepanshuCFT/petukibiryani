package com.rest.petukibiryani.user.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.Task;
import com.rest.petukibiryani.user.R;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rest.petukibiryani.user.adapter.CustomLocationAddressAdapter;
import com.rest.petukibiryani.user.helper.AppConstants;
import com.rest.petukibiryani.user.helper.AppPreferences;
import com.rest.petukibiryani.user.listeners.OnConnectionChangeListener;
import com.rest.petukibiryani.user.network.ApiInterface;
import com.rest.petukibiryani.user.network.ServiceGeneratorGoogleApi;
import com.rest.petukibiryani.user.responseModel.AddressResponse;
import com.rest.petukibiryani.user.responseModel.DistanceResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LocationActivity extends BaseActivity implements OnMapReadyCallback, OnConnectionChangeListener {


    @BindView(R.id.tv_location)
    TextView tv_location;
    @BindView(R.id.delivery_to)
    TextView delivery_to;
    @BindView(R.id.location_color)
    LinearLayout location_color;
    /*@BindView(R.id.cb_pick_up)
    CheckBox pick_up;*/
    /*@BindView(R.id.bt_order_for_later)
    Button orderLater;*/
    private Location currentLocation;
    private GoogleMap mMap;
    private int locationIndex = 0;
    /*private String timeSlot, selectedDate, timeToSave, dateToSave;
    private Calendar myCalendar;*/
    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        initView();
       /* PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder().setCountry("IN").build();
        autocompleteFragment.setFilter(typeFilter);
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                showProgressDialog(getString(R.string.please_wait), getString(R.string.loading));
                locationIndex = 1;
                getAddress(place.getLatLng().latitude, place.getLatLng().longitude);
            }
            @Override
            public void onError(Status status) {
            }
        });*/
    }

    private void initView() {
        ButterKnife.bind(this);

        if (Objects.requireNonNull(getIntent().getExtras()).getBoolean("order_now"))
            findViewById(R.id.order_now_layout).setVisibility(View.VISIBLE);
        else
            findViewById(R.id.order_now_layout).setVisibility(View.GONE);
        initializeToolbar(null,R.drawable.back);
        findViewById(R.id.layout_progressBar).setVisibility(View.VISIBLE);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        Objects.requireNonNull(mapFragment).getMapAsync(LocationActivity.this);
        bundle = Objects.requireNonNull(getIntent().getExtras());
        /*orderLater.setOnClickListener(v -> {
            if (clickable)
                alertForOrderLater();
            else {
                Snackbar snackbar = Snackbar.make(findViewById(R.id.location_layout), getString(R.string.you_are_not_in_7kms), Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        });*/
        findViewById(R.id.bt_order_now).setOnClickListener(v -> {
            if (AppPreferences.isWithinKms(this)) {
                switchActivity(MenuActivity.class, bundle);
                finish();
            } else {
                Snackbar snackbar = Snackbar.make(findViewById(R.id.location_layout), getString(R.string.you_are_not_in_7kms), Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        });
        Objects.requireNonNull(noConnectionLayout).setOnRefreshListener(() -> {
            findViewById(R.id.layout_progressBar).setVisibility(View.VISIBLE);
            new CheckInternetConnection(this, getApplicationContext()).execute();
            noConnectionLayout.setRefreshing(false);
        });
    }

    /*private void alertForOrderLater() {

        timeToSave = null;
        dateToSave = null;
        myCalendar = Calendar.getInstance();
        final Dialog dialog = new Dialog(LocationActivity.this);
        dialog.setContentView(R.layout.order_for_later);
        dialog.setCancelable(false);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        dialog.findViewById(R.id.iv_close).setOnClickListener(v -> dialog.dismiss());

        TextView dateTime = dialog.findViewById(R.id.tv_date_and_time);
        TextView selectDate = dialog.findViewById(R.id.selected_date);
        TextView selectTime = dialog.findViewById(R.id.selected_time);
        setDateField(selectDate, dateTime);
        setTimeField(selectTime, dateTime);
        String myFormat = "EEE, dd MMM"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        selectedDate = sdf.format(myCalendar.getTime());
        timeSlot = getTimeSlot(myCalendar.get(Calendar.HOUR_OF_DAY), myCalendar.get(Calendar.MINUTE));
        dateTime.setText((selectedDate + " at " + timeSlot));

        dialog.findViewById(R.id.bt_set_delivery_time).setOnClickListener(v -> {
            if (dateToSave != null && timeToSave != null) {
                AppPreferences.setOrderTimeAndDate(LocationActivity.this, dateToSave, timeToSave, pick_up.isChecked());
                switchActivity(MenuActivity.class, bundle);
                dialog.dismiss();
            } else
                displayToast(R.string.please_select_date_time);

        });

        if (!isFinishing())
            dialog.show();
    }
*/

    private void estimatingLocation() {
        showProgressDialog(getString(R.string.please_wait), getString(R.string.loading));
        if (isServicesOk()) {
            getDeviceLocation();
        }
    }

    private boolean isServicesOk() {

        int available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(LocationActivity.this);

        if (available == ConnectionResult.SUCCESS) {
            //everything is fine and the user can make map requests
            return true;
        } else if (GoogleApiAvailability.getInstance().isUserResolvableError(available)) {
            //an error occured but we can resolve it
            return false;
        }
        return false;
    }

    @SuppressWarnings("unchecked")
    private void getDeviceLocation() {
        FusedLocationProviderClient fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        try {
            final Task location = fusedLocationProviderClient.getLastLocation();
            location.addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    currentLocation = (Location) task.getResult();
                    if (currentLocation != null) {
                        locationIndex = 0;
                        getAddress(currentLocation.getLatitude(), currentLocation.getLongitude());
                    }
                } else {
                    currentLocation = null;
                }
            });
        } catch (SecurityException e) {
            Log.i("Security Exception", e.toString());
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap gMap) {
        mMap = gMap;

        CameraUpdate center= CameraUpdateFactory.newLatLng(new LatLng(AppConstants.pkbLat, AppConstants.pkbLong));
        CameraUpdate zoom=CameraUpdateFactory.zoomTo(15);
        gMap.moveCamera(center);
        gMap.animateCamera(zoom);
        gMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.map_style));
        gMap.clear();
        gMap.setInfoWindowAdapter(new CustomLocationAddressAdapter(this));
        gMap.getUiSettings().setMyLocationButtonEnabled(false);
        gMap.setMyLocationEnabled(false);
        gMap.getUiSettings().setZoomGesturesEnabled(true);

    }

    private void getAddress(double latitude, double longitude) {

        ApiInterface apiInterface = ServiceGeneratorGoogleApi.createClient(ApiInterface.class);
        Call<AddressResponse> call = apiInterface.getLocation(latitude + "," + longitude, getResources().getString(R.string.api_key));
        call.enqueue(new Callback<AddressResponse>() {
            @Override
            public void onResponse(@NonNull Call<AddressResponse> call, @NonNull Response<AddressResponse> response) {
                AddressResponse addressResponse = response.body();

                if (addressResponse != null && !addressResponse.getStatus().equals("ZERO_RESULTS")) {
                    String address = addressResponse.getResults().get(0).getFormattedAddress();
                    getDistance(latitude, longitude, address);
                } else
                    displayToast(R.string.not_able_to_get_location);
            }

            @Override
            public void onFailure(@NonNull Call<AddressResponse> call, @NonNull Throwable t) {
            }
        });
    }

    private void addMarkers(LatLng latLng, String address) {
        String titles[] = {getString(R.string.current_location_title),
                getString(R.string.selected_location_title)};
        MarkerOptions options = new MarkerOptions()
                .position(latLng)
                .title(titles[locationIndex])
                .snippet(address)
                .icon(BitmapDescriptorFactory.defaultMarker(fromColorString("#FFCC3D")));
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(mMap.addMarker(options).getPosition());
        mMap.addMarker(options);
        LatLng pkbLatLng = new LatLng(AppConstants.pkbLat, AppConstants.pkbLong);
        //mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 11f));
        MarkerOptions pkb_options = new MarkerOptions()
                .position(pkbLatLng)
                .title(getString(R.string.petu_ki_biryani))
                .snippet(AppConstants.PKB_ADDRESS)
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.pkb));
        builder.include(mMap.addMarker(pkb_options).getPosition());
        LatLngBounds bounds = builder.build();
        int padding = 150; // offset from edges of the map in pixels
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        mMap.moveCamera(cameraUpdate);
        hideProgressDialog();
        if(!AppPreferences.isWithinKms(this)){
            Snackbar snackbar = Snackbar.make(findViewById(R.id.location_layout), getString(R.string.you_are_not_in_7kms), Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    private void getDistance(double destLat, double destLong, String address) {

        ApiInterface apiInterface = ServiceGeneratorGoogleApi.createClient(ApiInterface.class);
        Call<DistanceResponse> call = apiInterface.getDistance(Double.toString(AppConstants.pkbLat) + "," + Double.toString(AppConstants.pkbLong),
                destLat + "," + destLong, false, "driving", true, getResources().getString(R.string.api_key));
        call.enqueue(new Callback<DistanceResponse>() {
            @Override
            public void onResponse(@NonNull Call<DistanceResponse> call, @NonNull Response<DistanceResponse> response) {
                DistanceResponse distanceResponse = response.body();
                setRoutePath(Objects.requireNonNull(distanceResponse), new LatLng(destLat, destLong), address);
            }

            @Override
            public void onFailure(@NonNull Call<DistanceResponse> call, @NonNull Throwable t) {
            }
        });
    }

    private void setRoutePath(DistanceResponse directionObj, LatLng latLng, String address) {
        mMap.clear();

        if(directionObj.getRoutes().size()>0){
            ArrayList<LatLng> points;
            DistanceResponse.Route route = directionObj.getRoutes().get(0);

            for (DistanceResponse.Leg leg : route.getLegs()) {
                PolylineOptions lineOptions = new PolylineOptions();
                points = new ArrayList<>();

                for (int j = 0; j < leg.getSteps().size(); j++) {
                    DistanceResponse.Step step = leg.getSteps().get(j);
                    points.addAll(decodePoly(step.getPolyline().getPoints()));
                }
                lineOptions.addAll(points);
                lineOptions.width(20);
                lineOptions.color(getResources().getColor(R.color.color_brown));
                mMap.addPolyline(lineOptions);
            }
            double distance = Double.parseDouble(directionObj.getRoutes().get(0).getLegs().get(0).getDistance().getText().replaceAll("[^.0123456789]", ""));
            String selLatLng = String.valueOf(latLng.latitude) + ":" + String.valueOf(latLng.longitude);
            AppPreferences.saveLocation(this, address, distance <= 7, true, selLatLng);
            setLocationLabel();
            addMarkers(latLng, address);
        }
        else{
            displayToast(R.string.enable_to_find_route);
            hideProgressDialog();
        }
    }

    private void setLocationLabel() {
        if (AppPreferences.isWithinKms(this)) {
            delivery_to.setText(R.string.delivery_to);
            tv_location.setText(AppPreferences.getLocation(this));
            location_color.setBackgroundColor(getResources().getColor(R.color.color_green));
        } else {
            location_color.setBackgroundColor(getResources().getColor(R.color.torch_red));
            delivery_to.setText(getString(R.string.no_stores_in_the_neighbourhood));
            tv_location.setText(getString(R.string.please_change_your_location));
        }
    }

    private List<LatLng> decodePoly(String polyline) {
        List<LatLng> poly = new ArrayList<>();
        int index = 0, len = polyline.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = polyline.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = polyline.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }
        return poly;
    }


    private float[] getHsvFromColor(String colorString) {
        float[] hsv = new float[3];
        int _color = Color.parseColor(colorString);
        Color.colorToHSV(_color, hsv);
        return hsv;
    }


    /**
     * Creates a bitmap descriptor that refers to a colorization of HEX color string.
     *
     * @param colorString hex color as string
     * @return the BitmapDescriptor that was loaded using colorString or null if failed to obtain.
     */
    public float fromColorString(String colorString) {
        return getHsvFromColor(colorString)[0];
    }


    /*private void setDateField(final TextView dateTextView, TextView dateTime) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, (view, year, monthOfYear, dayOfMonth) -> {
            Calendar newDate = Calendar.getInstance();
            newDate.set(year, monthOfYear, dayOfMonth);
            String myFormat = "EEE, dd MMM"; //In which you need put here
            String sendFormat = "yyyy-MM-dd";
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
            selectedDate = sdf.format(newDate.getTime());
            dateTextView.setText(selectedDate);
            dateTime.setText((selectedDate + " at " + timeSlot));
            sdf = new SimpleDateFormat(sendFormat, Locale.US);
            dateToSave = sdf.format(newDate.getTime());
        }, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), (dialog, which) -> {
            if (which == DialogInterface.BUTTON_NEGATIVE) {
                dialog.cancel();
            }
        });

        long now = System.currentTimeMillis() - 1000;
        datePickerDialog.getDatePicker().setMinDate(now);
        datePickerDialog.getDatePicker().setMaxDate(now + (1000 * 60 * 60 * 24 * 3)); // for 3 days

        dateTextView.setOnClickListener(view -> datePickerDialog.show());
    }


    private void setTimeField(final TextView timeTextView, TextView dateTime) {

        int hour = myCalendar.get(Calendar.HOUR_OF_DAY);
        int minute = myCalendar.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker = new TimePickerDialog(this, (timePicker, selectedHour, selectedMinute) -> {

            timeSlot = getTimeSlot(selectedHour, selectedMinute);
            if ((selectedHour < 11) || (selectedHour == 11 && selectedMinute < 30) ||
                    (selectedHour == 21 && selectedMinute > 30) || (selectedHour > 21))
                displayToast(getString(R.string.order_bw) + "\n" + getString(R.string.eleven_to_ten_thirty));
            else {
                timeTextView.setText(timeSlot);
                dateTime.setText((selectedDate + " at " + timeSlot));
                timeToSave = "" + selectedHour + ":" + selectedMinute + ":00";
            }

        }, hour, minute, false);

        timeTextView.setOnClickListener(v -> mTimePicker.show());

    }

    private String getTimeSlot(int selectedHour, int selectedMinute) {

        String fromAP = selectedHour > 11 ? "PM" : "AM";
        String toAP = selectedHour < 11 ? "AM" : "PM";
        int fromTime = selectedHour > 12 ? selectedHour - 12 : selectedHour;
        int toTime = selectedHour != 12 ? fromTime + 1 : 1;

        String fTime = fromTime > 9 ? ("" + fromTime) : ("0" + fromTime);
        String tTime = toTime > 9 ? ("" + toTime) : ("0" + toTime);
        String minutes = selectedMinute > 9 ? ("" + selectedMinute) : ("0" + selectedMinute);

        return (fTime + ":" + minutes + fromAP + " - " + tTime + ":" + minutes + toAP);
    }*/

    @Override
    public void onConnectionChange(boolean isConnected) {
        if (noConnectionLayout != null) {
            if (isConnected) {
                if(AppPreferences.getLatLong(this).equals("0:0")){
                    delivery_to.setText(getString(R.string.please_edit_your_location));
                    tv_location.setText(getString(R.string.not_able_to_get_location));
                    location_color.setBackgroundColor(getResources().getColor(R.color.color_stardust));
                }
                else if (AppPreferences.getLocationSession(this))// setting session saved location
                {
                    showProgressDialog(getString(R.string.please_wait), getString(R.string.loading));
                    String tempLL[] = AppPreferences.getLatLong(this).split(":");
                    LatLng latLng = new LatLng(Double.parseDouble(tempLL[0]), Double.parseDouble(tempLL[1]));
                    getAddress(latLng.latitude, latLng.longitude);
                    locationIndex = 1;
                } else // getting Location and distance between pkb and current Location
                    estimatingLocation();
                findViewById(R.id.location_layout).setVisibility(View.VISIBLE);
                noConnectionLayout.setVisibility(View.GONE);
            } else {
                findViewById(R.id.location_layout).setVisibility(View.GONE);
                noConnectionLayout.setVisibility(View.VISIBLE);
            }
        }
        findViewById(R.id.layout_progressBar).setVisibility(View.GONE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switchActivity(SearchLocationActivity.class);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        new CheckInternetConnection(this, getApplicationContext()).execute();
    }

}

