package com.rest.petukibiryani.user.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.rest.petukibiryani.user.R;
import com.rest.petukibiryani.user.helper.AppDatabases;
import com.rest.petukibiryani.user.helper.AppPreferences;
import com.rest.petukibiryani.user.listeners.OnConnectionChangeListener;
import com.rest.petukibiryani.user.listeners.OrderItemsDAO;
import com.rest.petukibiryani.user.models.CheckoutOrderList;
import com.rest.petukibiryani.user.models.OrderItems;
import com.rest.petukibiryani.user.network.ApiInterface;
import com.rest.petukibiryani.user.network.ServiceGenerator;
import com.rest.petukibiryani.user.responseModel.CheckoutResponse;
import com.rest.petukibiryani.user.responseModel.HomeBannerResponse;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckoutActivity extends BaseActivity implements OnConnectionChangeListener {

    @BindView(R.id.et_address1)
    EditText et_add1;
    @BindView(R.id.et_address2)
    EditText et_add2;
    @BindView(R.id.et_locality)
    EditText et_locality;
    @BindView(R.id.et_landmark)
    EditText et_landmark;
    @BindView(R.id.text_input_layout_add1)
    TextInputLayout text_input_layout_add1;
    @BindView(R.id.text_input_layout_add2)
    TextInputLayout text_input_layout_add2;
    @BindView(R.id.text_input_layout_locality)
    TextInputLayout text_input_layout_locality;
    @BindView(R.id.text_input_layout_landmark)
    TextInputLayout text_input_layout_landmark;
    @BindView(R.id.bt_cod)
    Button cashOnDelivery;
    @BindView(R.id.tv_add_pick_up)
    TextView textPickUpAddress;
    private CheckoutResponse checkoutResponse;
    private String add1, add2, locality, landmark, userId, cgst, sgst, points, totalPrice, specialNote;
    private String deliveryCharge, payablePrice, grandTotal, orderStatus;
    private List<CheckoutOrderList> orderLists = new ArrayList<>();
    private HashMap<String, RequestBody> map = new HashMap<>();
    private OrderItemsDAO orderItemsDAO;
    private Boolean isPickUp;
    private String packagingCharges;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        init();
        cashOnDelivery.setOnClickListener(v -> {
            boolean flag1, flag2, flag3, flag4;

            if (getString(et_add1).isEmpty()) {
                text_input_layout_add1.setError(getString(R.string.field_cannot_be_left_blank));
                flag1 = false;
            } else {
                text_input_layout_add1.setErrorEnabled(false);
                flag1 = true;
            }

            if (getString(et_add2).isEmpty()) {
                text_input_layout_add2.setError(getString(R.string.field_cannot_be_left_blank));
                flag2 = false;
            } else {
                text_input_layout_add2.setErrorEnabled(false);
                flag2 = true;
            }

            if (getString(et_locality).isEmpty()) {
                text_input_layout_locality.setError(getString(R.string.field_cannot_be_left_blank));
                flag3 = false;
            } else {
                text_input_layout_locality.setErrorEnabled(false);
                flag3 = true;
            }


            if (getString(et_landmark).isEmpty()) {
                text_input_layout_landmark.setError(getString(R.string.field_cannot_be_left_blank));
                flag4 = false;
            } else {
                text_input_layout_landmark.setErrorEnabled(false);
                flag4 = true;
            }


            if (flag1 && flag2 && flag3 && flag4 || isPickUp) {
                findViewById(R.id.layout_progressBar).setVisibility(View.VISIBLE);
                setResponse();
                //showProgressDialog(getString(R.string.loading), getString(R.string.please_wait));
                new CheckInternetConnection(this, getApplicationContext()).execute();
            }
        });
    }

    /***
     * Initializing all the views
     */
    @SuppressLint("CutPasteId")
    private void init() {
        ButterKnife.bind(this);
        initializeToolbar(getResources().getString(R.string.checkout), R.drawable.back);
        orderItemsDAO = AppDatabases.getOrderItemsDAO(getApplicationContext());
        isPickUp = AppPreferences.isPickUp(this);
        if (isPickUp) {
            cashOnDelivery.setText(getString(R.string.cash_on_pick_up));
            findViewById(R.id.layout_address).setVisibility(View.GONE);
        } else {
            String address[] = AppPreferences.getLocation(this).split(",");
            switch (address.length) {
                case 2:
                    et_locality.setText(address[0]);
                    et_landmark.setText(address[1]);
                    break;
                case 3:
                    et_add2.setText(address[0]);
                    et_locality.setText(address[1]);
                    et_landmark.setText(address[2]);
                    break;
                case 4:
                    et_add1.setText(address[0]);
                    et_add2.setText(address[1]);
                    et_locality.setText(address[2]);
                    et_landmark.setText(address[3]);
                    break;
                default:
                    et_add1.setText(address[0]);
                    et_add2.setText(address[1]);
                    et_locality.setText(address[2]);

                    StringBuilder landmark = new StringBuilder();
                    for (int i = 3; i < address.length; i++)
                        landmark.append(address[i]);
                    et_landmark.setText(landmark);
                case 1:
            }
            findViewById(R.id.layout_pick_up).setVisibility(View.GONE);
        }

    }


    /***
     *
     * @param editText represents EditText view
     * @return text contained in editText
     */
    private String getString(EditText editText) {
        return editText.getText().toString();
    }

    /***
     * Setting all the response values (userid,cgst,sgst,points,totalPrice,deliveryCharge,payablePrice,grandTotal,orderStatus,address1,
     * address2,locality,landmark,dateTime) for the Api
     */
    @SuppressLint("SimpleDateFormat")
    private void setResponse() {


        String deliveryType = isPickUp ? "Pick Up" : "Home Delivery";
        String deliveryOn = "";
        /*if(AppPreferences.getOrderLater(this))
            deliveryOn = AppPreferences.getOrderDate(this)+" "+AppPreferences.getOrderTime(this);
        else*/


        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            cgst = bundle.getString("cgst");
            sgst = bundle.getString("sgst");
            points = bundle.getString("points");
            totalPrice = bundle.getString("totalPrice");
            deliveryCharge = "0";
            payablePrice = bundle.getString("payablePrice");
            grandTotal = bundle.getString("grandTotal");
            orderStatus = "0";
            specialNote = bundle.getString("special_note");
            packagingCharges = bundle.getString("packaging_charges");
            if (isPickUp) {
                add1 = "Pick";
                add2 = "Up";
                locality = "";
                landmark = "";
            } else {
                add1 = getString(et_add1);
                add2 = getString(et_add2);
                locality = getString(et_locality);
                landmark = getString(et_landmark);
            }
            userId = AppPreferences.getUser_id(this);
        }
        String dateTime;
        Calendar cal = Calendar.getInstance();
        Date currentTime = cal.getTime();
        DateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateTime = timeFormat.format(currentTime);
        RequestBody rb_user_id = createPartFromString(userId);
        RequestBody rb_delivery_type = createPartFromString(deliveryType);
        RequestBody rb_delivery_on = createPartFromString(deliveryOn);
        RequestBody rb_special_note = createPartFromString(specialNote);
        RequestBody rb_cgst = createPartFromString(cgst);
        RequestBody rb_sgst = createPartFromString(sgst);
        RequestBody rb_points = createPartFromString(points);
        RequestBody rb_totalPrice = createPartFromString(totalPrice);
        RequestBody rb_deliveryCharge = createPartFromString(deliveryCharge);
        RequestBody rb_payablePrice = createPartFromString(payablePrice);
        RequestBody rb_grandTotal = createPartFromString(grandTotal);
        RequestBody rb_orderStatus = createPartFromString(orderStatus);
        RequestBody rb_add1 = createPartFromString(add1);
        RequestBody rb_add2 = createPartFromString(add2);
        RequestBody rb_locality = createPartFromString(locality);
        RequestBody rb_landmark = createPartFromString(landmark);
        RequestBody rb_date_time = createPartFromString(dateTime);
        RequestBody rb_pack_charge = createPartFromString(packagingCharges);


        map.put("user_id", rb_user_id);
        map.put("delivery_type", rb_delivery_type);
        map.put("delivery_on", rb_delivery_on);
        map.put("special_note", rb_special_note);
        map.put("cgst", rb_cgst);
        map.put("sgst", rb_sgst);
        map.put("points", rb_points);
        map.put("packaging_charges", rb_pack_charge);
        map.put("total_price", rb_totalPrice);
        map.put("delivery_charge", rb_deliveryCharge);
        map.put("payable_price", rb_payablePrice);
        map.put("grand_total", rb_grandTotal);
        map.put("order_status", rb_orderStatus);
        map.put("address_line1", rb_add1);
        map.put("address_line2", rb_add2);
        map.put("locality", rb_locality);
        map.put("landmark", rb_landmark);
        map.put("created_on", rb_date_time);

        List<OrderItems> orderItems = orderItemsDAO.getOrderedItems();
        for (OrderItems oI : orderItems) {
            CheckoutOrderList checkoutOrderList = new CheckoutOrderList();
            checkoutOrderList.setItemId(oI.getItemId());
            checkoutOrderList.setQuantity(String.valueOf(oI.getCounter()));
            checkoutOrderList.setTotalPrice(String.valueOf(oI.getCounter() * oI.getPrice()));
            checkoutOrderList.setUnitPrice(String.valueOf(oI.getPrice()));
            if (oI.getPlate().equals("normal") || oI.getPlate().equals("Half")) {
                checkoutOrderList.setQuantityType("0");
            } else {
                checkoutOrderList.setQuantityType("1");
            }

            orderLists.add(checkoutOrderList);
            Gson gson = new Gson();
            String string_orderLists = gson.toJson(orderLists);
            RequestBody rb_string_orderLists = createPartFromString(string_orderLists);
            map.put("order_item", rb_string_orderLists);
        }
    }

    /***
     * Sending all the response values (userid,cgst,sgst,points,totalPrice,deliveryCharge,payablePrice,grandTotal,orderStatus,address1,
     * address2,locality,landmark,dateTime) to the Api
     */
    private void checkout() {

        ApiInterface apiInterface = ServiceGenerator.createLoginRequest(ApiInterface.class);
        Call<CheckoutResponse> call = apiInterface.setCheckoutResponse(map);

        if (call != null) {
            call.enqueue(new Callback<CheckoutResponse>() {
                @Override
                public void onResponse(@NonNull Call<CheckoutResponse> call, @NonNull Response<CheckoutResponse> response) {
                    checkoutResponse = response.body();
                    findViewById(R.id.layout_progressBar).setVisibility(View.GONE);
                    if (checkoutResponse != null && checkoutResponse.isStatus()) {
                        displayDialog(checkoutResponse.getOrder_no());
                    }

                }

                @Override
                public void onFailure(@NonNull Call<CheckoutResponse> call, @NonNull Throwable t) {
                    findViewById(R.id.layout_progressBar).setVisibility(View.GONE);
                    displayToast(getString(R.string.unsuccessful));
                }
            });
        }
    }

    /***
     * Display the dialog box of order placed
     * @param orderNo represents OrderNo
     */
    private void displayDialog(String orderNo) {

        Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.order_placed_popup_layout);
        dialog.setCancelable(false);

        TextView tv_orderNo = dialog.findViewById(R.id.tv_order_no);
        tv_orderNo.setText(orderNo);
        if (!isFinishing())
            dialog.show();

        new Handler().postDelayed(() -> {
            if (!isFinishing())
                dialog.dismiss();
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            orderItemsDAO.deleteAll();
            updateUserDetails();
            startActivity(intent);
            finish();
        }, 3000);

    }

    private void getRestaurantStatus() {
        ApiInterface apiInterface = ServiceGenerator.createLoginRequest(ApiInterface.class);
        Call<HomeBannerResponse> call = apiInterface.getHomeBanners();
        call.enqueue(new Callback<HomeBannerResponse>() {
            @Override
            public void onResponse(@NonNull Call<HomeBannerResponse> call, @NonNull Response<HomeBannerResponse> response) {
                HomeBannerResponse homeBannerResponse = response.body();
                Log.i("InStatus", "" + AppPreferences.isRestOpen(CheckoutActivity.this));
                if (homeBannerResponse != null && homeBannerResponse.getStatus()) {
                    AppPreferences.setRestaurantStatus(CheckoutActivity.this,
                            homeBannerResponse.getRestaurantStatus().getStore_status(),
                            homeBannerResponse.getRestaurantStatus().getDescription());
                    if (AppPreferences.isRestOpen(CheckoutActivity.this))
                        checkout();
                    else {
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        displayToast(AppPreferences.getDescription(CheckoutActivity.this));
                        startActivity(intent);
                        finish();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<HomeBannerResponse> call, @NonNull Throwable t) {
            }
        });
    }

    @Override
    public void onConnectionChange(boolean isConnected) {

        if (isConnected) getRestaurantStatus();
        else{
            displayToast(R.string.no_internet_connection);
            findViewById(R.id.layout_progressBar).setVisibility(View.GONE);
        }
    }
}
