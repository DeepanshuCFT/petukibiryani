package com.rest.petukibiryani.user.models;

public class ImageSlider {
    private String name;
    private String resId;

    public ImageSlider(String name, String resId) {
        this.name = name;
        this.resId = resId;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getResId() {
        return resId;
    }

    public void setResId(String resId) {
        this.resId = resId;
    }
}
