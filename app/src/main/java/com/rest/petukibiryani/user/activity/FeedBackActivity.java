package com.rest.petukibiryani.user.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.rest.petukibiryani.user.R;
import com.rest.petukibiryani.user.listeners.OnConnectionChangeListener;
import com.rest.petukibiryani.user.network.ApiInterface;
import com.rest.petukibiryani.user.network.ServiceGenerator;
import com.rest.petukibiryani.user.responseModel.FeedBack;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FeedBackActivity extends BaseActivity implements OnConnectionChangeListener {

    @BindView(R.id.ratingbar) RatingBar ratingbar;
    @BindView(R.id.et_feedback) EditText et_feedback;
    @BindView(R.id.bt_submit) Button submit;
    @BindView(R.id.tv_order_no) TextView tv_order_no;
    private String message;
    private String order_id;
    private float rating;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        init();
        submit.setOnClickListener(v ->
        {
            rating = ratingbar.getRating();

            if(rating==0) {
                displayToast("please give some ratings");
            }
            else if(et_feedback.getText().toString().isEmpty())
            {
                displayToast("feedback cannot be left empty");
            }
            else {
                message = et_feedback.getText().toString();
                rating = ratingbar.getRating();
                feedBack(order_id, message, rating);
            }

        });
        findViewById(R.id.layout_progressBar).setVisibility(View.VISIBLE);
        new CheckInternetConnection(this, getApplicationContext()).execute();
    }

    /***
     * Initializing all the views
     */
    private void init() {
        ButterKnife.bind(this);
        initializeToolbar(getResources().getString(R.string.feedbacks),R.drawable.back);
        Objects.requireNonNull(noConnectionLayout).setOnRefreshListener(() -> {
            findViewById(R.id.layout_progressBar).setVisibility(View.VISIBLE);
            new CheckInternetConnection(this, getApplicationContext()).execute();
            noConnectionLayout.setRefreshing(false);
        });
        order_id=getIntent().getStringExtra("orderId");
        String orderNo = getIntent().getStringExtra("orderNo");
        tv_order_no.setText(orderNo);

    }


    @Override
    public void onConnectionChange(boolean isConnected) {
        if (noConnectionLayout != null) {
            if (isConnected) {
                feedBack(order_id, message, rating);
                noConnectionLayout.setVisibility(View.GONE);
            } else {
                noConnectionLayout.setVisibility(View.VISIBLE);
            }
        }
        findViewById(R.id.layout_progressBar).setVisibility(View.GONE);
    }


    /***
     * Sending feedback to the Api
     */
    public void feedBack(String ORDER, String FEEDBACK,float rtng) {
        ApiInterface apiInterface = ServiceGenerator.createLoginRequest(ApiInterface.class);
        Call<FeedBack> call = apiInterface.setFeedBack(ORDER, FEEDBACK, rtng);
        call.enqueue(new Callback<FeedBack>() {
            @Override
            public void onResponse(@NonNull Call<FeedBack> call, @NonNull Response<FeedBack> response) {
                findViewById(R.id.layout_progressBar).setVisibility(View.GONE);
                FeedBack feedback = response.body();
                assert feedback != null;
                if (feedback.isStatus()) {
                    Intent intent = new Intent(FeedBackActivity.this, MainActivity.class);
                    displayToast(feedback.getMsg());
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                } else {
                    displayToast(R.string.unsuccessful);
                }
            }

            @Override
            public void onFailure(@NonNull Call<FeedBack> call, @NonNull Throwable t) {
                findViewById(R.id.layout_progressBar).setVisibility(View.GONE);
                displayToast(R.string.some_thing_wrong);
            }
        });
    }

}
