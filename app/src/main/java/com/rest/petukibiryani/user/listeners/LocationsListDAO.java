package com.rest.petukibiryani.user.listeners;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.rest.petukibiryani.user.models.LocationsList;

import java.util.List;

@Dao
public interface LocationsListDAO {

    @Insert
    void insert(LocationsList... locationsList);

    @Delete
    void delete(LocationsList locationsList);

    @Query("SELECT * FROM LocationList")
    List<LocationsList> getAllData();

    @Query("SELECT COUNT(*) FROM LocationList WHERE latitude= :latitude AND longitude= :longitude")
    int getExistingData(double latitude,double longitude);

    @Query("SELECT * FROM LocationList WHERE latitude= :latitude AND longitude= :longitude")
    LocationsList getLatLongData(double latitude,double longitude);
}
