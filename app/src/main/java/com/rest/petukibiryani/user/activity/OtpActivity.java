package com.rest.petukibiryani.user.activity;

import android.annotation.SuppressLint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.rest.petukibiryani.user.R;
import com.rest.petukibiryani.user.helper.AppPreferences;
import com.rest.petukibiryani.user.network.ApiInterface;
import com.rest.petukibiryani.user.network.ServiceGenerator;
import com.rest.petukibiryani.user.responseModel.LoginResponseModel;
import com.rest.petukibiryani.user.responseModel.OtpResponseModel;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class OtpActivity extends BaseActivity {

    @BindViews({R.id.otp_digit_1, R.id.otp_digit_2, R.id.otp_digit_3, R.id.otp_digit_4})
    List<EditText> otpInputs;
    @BindView(R.id.timer)
    TextView timer;
    @BindView(R.id.resend)
    TextView resend;
    @BindView(R.id.tv_otp_mess)
    TextView tv_otp_message;
    private String user_id;
    private String mobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        ButterKnife.bind(this);
        SpannableString message = new SpannableString(tv_otp_message.getText().toString());
        message.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), 24, 43, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        message.setSpan(new StyleSpan(Typeface.BOLD), 24, 43, 0);
        tv_otp_message.setText(message);
        findViewById(R.id.bt_confirm_now).setOnClickListener(v -> getOTPForSubmission());
        for (int i = 0; i < 4; i++) {
            otpInputs.get(i).addTextChangedListener(new GenericTextWatcher(otpInputs.get(i)));
        }
        mobile = getIntent().getStringExtra("mobile");
        setTimer();
        resend.setOnClickListener(v -> resendOtp());
        AppPreferences.saveUserID(this, getIntent().getStringExtra("user_id"));
        user_id = AppPreferences.getUser_id(this);
        //registerReceiver(myReceiver, new IntentFilter(AppConstants.INTENT_FILTER_SMS_RECIEVED));
    }


    /*private final BroadcastReceiver myReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            Bundle data = intent.getExtras();

            Object[] pdus = ((Object[]) Objects.requireNonNull(data).get("pdus"));

            for (Object pdu : Objects.requireNonNull(pdus)) {
                SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdu);

                String sender = smsMessage.getDisplayOriginatingAddress();
                //You must check here if the sender is your provider and not another one with same text.

                String messageBody = smsMessage.getMessageBody();
                if (sender.substring(3, 9).equals("PETUKI")) {
                    String otp = messageBody.substring(9, 13);
                    setMessage(otp);
                }
            }
        }
    };*/

    public class GenericTextWatcher implements TextWatcher {
        private View view;

        GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {

            String text = editable.toString();
            switch (view.getId()) {
                case R.id.otp_digit_1:
                    if (text.length() == 1)
                        otpInputs.get(1).requestFocus();
                    break;
                case R.id.otp_digit_2:
                    if (text.length() == 1)
                        otpInputs.get(2).requestFocus();
                    break;
                case R.id.otp_digit_3:
                    if (text.length() == 1)
                        otpInputs.get(3).requestFocus();
                    break;
                case R.id.otp_digit_4:
                    if (text.length() == 0)
                        otpInputs.get(3).requestFocus();
                    break;
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

        }
    }

    /*private void setMessage(String messageText) {

        for (int i = 0; i < 4; i++) {
            otpInputs.get(i).setText(("" + messageText.charAt(i)));
        }
        //  getOTPForSubmission();
    }*/

    private void resendOtp() {
        showProgressDialog(getString(R.string.loading), getString(R.string.please_wait));
        ApiInterface apiInterface = ServiceGenerator.createLoginRequest(ApiInterface.class);
        Call<LoginResponseModel> call = apiInterface.loginUser(mobile);
        call.enqueue(new Callback<LoginResponseModel>() {
            @Override
            public void onResponse(@NonNull Call<LoginResponseModel> call, @NonNull Response<LoginResponseModel> response) {
                hideProgressDialog();
                LoginResponseModel loginResponse = response.body();
                if (loginResponse != null) {
                    if (loginResponse.getStatus()) {
                        resend.setVisibility(View.GONE);
                        timer.setVisibility(View.VISIBLE);
                        setTimer();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<LoginResponseModel> call, @NonNull Throwable t) {
                hideProgressDialog();
                displayToast(R.string.some_thing_wrong);
            }
        });


    }


    private void getOTPForSubmission() {
        StringBuilder enteredOTP = new StringBuilder();
        for (EditText digit : otpInputs) {
            enteredOTP.append(digit.getText().toString());
        }
        verify(enteredOTP.toString());
    }

    public void verify(String OTP) {

        showProgressDialog(getString(R.string.loading), getString(R.string.please_wait));
        ApiInterface apiInterface = ServiceGenerator.createLoginRequest(ApiInterface.class);
        Call<OtpResponseModel> call = apiInterface.Otp_Verify(user_id, OTP);
        call.enqueue(new Callback<OtpResponseModel>() {
            @Override
            public void onResponse(@NonNull Call<OtpResponseModel> call, @NonNull Response<OtpResponseModel> response) {
                OtpResponseModel otpResponseModel = response.body();
                if (Objects.requireNonNull(otpResponseModel).getStatus()) {
                    // if successfully logged in all data is saved
                    assert response.body() != null;
                    AppPreferences.saveUserDetails(getApplicationContext(), new Gson().toJson(response.body().getUser_data().getUser_detail()));
                    switchActivity(MainActivity.class);
                    finish();
                } else {
                    displayToast(otpResponseModel.getMsg());
                }
                hideProgressDialog();
            }

            @Override
            public void onFailure(@NonNull Call<OtpResponseModel> call, @NonNull Throwable t) {
                hideProgressDialog();
                displayToast(R.string.some_thing_wrong);
            }
        });
    }

    public void setTimer() {
        new CountDownTimer(120000, 1000) { // adjust the milli seconds here

            @SuppressLint("DefaultLocale")
            public void onTick(long millisUntilFinished) {
                resend.setVisibility(View.GONE);
                long seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished));
                String secs = seconds > 9 ? "" + seconds : "0" + seconds;
                timer.setText(("0" + TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) + ":" + secs + " min"));
            }

            public void onFinish() {
                resend.setVisibility(View.VISIBLE);
                timer.setVisibility(View.GONE);
            }
        }.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //registerReceiver(myReceiver, new IntentFilter(AppConstants.INTENT_FILTER_SMS_RECIEVED));
    }

    @Override
    protected void onPause() {
        super.onPause();
        //unregisterReceiver(myReceiver);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        /*try {
            unregisterReceiver(myReceiver);
        } catch (IllegalArgumentException e) {
            Log.i("Exception:", e.toString());
        }*/

    }
}

