package com.rest.petukibiryani.user.network.interceptor;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class LoginInterceptor implements Interceptor{

    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Request originalRequest = chain.request();
        Request compressedRequest = originalRequest.newBuilder()
                .header("Content-Type", "application/json")
                .method(originalRequest.method(), originalRequest.body())
                .build();
        return chain.proceed(compressedRequest);
    }
}
