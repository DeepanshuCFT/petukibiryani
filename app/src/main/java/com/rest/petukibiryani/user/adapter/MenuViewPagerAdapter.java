package com.rest.petukibiryani.user.adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.rest.petukibiryani.user.fragment.MenuTabFragment;

import java.util.ArrayList;

public class MenuViewPagerAdapter extends FragmentPagerAdapter {

    private ArrayList<String> mGridMenuResponseList;

    public MenuViewPagerAdapter(FragmentManager fm, ArrayList<String> mGridMenuResponseList) {
        super(fm);
        this.mGridMenuResponseList = mGridMenuResponseList;
    }

    @Override
    public Fragment getItem(int position) {
        return MenuTabFragment.getInstance(position);
    }

    @Override
    public int getCount() {
        return mGridMenuResponseList.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mGridMenuResponseList.get(position);
    }
}
