package com.rest.petukibiryani.user.listeners;

public interface OnConnectionChangeListener {

    void onConnectionChange(boolean isConnected);
}
