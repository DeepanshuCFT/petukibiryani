package com.rest.petukibiryani.user.models;

public class OrderItemsList  {
    private String itemId,itemName,plate,description,categoryName;
    private int price,counter;

    public OrderItemsList(String itemId, String itemName, String plate, String description, String categoryName, int price, int counter) {
        this.itemId = itemId;
        this.itemName = itemName;
        this.plate = plate;
        this.description = description;
        this.categoryName = categoryName;
        this.price = price;
        this.counter = counter;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }
}
