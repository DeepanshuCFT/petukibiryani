package com.rest.petukibiryani.user.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.rest.petukibiryani.user.helper.AppPreferences;
import com.rest.petukibiryani.user.listeners.OnConnectionChangeListener;
import com.rest.petukibiryani.user.network.ApiInterface;
import com.rest.petukibiryani.user.network.ServiceGenerator;
import com.rest.petukibiryani.user.responseModel.UserDetailsResponse;
import com.rest.petukibiryani.user.R;
import com.rest.petukibiryani.user.utilities.NetworkUtil;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.rest.petukibiryani.user.service.MyFirebaseMessagingService.INTENT_FILTER;


public abstract class BaseActivity extends AppCompatActivity {

    private ProgressDialog mProgressDialog;
    BroadcastReceiver mNetworkReceiver;
    @Nullable
    @BindView(R.id.no_connection_layout_swipe)
    SwipeRefreshLayout noConnectionLayout;
    @Nullable
    @BindView(R.id.toolbar)
    Toolbar toolbar;


    public void addFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.layout_container, fragment, fragment.getClass().getName());
        fragmentTransaction.commit();
    }

    /*public void setGone(View v){
        v.setVisibility(View.GONE);
    }

    public void setGone(int id){
        setGone(findViewById(id));
    }*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void initializeToolbar(String title, int icon) {
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(title);
        Objects.requireNonNull(toolbar).setNavigationIcon(getResources().getDrawable(icon));
        toolbar.setNavigationOnClickListener(v -> super.onBackPressed());

    }


    public void switchActivity(Class<?> destinationActivity) {
        startActivity(new Intent(this, destinationActivity));
    }

    /**
     * Method used to switch from current activity to other with assignmentData
     *
     * @param destinationActivity activity to open
     * @param bundle              assignmentData that carry to destination activity
     */
    public void switchActivity(Class<?> destinationActivity, Bundle bundle) {
        Intent intent = new Intent(this, destinationActivity);
        if (bundle != null)
            intent.putExtras(bundle);
        startActivity(intent);
    }



    /*public void switchActivity(Class<?> destinationActivity, int requestCode) {
        Intent intent = new Intent(this, destinationActivity);
        startActivityForResult(intent, requestCode);
    }


  /*  public void switchActivity(Class<?> destinationActivity, Bundle bundle, int requestCode) {
        Intent intent = new Intent(this, destinationActivity);
        intent.putExtras(bundle);
        startActivityForResult(intent, requestCode);
    }**/

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mNetworkReceiver, new IntentFilter(INTENT_FILTER));
        LocalBroadcastManager.getInstance(this).registerReceiver(mNetworkReceiver, new IntentFilter(INTENT_FILTER));
    }


    /**
     * Method used to display short duration toast
     *
     * @param message message to be displayed
     */
    public void displayToast(String message) {
        if (TextUtils.isEmpty(message) || message.equalsIgnoreCase("null"))
            return;
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    /**
     * Method used to display short duration toast
     *
     * @param resId resource id of the message string to be displayed
     */
    public void displayToast(int resId) {
        displayToast(getString(resId));
    }

    /**
     * Method to show Progress Dialog when requesting server
     *
     * @param message message to be displayed
     */
    public void showProgressDialog(String title, String message) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.setTitle(title);
            mProgressDialog.setMessage(message);
        }
        if (!isFinishing())
            mProgressDialog.show();

    }

    /**
     * Method to hide progress dialog when received response from API
     */
    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing() && !isFinishing())
            mProgressDialog.dismiss();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.cancel();
        }
        try {
            this.unregisterReceiver(mNetworkReceiver);
        } catch (IllegalArgumentException e) {
            Log.i("Exception:", e.toString());
        }
    }

    @NonNull
    public RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(okhttp3.MultipartBody.FORM, descriptionString);
    }

    public void updateUserDetails() {
        String user_id = AppPreferences.getUser_id(this);
        ApiInterface apiInterface = ServiceGenerator.createLoginRequest(ApiInterface.class);
        Call<UserDetailsResponse> call = apiInterface.getUserDetails(user_id);
        call.enqueue(new Callback<UserDetailsResponse>() {
            @Override
            public void onResponse(@NonNull Call<UserDetailsResponse> call, @NonNull Response<UserDetailsResponse> response) {
                UserDetailsResponse userDetailsResponse = response.body();

                if (userDetailsResponse != null && userDetailsResponse.getStatus()) {
                    ArrayList<UserDetailsResponse.Datum> datumArrayList = new ArrayList<>(userDetailsResponse.getUserDetails());
                    AppPreferences.saveBonusPts(getApplicationContext(), datumArrayList);
                    datumArrayList.clear();
                }
            }

            @Override
            public void onFailure(@NonNull Call<UserDetailsResponse> call, @NonNull Throwable t) {
            }
        });
    }




    @SuppressLint("StaticFieldLeak")
    public class CheckInternetConnection extends AsyncTask<Void, Void, Boolean> {

        private OnConnectionChangeListener onConnectionChangeListener;
        private Context context;

        CheckInternetConnection(OnConnectionChangeListener onConnectionChangeListener, Context context) {
            this.context = context;
            this.onConnectionChangeListener = onConnectionChangeListener;
        }

        private boolean isConnected() {
            return NetworkUtil.getConnectivityStatus(context);
        }

        @Override
        protected Boolean doInBackground(Void... voids) {

            boolean result;
            try {
                Socket socket = new Socket();
                SocketAddress socketAddress = new InetSocketAddress("8.8.8.8", 53);
                socket.connect(socketAddress, 2000);
                socket.close();
                result = true;
            } catch (IOException e) {
                e.printStackTrace();
                result = false;
            }

            return result;
        }


        @Override
        protected void onPostExecute(Boolean result) {
            if (isConnected()) {
                if (result)
                    onConnectionChangeListener.onConnectionChange(true);
                else
                    onConnectionChangeListener.onConnectionChange(false);
            } else
                onConnectionChangeListener.onConnectionChange(false);

        }
    }

}
