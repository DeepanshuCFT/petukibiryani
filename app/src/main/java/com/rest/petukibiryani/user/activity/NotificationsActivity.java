package com.rest.petukibiryani.user.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.rest.petukibiryani.user.R;
import com.rest.petukibiryani.user.adapter.NotificationAdapter;
import com.rest.petukibiryani.user.helper.AppPreferences;
import com.rest.petukibiryani.user.listeners.CheckVisibleListener;
import com.rest.petukibiryani.user.listeners.OnConnectionChangeListener;
import com.rest.petukibiryani.user.network.ApiInterface;
import com.rest.petukibiryani.user.network.ServiceGenerator;
import com.rest.petukibiryani.user.responseModel.NotificationResponseModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.rest.petukibiryani.user.service.MyFirebaseMessagingService.INTENT_FILTER;


public class NotificationsActivity extends BaseActivity implements CheckVisibleListener, OnConnectionChangeListener {

    @BindView(R.id.ll_null_screen) LinearLayout null_layout;
    @BindView(R.id.rv_notifications) RecyclerView mRecyclerView;
    @BindView(R.id.tv_notifi) TextView tv_notifications;
    public NotificationAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<NotificationResponseModel.Datum> mNotificationResponseModel = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ButterKnife.bind(this);
        initializeToolbar(getResources().getString(R.string.notification), R.drawable.back);
        Objects.requireNonNull(noConnectionLayout).setOnRefreshListener(() -> {
            new CheckInternetConnection(this, getApplicationContext()).execute();
            noConnectionLayout.setRefreshing(false);
        });
        MainActivity.totalItems = 0;

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(NotificationsActivity.this, instanceIdResult -> {
            String newToken = instanceIdResult.getToken();
            Log.e("newToken", newToken);

        });

        mAdapter = new NotificationAdapter(this, mNotificationResponseModel, this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        ((LinearLayoutManager) mLayoutManager).setReverseLayout(true);
        ((LinearLayoutManager) mLayoutManager).setStackFromEnd(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
    }


    @Override
    public void onConnectionChange(boolean isConnected) {
        if (noConnectionLayout != null) {
            if (isConnected) {
                getNotificationList();
                noConnectionLayout.setVisibility(View.GONE);
            } else {
                noConnectionLayout.setVisibility(View.VISIBLE);
                findViewById(R.id.layout_progressBar).setVisibility(View.GONE);
            }
        }
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            MainActivity.totalItems = 0;
            mLayoutManager = new LinearLayoutManager(NotificationsActivity.this);
            ((LinearLayoutManager) mLayoutManager).setReverseLayout(true);
            ((LinearLayoutManager) mLayoutManager).setStackFromEnd(true);
            mRecyclerView.setLayoutManager(mLayoutManager);
            mNotificationResponseModel.clear();
            getNotificationList();
        }
    };

    @Override
    public void setVisibility() {
        mRecyclerView.setVisibility(View.GONE);
        tv_notifications.setVisibility(View.GONE);
        null_layout.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        findViewById(R.id.layout_progressBar).setVisibility(View.VISIBLE);
        new CheckInternetConnection(this,getApplicationContext()).execute();
        registerReceiver(broadcastReceiver, new IntentFilter(INTENT_FILTER));
        LocalBroadcastManager.getInstance(NotificationsActivity.this).registerReceiver(broadcastReceiver, new IntentFilter(INTENT_FILTER));
    }

    @Override
    protected void onPause() {
        this.unregisterReceiver(broadcastReceiver);
        super.onPause();

    }

    private void getNotificationList() {
        setVisibility();
        //findViewById(R.id.layout_progressBar).setVisibility(View.VISIBLE);
        null_layout.setVisibility(View.GONE);
        String userId = AppPreferences.getUser_id(this);
        ApiInterface apiInterface = ServiceGenerator.createLoginRequest(ApiInterface.class);
        Call<NotificationResponseModel> call = apiInterface.getNotificationList(userId);
        call.enqueue(new Callback<NotificationResponseModel>() {
            @Override
            public void onResponse(@NonNull Call<NotificationResponseModel> call, @NonNull Response<NotificationResponseModel> response) {
                NotificationResponseModel notificationResponseModel = response.body();
                findViewById(R.id.layout_progressBar).setVisibility(View.GONE);
                if (notificationResponseModel != null && notificationResponseModel.getStatus() && notificationResponseModel.getData() != null) {
                    if (notificationResponseModel.getData().size() > 0) {
                        null_layout.setVisibility(View.GONE);
                        mRecyclerView.setVisibility(View.VISIBLE);
                        tv_notifications.setVisibility(View.VISIBLE);
                    }
                    mNotificationResponseModel.addAll(notificationResponseModel.getData());
                    mAdapter.notifyDataSetChanged();
                }
                else if (Objects.requireNonNull(notificationResponseModel).getData() == null){
                    null_layout.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(@NonNull Call<NotificationResponseModel> call, @NonNull Throwable t) {
                findViewById(R.id.layout_progressBar).setVisibility(View.GONE);
                displayToast(R.string.some_thing_wrong);
            }
        });
    }
}
