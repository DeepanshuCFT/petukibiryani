package com.rest.petukibiryani.user.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.rest.petukibiryani.user.R;
import com.rest.petukibiryani.user.activity.BaseActivity;
import com.rest.petukibiryani.user.adapter.TabMenuAdapter;
import com.rest.petukibiryani.user.helper.AppPreferences;
import com.rest.petukibiryani.user.network.ApiInterface;
import com.rest.petukibiryani.user.network.ServiceGenerator;
import com.rest.petukibiryani.user.responseModel.OffersResponse;
import com.rest.petukibiryani.user.responseModel.TabMenuResponse;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MenuTabFragment extends Fragment {

    @BindView(R.id.menu_rv)
    RecyclerView recyclerView;
    @BindView(R.id.layout_progressBar)
    LinearLayout ll_pb;
    private  OffersResponse offersResponse;
    private TabMenuResponse tabMenuResponse;
    private ArrayList<TabMenuResponse.Datum> tabMenuResponseArrayList = new ArrayList<>();
    private ArrayList<OffersResponse.Datum> offersResponseArrayList=new ArrayList<>();
    private String[] category_ids;
    private TabMenuAdapter tabMenuAdapter;
    private int height,weight;

    public static Fragment getInstance(int position) {
        Bundle tabs_bundle = new Bundle();
        tabs_bundle.putInt("position", position);
        MenuTabFragment menuTabFragment = new MenuTabFragment();
        menuTabFragment.setArguments(tabs_bundle);
        return menuTabFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         height=getResources().getDimensionPixelSize(R.dimen._165sdp);
         weight=getResources().getDimensionPixelSize(R.dimen._105sdp);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu_tab, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        SharedPreferences spCatIds = AppPreferences.getSharedPreferencesForCategoryId(Objects.requireNonNull(getContext()));
        category_ids = Objects.requireNonNull(spCatIds.getString("category_ids", null)).split("#");
        if (getArguments() != null) {
            int position = getArguments().getInt("position");
            getTabMenu(position);
            recyclerView.setAdapter(tabMenuAdapter);
        }
    }

    public void getTabMenu(int position) {
        ll_pb.setVisibility(View.VISIBLE);
        ApiInterface apiInterface = ServiceGenerator.createLoginRequest(ApiInterface.class);
        if(position==0)
        {
            offersResponseArrayList.clear();

            Call<OffersResponse> call = apiInterface.getOfferResponse();

            tabMenuAdapter = new TabMenuAdapter(getContext(), tabMenuResponseArrayList,offersResponseArrayList,position, height, weight);

            call.enqueue(new Callback<OffersResponse>() {
                @Override
                public void onResponse(@NonNull Call<OffersResponse> call, @NonNull Response<OffersResponse> response) {
                    ll_pb.setVisibility(View.GONE);
                    offersResponse = response.body();
                    if (offersResponse != null && offersResponse.getStatus()) {
                        offersResponseArrayList.addAll(offersResponse.getData());
                        tabMenuAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<OffersResponse> call, @NonNull Throwable t) {

                    try {
                        ll_pb.setVisibility(View.GONE);
                        ((BaseActivity) Objects.requireNonNull(getActivity())).displayToast(R.string.some_thing_wrong);
                    }catch (Exception ignored){}

                }
            });
        }
        else {

            tabMenuResponseArrayList.clear();
            Call<TabMenuResponse> call = apiInterface.getTabMenuResponse(category_ids[position-1]);

            tabMenuAdapter = new TabMenuAdapter(getContext(), tabMenuResponseArrayList,offersResponseArrayList,position, height, weight);

            call.enqueue(new Callback<TabMenuResponse>() {
                @Override
                public void onResponse(@NonNull Call<TabMenuResponse> call, @NonNull Response<TabMenuResponse> response) {
                    ll_pb.setVisibility(View.GONE);
                    tabMenuResponse = response.body();
                    if (tabMenuResponse != null && tabMenuResponse.getStatus()) {
                        tabMenuResponseArrayList.addAll(tabMenuResponse.getData());
                        tabMenuAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<TabMenuResponse> call, @NonNull Throwable t) {
                    ll_pb.setVisibility(View.GONE);
                   // ((BaseActivity) Objects.requireNonNull(getActivity())).displayToast(R.string.some_thing_wrong);
                }
            });
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        tabMenuAdapter.notifyDataSetChanged();
    }


}
