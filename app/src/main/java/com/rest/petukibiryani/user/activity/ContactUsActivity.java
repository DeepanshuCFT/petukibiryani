package com.rest.petukibiryani.user.activity;

import android.annotation.SuppressLint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.SpannableStringBuilder;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.TextView;

import com.rest.petukibiryani.user.R;
import com.rest.petukibiryani.user.listeners.OnConnectionChangeListener;
import com.rest.petukibiryani.user.network.ApiInterface;
import com.rest.petukibiryani.user.network.ServiceGenerator;
import com.rest.petukibiryani.user.responseModel.ContactUs;


import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactUsActivity extends BaseActivity implements OnConnectionChangeListener {

    @BindView(R.id.tv_outlet)
    TextView tv_outlet;
    @BindView(R.id.tv_shop)
    TextView tv_shop;
    @BindView(R.id.tv_whatsapp)
    TextView tv_whatsapp;
    @BindView(R.id.tv_email)
    TextView tv_email;
    @BindView(R.id.tv_paytm)
    TextView tv_paytm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        init();
    }

    /***
     * Initializing all the views
     */
    @SuppressLint("SetTextI18n")
    private void init() {
        ButterKnife.bind(this);
        initializeToolbar(getResources().getString(R.string.contact_us), R.drawable.back);
        Objects.requireNonNull(noConnectionLayout).setOnRefreshListener(() -> {
            findViewById(R.id.layout_progressBar).setVisibility(View.VISIBLE);
            new CheckInternetConnection(this, getApplicationContext()).execute();
            noConnectionLayout.setRefreshing(false);
        });

        tv_outlet.setText(getResources().getString(R.string.outlet));
        tv_shop.setText(getResources().getString(R.string.shop));
        tv_whatsapp.setText(getResources().getString(R.string.whatsapp));
        tv_paytm.setText(getResources().getString(R.string.paytm));
        tv_email.setText(getResources().getString(R.string.email));
    }

    @Override
    public void onConnectionChange(boolean isConnected) {
        if (noConnectionLayout != null) {
            if (isConnected) {
                contactUs();
                noConnectionLayout.setVisibility(View.GONE);
            } else {
                noConnectionLayout.setVisibility(View.VISIBLE);
                findViewById(R.id.layout_progressBar).setVisibility(View.GONE);
            }
        }

    }

    /***
     * Fetching data from Contact_us api and setting as text
     */
    private void contactUs() {
        ApiInterface apiInterface = ServiceGenerator.createLoginRequest(ApiInterface.class);
        Call<ContactUs> call = apiInterface.getContactUs();
        call.enqueue(new Callback<ContactUs>() {
            @Override
            public void onResponse(@NonNull Call<ContactUs> call, @NonNull Response<ContactUs> response) {
                findViewById(R.id.layout_progressBar).setVisibility(View.GONE);
                ContactUs contactUs = response.body();
                if (contactUs != null && contactUs.getStatus()) {
                    String addr = contactUs.getData().getAddress();
                    String shop = contactUs.getData().getTiming();
                    String whatsapp = contactUs.getData().getWa_number();
                    String paytm = contactUs.getData().getPaytm_number();
                    String email = contactUs.getData().getEmail();

                    SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(getResources().getString(R.string.outlet) + " " + addr);
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), 0, 16, 0);
                    spannableStringBuilder.setSpan(new RelativeSizeSpan(1.2f), 0, 16, 0);
                    tv_outlet.setText(spannableStringBuilder);

                    SpannableStringBuilder mBuilderShop = new SpannableStringBuilder(getResources().getString(R.string.shop) + " " + shop);
                    mBuilderShop.setSpan(new StyleSpan(Typeface.BOLD), 0, 13, 0);
                    mBuilderShop.setSpan(new RelativeSizeSpan(1.2f), 0, 13, 0);
                    tv_shop.setText(mBuilderShop);


                    SpannableStringBuilder mBuilderWhatsapp = new SpannableStringBuilder(getResources().getString(R.string.whatsapp) + " " + whatsapp);
                    mBuilderWhatsapp.setSpan(new StyleSpan(Typeface.BOLD), 0, 13, 0);
                    mBuilderWhatsapp.setSpan(new RelativeSizeSpan(1.2f), 0, 13, 0);
                    tv_whatsapp.setText(mBuilderWhatsapp);


                    SpannableStringBuilder mBuilderPaytm = new SpannableStringBuilder(getResources().getString(R.string.paytm) + " " + paytm);
                    mBuilderPaytm.setSpan(new StyleSpan(Typeface.BOLD), 0, 10, 0);
                    mBuilderPaytm.setSpan(new RelativeSizeSpan(1.2f), 0, 10, 0);
                    tv_paytm.setText(mBuilderPaytm);


                    SpannableStringBuilder mBuilderEmail = new SpannableStringBuilder(getResources().getString(R.string.email) + " " + email);
                    mBuilderEmail.setSpan(new StyleSpan(Typeface.BOLD), 0, 10, 0);
                    mBuilderEmail.setSpan(new RelativeSizeSpan(1.2f), 0, 11, 0);
                    tv_email.setText(mBuilderEmail);

                }
            }

            @Override
            public void onFailure(@NonNull Call<ContactUs> call, @NonNull Throwable t) {
                findViewById(R.id.layout_progressBar).setVisibility(View.GONE);
                displayToast(R.string.some_thing_wrong);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        findViewById(R.id.layout_progressBar).setVisibility(View.VISIBLE);
        new CheckInternetConnection(this, getApplicationContext()).execute();

    }
}
