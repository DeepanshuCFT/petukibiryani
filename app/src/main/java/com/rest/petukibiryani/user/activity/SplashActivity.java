package com.rest.petukibiryani.user.activity;


import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.pm.PackageInfoCompat;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.rest.petukibiryani.user.R;
import com.rest.petukibiryani.user.helper.AppPreferences;


import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class SplashActivity extends BaseActivity {

    private final int REQUEST_CODE = 1;
    private boolean ALL_PERMISSIONS_GRANTED = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        clearData();
        AppPreferences.setSessionFalse(this);
        // if version greater than lollipop then ask for runtime permissions else animate pkb
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!permissionIsEnabledOrNot(ACCESS_FINE_LOCATION,
                    ACCESS_COARSE_LOCATION))
                requestMultiplePermissions();
            else
                ALL_PERMISSIONS_GRANTED = true;
        } else
            animatePKB();
        if (ALL_PERMISSIONS_GRANTED)
            animatePKB();
    }

    private void clearData() {

        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);

            long versionCode = PackageInfoCompat.getLongVersionCode(packageInfo);

            if (versionCode > 7) {
                AppPreferences.clearData(this);
            }

            // send version code from here

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();

        }
    }


    private void animatePKB() {
        new Handler().postDelayed(() -> {

            findViewById(R.id.upper_image).setVisibility(View.VISIBLE);
            findViewById(R.id.upper_image).setAnimation(AnimationUtils.loadAnimation(this, R.anim.anim_from_up));

            new Handler().postDelayed(() -> {

                if (AppPreferences.getAuth_token(SplashActivity.this) != null) {
                    switchActivity(MainActivity.class);
                    finish();
                } else {
                    switchActivity(LoginActivity.class);
                    finish();

                }
            }, 1600);
        }, 100);
    }

    private void requestMultiplePermissions() {
        ActivityCompat.requestPermissions(SplashActivity.this,
                new String[]{ACCESS_FINE_LOCATION,
                        ACCESS_COARSE_LOCATION},
                REQUEST_CODE);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        boolean permissionGranted = true;
        switch (requestCode) {
            case REQUEST_CODE:
                if (grantResults.length > 0) {
                    for (int i : grantResults)
                        permissionGranted = permissionGranted && (i == PackageManager.PERMISSION_GRANTED);
                    if (!permissionGranted)
                        Toast.makeText(SplashActivity.this, getString(R.string.all_permissions_are_not_granted), Toast.LENGTH_SHORT).show();
                    else {
                        ALL_PERMISSIONS_GRANTED = true;
                        animatePKB();
                    }
                }
        }
    }

    private boolean permissionIsEnabledOrNot(String... permissions) {

        boolean permissionEnabled = true;
        for (String i : permissions)
            permissionEnabled = permissionEnabled &&
                    (ContextCompat.checkSelfPermission(getApplicationContext(), i) == PackageManager.PERMISSION_GRANTED);
        return permissionEnabled;
    }

}
