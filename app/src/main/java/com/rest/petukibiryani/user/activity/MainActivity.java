package com.rest.petukibiryani.user.activity;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.rest.petukibiryani.user.R;
import com.rest.petukibiryani.user.adapter.ExpandableListAdapter;
import com.rest.petukibiryani.user.adapter.GridMenuAdapter;
import com.rest.petukibiryani.user.adapter.MySlideAdapter;
import com.rest.petukibiryani.user.helper.AppConstants;
import com.rest.petukibiryani.user.helper.AppDatabases;
import com.rest.petukibiryani.user.helper.AppPreferences;
import com.rest.petukibiryani.user.listeners.MyClickListener;
import com.rest.petukibiryani.user.listeners.OnConnectionChangeListener;
import com.rest.petukibiryani.user.models.ExpandedMenuModel;
import com.rest.petukibiryani.user.models.ImageSlider;
import com.rest.petukibiryani.user.network.ApiInterface;
import com.rest.petukibiryani.user.network.ServiceGenerator;
import com.rest.petukibiryani.user.network.ServiceGeneratorGoogleApi;
import com.rest.petukibiryani.user.responseModel.AddressResponse;
import com.rest.petukibiryani.user.responseModel.DistanceResponse;
import com.rest.petukibiryani.user.responseModel.GridMenuResponse;
import com.rest.petukibiryani.user.responseModel.HomeBannerResponse;
import com.rest.petukibiryani.user.responseModel.SocialLoginResponse;
import com.rest.petukibiryani.user.service.CartCheckService;
import com.rest.petukibiryani.user.views.PicassoImageLoadingService;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ss.com.bannerslider.Slider;

import static com.rest.petukibiryani.user.service.MyFirebaseMessagingService.INTENT_FILTER;

public class MainActivity extends BaseActivity implements MyClickListener, OnConnectionChangeListener {

    @BindView(R.id.notification_counter)
    TextView notification_counter;
    @BindView(R.id.cart_counter)
    TextView tv_cart_counter;
    @BindView(R.id.location_color)
    LinearLayout location_color;
    @BindView(R.id.layout_welcome)
    LinearLayout layout_welcome;
    @BindView(R.id.tv_location)
    TextView tv_location;
    @BindView(R.id.delivery_to)
    TextView delivery_to;
    @BindView(R.id.user_name)
    TextView tv_name;
    @BindView(R.id.user_mobile)
    TextView tv_mobile;
    @BindView(R.id.welcome_user)
    TextView tv_welcome_user;
    @BindView(R.id.your_points)
    TextView tv_your_points;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.main_toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler_item)
    RecyclerView recyclerView;
    @BindView(R.id.expandableListView)
    ExpandableListView expandableList;
    @BindView(R.id.banner_slider)
    Slider slider;
    @BindView(R.id.order_now)
    Button orderNow;
    @BindView(R.id.layout_refresh_home)
    SwipeRefreshLayout refreshHome;
    @BindView(R.id.tv_timings)
    TextView tv_timings;

    private GridMenuResponse mGridMenuResponse;
    private GridMenuAdapter gridMenuAdapter;
    private List<ExpandedMenuModel> listDataHeader;
    private ArrayList<GridMenuResponse.Datum> mGridMenuResponseList;
    private ArrayList<String> categoryList;
    /* private LatLng currentLocation;*/
    boolean isLogin = false;
    private List<ImageSlider> imageList = new ArrayList<>();
    private HomeBannerResponse homeBannerResponse;
    public static int totalItems = 0;
    private Intent cartCheckIntent;
    private Handler handler;

    //private RateOrderFragment rateOrderFragment;
    //private final int versionCode = 9;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //initializing service for cart check
        initService();

        // initializing all views
        initAllViews();

        // initializing drawer
        initDrawer();

        /*//checkConnection
        checkConnection();*/

    }

    private void blink() {

        handler = new Handler();
        handler.postDelayed(() -> {

            if(tv_timings.getVisibility() == View.INVISIBLE)
                tv_timings.setVisibility(View.VISIBLE);
            else
                tv_timings.setVisibility(View.INVISIBLE);
            blink();
        },600);
    }

    private void initService() {
        cartCheckIntent = new Intent(getApplicationContext(), CartCheckService.class);

        if (!isMyServiceRunning())
            startService(cartCheckIntent);
    }

    private void initAllViews() {
        ButterKnife.bind(this);
        findViewById(R.id.cart_layout).setVisibility(View.GONE);
        Objects.requireNonNull(noConnectionLayout).setOnRefreshListener(() -> {
            findViewById(R.id.layout_progressBar).setVisibility(View.VISIBLE);
            new CheckInternetConnection(this, getApplicationContext()).execute();
            noConnectionLayout.setRefreshing(false);
        });
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open, R.string.close) {
            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                updateUserDetails();
                initDrawer();
            }
        };
        mDrawerToggle.setDrawerIndicatorEnabled(false);
        drawerLayout.addDrawerListener(mDrawerToggle);
        Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_menu, this.getTheme());
        mDrawerToggle.setHomeAsUpIndicator(drawable);
        mDrawerToggle.setToolbarNavigationClickListener(v -> {
            if (drawerLayout.isDrawerVisible(GravityCompat.START)) {
                drawerLayout.closeDrawer(GravityCompat.START);
            } else {
                initDrawer();
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });
        toolbar.setTitle(null);
        //findViewById(R.id.cart).setOnClickListener(v -> switchActivity(CartActivity.class));
    }

    private void initDrawer() {

        initNavigationHeader();
        updateUserDetails();
        prepareListData();
        ExpandableListAdapter mMenuAdapter = new ExpandableListAdapter(this, listDataHeader, AppPreferences.getBonusPts(this));
        expandableList.setAdapter(mMenuAdapter);
        expandableList.setOnGroupClickListener((parent, v, groupPosition, id) -> {

            if (listDataHeader.get(groupPosition).getIconName().equalsIgnoreCase(getString(R.string.order)))
                switchActivity(OrderActivity.class);
            else if (listDataHeader.get(groupPosition).getIconName().equalsIgnoreCase(getString(R.string.profile)))
                switchActivity(ProfileActivity.class);
            else if (listDataHeader.get(groupPosition).getIconName().equalsIgnoreCase(getString(R.string.notifictions)))
                switchActivity(NotificationsActivity.class);
            else if (listDataHeader.get(groupPosition).getIconName().equalsIgnoreCase(getString(R.string.contact_us)))
                switchActivity(ContactUsActivity.class);
            else if (listDataHeader.get(groupPosition).getIconName().equalsIgnoreCase(getString(R.string.about_pkb)))
                switchActivity(AboutPKBActivity.class);
            else if (listDataHeader.get(groupPosition).getIconName().equalsIgnoreCase(getString(R.string.Terms_condtn)))
                switchActivity(TermsConditions.class);
            else if (listDataHeader.get(groupPosition).getIconName().equalsIgnoreCase(getString(R.string.refer_and_earn)))
                switchActivity(ReferNEarnActivity.class);


            if (groupPosition != 3 && groupPosition != listDataHeader.size() - 1)
                drawerLayout.closeDrawer(Gravity.START);
            return false;
        });

    }

    private void checkConnection() {
        //showProgressDialog(getString(R.string.loading), getString(R.string.fetching_details));
        findViewById(R.id.layout_progressBar).setVisibility(View.VISIBLE);
        findViewById(R.id.nestedView).setVisibility(View.GONE);
        new CheckInternetConnection(this, getApplicationContext()).execute();
    }

    @Override
    public void onConnectionChange(boolean isConnected) {
        if (noConnectionLayout != null) {
            if (isConnected) {
                settingAllViews();
                findViewById(R.id.nestedView).setVisibility(View.VISIBLE);
                noConnectionLayout.setVisibility(View.GONE);
            } else {
                findViewById(R.id.nestedView).setVisibility(View.GONE);
                noConnectionLayout.setVisibility(View.VISIBLE);
                findViewById(R.id.layout_progressBar).setVisibility(View.GONE);
            }
        }
    }

    private void settingAllViews() {

        mGridMenuResponseList = new ArrayList<>();
        categoryList = new ArrayList<>();
        orderNow.setOnClickListener(v -> switchToMenu(0, false));
        location_color.setVisibility(View.GONE);
        refreshHome.setOnRefreshListener(() -> {
            new CheckInternetConnection(this, getApplicationContext()).execute();
            refreshHome.setRefreshing(false);
        });

        findViewById(R.id.cart).setOnClickListener(v -> switchToCart());

        findViewById(R.id.layout_pkb_menu).setOnClickListener(v -> switchToMenu(0, false));

        findViewById(R.id.notification).setOnClickListener(v -> switchActivity(NotificationsActivity.class));

        findViewById(R.id.edit).setOnClickListener(v -> {

            Bundle bundle = new Bundle();
            bundle.putBoolean("order_now", false);
            switchActivity(LocationActivity.class, bundle);
        });

        if (AppPreferences.getLocationSession(this))// setting session saved location
            setSavedLocation();
        else // getting Location and distance between pkb and current Location
            estimatingLocation();


        // setting banner on the screen
        getHomeBanner();

        // initializing image slider in home banner
        initImageSlider();

        // updating user details using user_id
        updateUserDetails();

        // initializing grid menu
        initGridMenu();

        //check and refresh FireBase token
        checkAndRefreshFireBase();

    }

    private void setSavedLocation() {

        if (AppPreferences.getLatLong(this).equals("0:0")) {
            delivery_to.setText(getString(R.string.please_edit_your_location));
            tv_location.setText(getString(R.string.not_able_to_get_location));
            location_color.setBackgroundColor(getResources().getColor(R.color.color_stardust));
        } else if (AppPreferences.getSharedPreferencesForAddress(this).getBoolean(AppConstants.WITH_KMS, false)) {
            delivery_to.setText(R.string.delivery_to);
            tv_location.setText(AppPreferences.getLocation(this));
            location_color.setBackgroundColor(getResources().getColor(R.color.color_green));
        } else {
            location_color.setBackgroundColor(getResources().getColor(R.color.torch_red));
            delivery_to.setText(getString(R.string.no_stores_in_the_neighbourhood));
            tv_location.setText(getString(R.string.please_change_your_location));
        }
        //check for unrated orders
        //checkForUnratedOrders();
    }

    private void estimatingLocation() {
        if (isServicesOk())
            getDeviceLocation();
    }

    private void getHomeBanner() {
        ApiInterface apiInterface = ServiceGenerator.createLoginRequest(ApiInterface.class);
        Call<HomeBannerResponse> call = apiInterface.getHomeBanners();
        ArrayList<HomeBannerResponse.Datum> homeBannerResponseArrayList = new ArrayList<>();
        call.enqueue(new Callback<HomeBannerResponse>() {
            @Override
            public void onResponse(@NonNull Call<HomeBannerResponse> call, @NonNull Response<HomeBannerResponse> response) {
                homeBannerResponse = response.body();
                if (homeBannerResponse != null && homeBannerResponse.getStatus()) {
                    homeBannerResponseArrayList.addAll(homeBannerResponse.getData());
                    for (int i = 0; i < homeBannerResponseArrayList.size(); i++) {
                        imageList.add(new ImageSlider(homeBannerResponseArrayList.get(i).getTitle(), homeBannerResponseArrayList.get(i).getImage()));
                        initImageSlider();
                    }
                    AppPreferences.setRestaurantStatus(MainActivity.this,
                            homeBannerResponse.getRestaurantStatus().getStore_status(),
                            homeBannerResponse.getRestaurantStatus().getDescription());
                }
                if(handler!=null)
                    handler.removeCallbacksAndMessages(null);
                if (!AppPreferences.isRestOpen(MainActivity.this)) {

                    layout_welcome.setBackgroundColor(getResources().getColor(R.color.color_gry));
                    orderNow.setBackgroundResource(R.drawable.button_round_grey);
                    orderNow.setTextColor(getResources().getColor(R.color.color_white));
                    findViewById(R.id.cart_layout).setVisibility(View.GONE);
                    blink();
                } else {
                    findViewById(R.id.cart_layout).setVisibility(View.VISIBLE);
                    location_color.setVisibility(View.VISIBLE);
                    layout_welcome.setBackgroundResource(R.drawable.wel_order_background);
                    orderNow.setBackgroundResource(R.drawable.button_ripple_round);
                    orderNow.setTextColor(getResources().getColor(R.color.color_brown));
                    handler=null;
                }
                // setting grid menu
                getMenuItem();

            }

            @Override
            public void onFailure(@NonNull Call<HomeBannerResponse> call, @NonNull Throwable t) {
            }
        });
    }

    private void initImageSlider() {

        Slider.init(new PicassoImageLoadingService(this));
        slider.setAdapter(new MySlideAdapter(imageList));
        slider.setLoopSlides(true);
        slider.setInterval(4000);
        slider.setAnimateIndicators(false);
        slider.setOnSlideClickListener(position -> switchToMenu(0, false));
    }

    private void initGridMenu() {

        gridMenuAdapter = new GridMenuAdapter(this, mGridMenuResponseList, this);
        recyclerView.setHasFixedSize(true);
        int numberOfColumns = 2;
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, numberOfColumns);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(gridMenuAdapter);
    }

    private void checkAndRefreshFireBase() {
        /*Check FireBase Token  and Refresh FireBaseToken*/
        if (AppPreferences.getFirebaseToken(this) == null)
            isLogin = true;
        else if (AppPreferences.getFirebaseToken(this) != null &&
                AppPreferences.getFirebaseToken(this).equals(AppPreferences.getRefreshFirebaseToken(this))) {
            isLogin = false;
        }

        if (isLogin)
            getFirBaseToken();
    }

    private boolean isMyServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (CartCheckService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void initNavigationHeader() {
        tv_name.setText(AppPreferences.getUserName(this));
        tv_mobile.setText(("+91- " + AppPreferences.getMobileNo(this)));
        tv_welcome_user.setText(("Welcome " + AppPreferences.getUserName(this)));
        tv_your_points.setText(("You have " + AppPreferences.getBonusPts(this) + " points in your wallet"));
    }

    private void prepareListData() {

        listDataHeader = new ArrayList<>();
        listDataHeader.add(new ExpandedMenuModel(getString(R.string.home), R.drawable.home));
        listDataHeader.add(new ExpandedMenuModel(getString(R.string.profile), R.drawable.profile));
        listDataHeader.add(new ExpandedMenuModel(getString(R.string.order), R.drawable.order));
        listDataHeader.add(new ExpandedMenuModel(getString(R.string.pkb_loyality_bonus), R.drawable.bonus));
        listDataHeader.add(new ExpandedMenuModel(getString(R.string.refer_and_earn), R.drawable.refer_earn));
        listDataHeader.add(new ExpandedMenuModel(getString(R.string.notifictions), R.drawable.notification_black));
        listDataHeader.add(new ExpandedMenuModel(getString(R.string.contact_us), R.drawable.phone_black));
        listDataHeader.add(new ExpandedMenuModel(getString(R.string.about_pkb), R.drawable.about_pkb));
        listDataHeader.add(new ExpandedMenuModel(getString(R.string.terms_condition), R.drawable.terms));
        listDataHeader.add(new ExpandedMenuModel(getString(R.string.lisNo), R.drawable.ic_fssai));
    }


    private void switchToMenu(int position, boolean isGrid) {
        Log.i("user", AppPreferences.getUser_id(this));
        Bundle bundle = new Bundle();
        bundle.putBoolean("order_now", true);
        bundle.putStringArrayList("category_list", categoryList);
        if (categoryList.size() > 0) {
            if (isGrid)
                bundle.putInt("item_position", position + 1);
            if (!AppPreferences.isRestOpen(this))
                displayToast(AppPreferences.getDescription(this));
            else if (!AppPreferences.isWithinKms(this))
                switchActivity(LocationActivity.class, bundle);
            else
                switchActivity(MenuActivity.class, bundle);
        }
    }

    private void switchToCart() {
        if (!AppPreferences.isRestOpen(this))
            displayToast(AppPreferences.getDescription(this));
        else if (!AppPreferences.isWithinKms(this))
            switchActivity(LocationActivity.class);
        else
            switchActivity(CartActivity.class);
    }

    private void setNotificationCounter() {
        if (totalItems == 0) {
            notification_counter.setVisibility(View.GONE);
        } else {
            notification_counter.setVisibility(View.VISIBLE);
            notification_counter.setText(String.valueOf(totalItems));
        }
    }

    @SuppressWarnings("unchecked")
    private void getDeviceLocation() {
        FusedLocationProviderClient fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        try {
            final Task location = fusedLocationProviderClient.getLastLocation();
            location.addOnCompleteListener(task -> {
                if (task.isSuccessful() && task.getResult() != null) {
                    Location curLocation = (Location) task.getResult();
                    Log.i("Location", "Latitude: " + Objects.requireNonNull(curLocation).getLatitude() +
                            "\n Longitude: " + curLocation.getLongitude());
                    getLocation(Objects.requireNonNull(curLocation).getLatitude(), curLocation.getLongitude());
                } else {
                    delivery_to.setText(getString(R.string.please_edit_your_location));
                    tv_location.setText(getString(R.string.not_able_to_get_location));
                    AppPreferences.saveLocation(MainActivity.this, getString(R.string.not_able_to_get_location), false, true, "0:0");
                    findViewById(R.id.layout_progressBar).setVisibility(View.GONE);
                    //check for unrated orders
                    //checkForUnratedOrders();
                }
            });
        } catch (SecurityException e) {
            Log.i("Security Exception", e.toString());
        }
    }

    private boolean isServicesOk() {

        int available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(MainActivity.this);

        if (available == ConnectionResult.SUCCESS) {
            //everything is fine and the user can make map requests
            return true;
        } else if (GoogleApiAvailability.getInstance().isUserResolvableError(available)) {
            //an error occurred but we can resolve it
            return false;
        }
        return false;
    }

    private void getLocation(double latitude, double longitude) {

        ApiInterface apiInterface = ServiceGeneratorGoogleApi.createClient(ApiInterface.class);
        Call<AddressResponse> call = apiInterface.getLocation(latitude + "," + longitude, getResources().getString(R.string.api_key));
        call.enqueue(new Callback<AddressResponse>() {
            @Override
            public void onResponse(@NonNull Call<AddressResponse> call, @NonNull Response<AddressResponse> response) {
                AddressResponse addressResponse = response.body();

                if (addressResponse != null && !addressResponse.getStatus().equals("ZERO_RESULTS")) {
                    String address = addressResponse.getResults().get(0).getFormattedAddress();
                    getDistance(latitude, longitude, address);
                } else {
                    displayToast(R.string.not_able_to_get_location);
                    findViewById(R.id.layout_progressBar).setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(@NonNull Call<AddressResponse> call, @NonNull Throwable t) {
            }
        });
    }

    private void getDistance(double destLat, double destLong, String curAddress) {
        findViewById(R.id.layout_progressBar).setVisibility(View.VISIBLE);
        ApiInterface apiInterface = ServiceGeneratorGoogleApi.createClient(ApiInterface.class);
        Call<DistanceResponse> call = apiInterface.getDistance(Double.toString(AppConstants.pkbLat) + "," + Double.toString(AppConstants.pkbLong),
                destLat + "," + destLong, false, "driving", true, getResources().getString(R.string.api_key));
        call.enqueue(new Callback<DistanceResponse>() {
            @Override
            public void onResponse(@NonNull Call<DistanceResponse> call, @NonNull Response<DistanceResponse> response) {
                DistanceResponse distanceResponse = response.body();
                if (distanceResponse != null) {
                    String distance = distanceResponse.getRoutes().get(0).getLegs().get(0).getDistance().getText().replaceAll("[^.0123456789]", "");
                    String latLng = String.valueOf(destLat) + ":" + String.valueOf(destLong);
                    Log.d("distancePkb", distance);
                    if (Double.parseDouble(distance) <= 7) {
                        delivery_to.setText(R.string.delivery_to);
                        tv_location.setText(curAddress);
                        location_color.setBackgroundColor(getResources().getColor(R.color.color_green));
                        AppPreferences.saveLocation(MainActivity.this, curAddress, true, false, latLng);
                    } else {
                        location_color.setBackgroundColor(getResources().getColor(R.color.torch_red));
                        delivery_to.setText(getString(R.string.no_stores_in_the_neighbourhood));
                        tv_location.setText(getString(R.string.please_change_your_location));
                        AppPreferences.saveLocation(MainActivity.this, curAddress, false, false, latLng);

                    }
                    findViewById(R.id.layout_progressBar).setVisibility(View.GONE);
                    //check for unrated orders
                    //checkForUnratedOrders();
                }
            }

            @Override
            public void onFailure(@NonNull Call<DistanceResponse> call, @NonNull Throwable t) {
            }
        });
    }

    private void getMenuItem() {
        ApiInterface apiInterface = ServiceGenerator.createLoginRequest(ApiInterface.class);
        Call<GridMenuResponse> call = apiInterface.getGridMenuResponse();
        call.enqueue(new Callback<GridMenuResponse>() {
            @Override
            public void onResponse(@NonNull Call<GridMenuResponse> call, @NonNull Response<GridMenuResponse> response) {
                findViewById(R.id.layout_progressBar).setVisibility(View.GONE);
                mGridMenuResponse = response.body();
                if (mGridMenuResponse != null && mGridMenuResponse.getStatus()) {
                    mGridMenuResponseList.clear();
                    mGridMenuResponseList.addAll(mGridMenuResponse.getData());

                    categoryList.add("Offers");
                    StringBuilder allCategoryIds = new StringBuilder();
                    for (GridMenuResponse.Datum g : mGridMenuResponseList) {
                        categoryList.add(g.getCategoryName());
                        allCategoryIds.append(g.getId()).append("#");
                    }
                    AppPreferences.saveCategory_ids(MainActivity.this, allCategoryIds.toString());
                    gridMenuAdapter.notifyDataSetChanged();

                }

            }

            @Override
            public void onFailure(@NonNull Call<GridMenuResponse> call, @NonNull Throwable t) {
                displayToast(R.string.some_thing_wrong);
                findViewById(R.id.layout_progressBar).setVisibility(View.GONE);
            }
        });
    }

    /*
       Get FireBase Token and Send through Api
        */
    private void getFirBaseToken() {

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, instanceIdResult -> {
            String fireBaseToken = instanceIdResult.getToken();
            ApiInterface apiInterface = ServiceGenerator.createRequestApi(ApiInterface.class, AppPreferences.getAuth_token(this));
            Call<SocialLoginResponse> call = apiInterface.getSocialLogin(fireBaseToken);
            call.enqueue(new Callback<SocialLoginResponse>() {
                @Override
                public void onResponse(@NonNull Call<SocialLoginResponse> call, @NonNull Response<SocialLoginResponse> response) {

                    SocialLoginResponse socialLoginResponse = response.body();
                    if (socialLoginResponse != null) {
                        if (socialLoginResponse.getStatus()) {
                            AppPreferences.saveFirebaseToken(getApplicationContext(), fireBaseToken);
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<SocialLoginResponse> call, @NonNull Throwable t) {
                }
            });

        });
    }

    private void setCartCounter(int totalItems) {
        if (totalItems == 0) {
            tv_cart_counter.setVisibility(View.GONE);
        } else {
            tv_cart_counter.setVisibility(View.VISIBLE);
            tv_cart_counter.setText(String.valueOf(totalItems));
        }
    }

    @Override
    public void myOnClick(String id, int position) {
        switchToMenu(position, true);
    }

    public void nothing(View v) {
    }

   /* private void alertNoOrder() {
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.setContentView(R.layout.layout_no_order);
        dialog.setCancelable(false);
        dialog.findViewById(R.id.close_no_order).setOnClickListener(v -> {
            dialog.dismiss();
            ableToOrder();
        });
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        if (!isFinishing())
            dialog.show();
    }*/
  /*  private void alertIfNotInSeven() {

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setCancelable(false);
        builder.setTitle(getString(R.string.you_are_not_in_7kms_dialog));
        builder.setMessage(getString(R.string.contact_us_for_further_details));
        builder.setPositiveButton("OK", (dialog, which) -> switchActivity(ContactUsActivity.class));
        builder.setNegativeButton("Cancel", null);
        if (!isFinishing())
            builder.show();
    }*/

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            setNotificationCounter();
        }
    };


    /*private void checkForUnratedOrders(){
        String userId = AppPreferences.getUser_id(this);
        ApiInterface apiInterface = ServiceGenerator.createLoginRequest(ApiInterface.class);
        Call<OrderListResponse> call = apiInterface.getOrderList(userId);
        call.enqueue(new Callback<OrderListResponse>() {
            @Override
            public void onResponse(@NonNull Call<OrderListResponse> call, @NonNull Response<OrderListResponse> response) {
                OrderListResponse orderListResponse = response.body();
                if (orderListResponse != null && orderListResponse.getStatus() && orderListResponse.getData() != null) {
                    if (orderListResponse.getData().size() > 0) {
                        ArrayList<OrderListResponse.Datum> orderList = new ArrayList<>(orderListResponse.getData());

                        for (OrderListResponse.Datum oD : orderList) {
                            if (oD.getOrderStatus().equals("4")) {
                                if (oD.getFeedbackId() == null) {
                                    alertForOrderRating(oD.getOrderNo(),oD.getId());
                                    break;
                                }
                            }
                        }

                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<OrderListResponse> call, @NonNull Throwable t) {

            }
        });
    }

    private void alertForOrderRating(String no,String id) {

        if(rateOrderFragment == null) {
            rateOrderFragment = RateOrderFragment.getInstance(no, id);
            rateOrderFragment.show(getSupportFragmentManager(), rateOrderFragment.getTag());
        }
    }*/


    @Override
    protected void onPause() {
        this.unregisterReceiver(broadcastReceiver);
        super.onPause();

    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(Gravity.START))
            drawerLayout.closeDrawer(Gravity.START);
        else
            super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //checkConnection
        checkConnection();
        setNotificationCounter();
        if (AppPreferences.getLocationSession(this))
            setSavedLocation();
        updateUserDetails();
        tv_your_points.setText(("You have " + AppPreferences.getBonusPts(this) + " points in your wallet"));
        setCartCounter(AppDatabases.getOrderItemsDAO(getApplicationContext()).getTotalOrderItems());
        AppPreferences.setPickUp(this, false);
        registerReceiver(broadcastReceiver, new IntentFilter(INTENT_FILTER));
        LocalBroadcastManager.getInstance(MainActivity.this).registerReceiver(broadcastReceiver, new IntentFilter(INTENT_FILTER));
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("status", "stopped");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("status", "Destroy");
        stopService(cartCheckIntent);
    }
}
