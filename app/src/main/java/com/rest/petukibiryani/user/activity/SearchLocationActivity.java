package com.rest.petukibiryani.user.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.rest.petukibiryani.user.R;
import com.rest.petukibiryani.user.adapter.LocationListAdapter;
import com.rest.petukibiryani.user.helper.AppDatabases;
import com.rest.petukibiryani.user.helper.AppPreferences;
import com.rest.petukibiryani.user.listeners.LocationsListDAO;
import com.rest.petukibiryani.user.listeners.MyClickListener;
import com.rest.petukibiryani.user.models.Locations;
import com.rest.petukibiryani.user.models.LocationsList;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchLocationActivity extends AppCompatActivity implements MyClickListener {

    @BindView(R.id.rv_locations)
    RecyclerView rvLocations;
    @BindView(R.id.prev_searches)
    TextView prevSearches;
    private LocationsListDAO locationListDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_location);
        ButterKnife.bind(this);
        locationListDAO = AppDatabases.getLocationsListDAO(this);
        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder().setCountry("IN").build();
        autocompleteFragment.setFilter(typeFilter);
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                String latLng = String.valueOf(place.getLatLng().latitude + ":" + place.getLatLng().longitude);
                AppPreferences.setLatLong(SearchLocationActivity.this, latLng);
                if (AppDatabases.getLocationsListDAO(getApplicationContext()).getExistingData(place.getLatLng().latitude, place.getLatLng().longitude) == 0) {
                    AppDatabases.getLocationsListDAO(getApplicationContext()).insert(new LocationsList(Objects.requireNonNull(place.getAddress()).toString(), place.getLatLng().latitude, place.getLatLng().longitude));
                }
                finish();
            }

            @Override
            public void onError(Status status) {
            }
        });

        initRecyclerView();
    }

    private void initRecyclerView() {

        List<LocationsList> locationsLists = locationListDAO.getAllData();

        if (locationsLists.size() > 0) {
            List<Locations> locations = new ArrayList<>();

            if (locationsLists.size() > 8) {
                locationListDAO.delete(locationsLists.get(0));
                locationsLists = locationListDAO.getAllData();
            }
            for (int i = locationsLists.size() - 1; i >= 0; i--) {
                locations.add(new Locations(locationsLists.get(i).getAddress(),
                        locationsLists.get(i).getLatitude(),
                        locationsLists.get(i).getLongitude()));
            }

            LocationListAdapter locationListAdapter = new LocationListAdapter(this, locations, this);
            rvLocations.setLayoutManager(new LinearLayoutManager(this));
            rvLocations.setHasFixedSize(true);
            rvLocations.setAdapter(locationListAdapter);
        } else
            prevSearches.setVisibility(View.GONE);

    }

    @Override
    public void myOnClick(String latLng, int position) {
        String latLongs[] = latLng.split(":");
        int count = locationListDAO.getExistingData(Double.parseDouble(latLongs[1]), Double.parseDouble(latLongs[2]));
        if (count == 0) {
            locationListDAO.insert(new LocationsList(latLongs[0], Double.parseDouble(latLongs[1]), Double.parseDouble(latLongs[2])));
        } else {
            locationListDAO.delete(locationListDAO.getLatLongData(Double.parseDouble(latLongs[1]), Double.parseDouble(latLongs[2])));
            locationListDAO.insert(new LocationsList(latLongs[0], Double.parseDouble(latLongs[1]), Double.parseDouble(latLongs[2])));
        }
        AppPreferences.setLatLong(SearchLocationActivity.this, latLongs[1] + ":" + latLongs[2]);
        finish();
    }
}
