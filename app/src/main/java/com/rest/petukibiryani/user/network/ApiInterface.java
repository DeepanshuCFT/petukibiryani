package com.rest.petukibiryani.user.network;

import com.rest.petukibiryani.user.responseModel.AboutUs;
import com.rest.petukibiryani.user.responseModel.AddressResponse;
import com.rest.petukibiryani.user.responseModel.CartReminderResponse;
import com.rest.petukibiryani.user.responseModel.CheckoutResponse;
import com.rest.petukibiryani.user.responseModel.ContactUs;
import com.rest.petukibiryani.user.responseModel.DistanceResponse;
import com.rest.petukibiryani.user.responseModel.FeedBack;
import com.rest.petukibiryani.user.responseModel.GridMenuResponse;
import com.rest.petukibiryani.user.responseModel.HomeBannerResponse;
import com.rest.petukibiryani.user.responseModel.LoginResponseModel;
import com.rest.petukibiryani.user.responseModel.NotificationResponseModel;
import com.rest.petukibiryani.user.responseModel.OffersLoyaltyPoints;
import com.rest.petukibiryani.user.responseModel.OffersResponse;
import com.rest.petukibiryani.user.responseModel.OrderListResponse;
import com.rest.petukibiryani.user.responseModel.OtpResponseModel;
import com.rest.petukibiryani.user.responseModel.OrderDetailsResponse;


import com.rest.petukibiryani.user.responseModel.ReferCodeResponse;
import com.rest.petukibiryani.user.responseModel.RegistrationResponse;
import com.rest.petukibiryani.user.responseModel.SocialLoginResponse;
import com.rest.petukibiryani.user.responseModel.TabMenuResponse;
import com.rest.petukibiryani.user.responseModel.TermsaNdCondition;
import com.rest.petukibiryani.user.responseModel.UpdateResponse;
import com.rest.petukibiryani.user.responseModel.UserDetailsResponse;
import com.rest.petukibiryani.user.responseModel.UserProfileResponse;

import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PartMap;
import retrofit2.http.Query;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("api/v2/App/auth/user_login/")
    Call<LoginResponseModel> loginUser(@Field("mobile") String mobile);

    @FormUrlEncoded
    @POST("api/v2/App/auth/user_registration/")
    Call<RegistrationResponse> register_User(@Field("mobile") String mobile, @Field("name") String name, @Field("refer_by") String referCode);

    @FormUrlEncoded
    @POST("api/v2/App/auth/verify_user")
    Call<OtpResponseModel> Otp_Verify(@Field("user_id") String user_id, @Field("otp") String otp);

    @FormUrlEncoded
    @POST("api/v2/App/User/userDetails")
    Call<UserDetailsResponse> getUserDetails(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("api/v2/App/User/get_user_profile")
    Call<UserProfileResponse> getUserProfile(@Field("user_id") String user_id);

    @GET("api/v2/App/Home")
    Call<HomeBannerResponse> getHomeBanners();

    @FormUrlEncoded
    @POST("api/v2/App/Feedback/user_feedback")
    Call<FeedBack> setFeedBack(@Field("order_id") String order_id, @Field("message") String message, @Field("rating") Float rating);

    @GET("api/v2/App/Category/index")
    Call<GridMenuResponse> getGridMenuResponse();


    @GET("api/v2/App/Home/promotional_banner")
    Call<OffersResponse> getOfferResponse();


    @Multipart
    @POST("api/v2/App/Order/saveOrder")
    Call<CheckoutResponse> setCheckoutResponse(@PartMap Map<String, RequestBody> partMap);

    @Multipart
    @POST("api/v2/App/User/update_user_profile")
    Call<UpdateResponse> setUpdateResponse(@PartMap Map<String, RequestBody> partMap);

    @FormUrlEncoded
    @POST("api/v2/App/Category/subcategory/")
    Call<TabMenuResponse> getTabMenuResponse(@Field("category_id") String category_id);

    @GET("api/v2/App/Page/page_detail/2")
    Call<AboutUs> getAboutUs();

    @GET("api/v2/App/Page/contact")
    Call<ContactUs> getContactUs();

    @GET("api/v2/App/Page/page_detail/4")
    Call<TermsaNdCondition> getTermsCondition();

    @FormUrlEncoded
    @POST("api/v2/App/Home/get_offers")
    Call<OffersLoyaltyPoints> getOffer(@Field("current_date") String current_date, @Field("current_time") String current_time);

    @FormUrlEncoded
    @POST("api/v2/App/Order/details/")
    Call<OrderDetailsResponse> getOrderDetails(@Field("order_id") String order_id);

    @FormUrlEncoded
    @POST("api/v2/App/Order/orderList/")
    Call<OrderListResponse> getOrderList(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("api/v2/App/Notification/updateToken")
    Call<SocialLoginResponse> getSocialLogin(@Field("device_token") String device_token);

    @Headers("Content-Type: application/json")
    @POST("api/v2/App/User/getReferCode")
    Call<ReferCodeResponse> getReferCode(@Body String user_id);

    @FormUrlEncoded
    @POST("api/v2/App/Notification/user_notifications")
    Call<NotificationResponseModel> getNotificationList(@Field("user_id") String user_id);

    @GET("api/geocode/json")
    Call<AddressResponse> getLocation(@Query("latlng") String latlog, @Query("key") String key);

    @GET("api/directions/json")
    Call<DistanceResponse> getDistance(@Query("origin") String origin, @Query("destination") String destination, @Query("sensor") Boolean sensor,
                                       @Query("mode") String mode, @Query("alternatives") Boolean alternatives, @Query("key") String key);

    @FormUrlEncoded
    @POST("api/v2/App/Notification/cartReminder")
    Call<CartReminderResponse> setReminder(@Field("user_id") String user_id);
}
