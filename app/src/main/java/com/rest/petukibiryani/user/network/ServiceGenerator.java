package com.rest.petukibiryani.user.network;

import com.google.gson.GsonBuilder;
import com.rest.petukibiryani.user.network.interceptor.LoginInterceptor;
import com.rest.petukibiryani.user.network.interceptor.NormalApiInterceptor;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static java.sql.Types.TIME;

public class ServiceGenerator {
    //public static final String BASE_URL = "http://192.168.1.14/pkb/"; //Local_URL
    public static final String BASE_URL = "http://www.petukibiryani.com/pos/";//Live URL

    private static OkHttpClient.Builder httpClient;

    private static Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()));

    private static void createClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        httpClient.connectTimeout(TIME, TimeUnit.SECONDS);
        httpClient.readTimeout(TIME, TimeUnit.SECONDS);
    }

    public static <S> S createLoginRequest(Class<S> serviceClass) {
        createClient();
        httpClient.addInterceptor(new LoginInterceptor());
        OkHttpClient okHttpClient = httpClient.build();
        Retrofit retrofit = builder.client(okHttpClient).build();
        return retrofit.create(serviceClass);
    }

 //   private static Retrofit retrofit = null;

    /*public static Retrofit getResponse() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }*/

    public static <S> S createRequestApi(Class<S> serviceClass, String token) {
        createClient();
        httpClient.addInterceptor(new NormalApiInterceptor(token));
        OkHttpClient okHttpClient = httpClient.build();
        Retrofit retrofit = builder.client(okHttpClient).build();
        return retrofit.create(serviceClass);
    }


}
