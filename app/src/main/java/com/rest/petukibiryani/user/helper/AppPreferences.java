package com.rest.petukibiryani.user.helper;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.rest.petukibiryani.user.responseModel.OtpResponseModel;
import com.rest.petukibiryani.user.responseModel.UserDetailsResponse;

import java.util.ArrayList;

public class AppPreferences {

    public static void saveUserDetails(Context context, String userData){
        OtpResponseModel.User_Details user_details = new Gson().fromJson(userData, OtpResponseModel.User_Details.class);

        SharedPreferences.Editor editor = context.getSharedPreferences(AppConstants.PREFERENCE_USER, Context.MODE_PRIVATE).edit();
        editor.putString(AppConstants.NAME, user_details.getName());
        editor.putString(AppConstants.BONUS_PTS, user_details.getPoints());
        editor.putString(AppConstants.MOBILE, user_details.getMobile());
        editor.putString(AppConstants.AUTH, user_details.getAuth_token());
        editor.putBoolean(AppConstants.EXIT, true);
        editor.apply();
    }

    public static void setUserName(Context context,String name){
        SharedPreferences.Editor editor = context.getSharedPreferences(AppConstants.PREFERENCE_USER, Context.MODE_PRIVATE).edit();
        editor.putString(AppConstants.NAME, name);
        editor.apply();
    }


    /*public static void setOrderTimeAndDate(Context context,String date,String time,boolean isPickUp){
        SharedPreferences.Editor editor = context.getSharedPreferences(AppConstants.PREFERENCE_USER, Context.MODE_PRIVATE).edit();
        editor.putString(AppConstants.DATE, date);
        editor.putString(AppConstants.TIME, time);
        editor.putBoolean(AppConstants.PICK_UP, isPickUp);
        editor.putBoolean(AppConstants.ORDER_LATER,true);
        editor.apply();
    }*/

    public static void setPickUp(Context context,boolean isPickUp){
        SharedPreferences.Editor editor = context.getSharedPreferences(AppConstants.PREFERENCE_USER, Context.MODE_PRIVATE).edit();
        //editor.putBoolean(AppConstants.ORDER_LATER,false);
        editor.putBoolean(AppConstants.PICK_UP, isPickUp);
        editor.apply();
    }

    public static void setRestaurantStatus(Context context,boolean isRestOpen,String description){
        SharedPreferences.Editor editor = context.getSharedPreferences(AppConstants.PREFERENCE_USER, Context.MODE_PRIVATE).edit();
        editor.putBoolean(AppConstants.OPEN_CLOSE,isRestOpen);
        editor.putString(AppConstants.DESCRIPTION,description);
        editor.apply();
    }
    public static String getDescription(Context context){
        return context.getSharedPreferences(AppConstants.PREFERENCE_USER,Context.MODE_PRIVATE).getString(AppConstants.DESCRIPTION,"");
    }
    public static Boolean isRestOpen(Context context){
        return context.getSharedPreferences(AppConstants.PREFERENCE_USER,Context.MODE_PRIVATE).getBoolean(AppConstants.OPEN_CLOSE,true);
    }

  /*  public static String getOrderTime(Context context){
        return context.getSharedPreferences(AppConstants.PREFERENCE_USER,Context.MODE_PRIVATE).getString(AppConstants.TIME,"");
    }

    public static String getOrderDate(Context context){
        return context.getSharedPreferences(AppConstants.PREFERENCE_USER,Context.MODE_PRIVATE).getString(AppConstants.DATE,"");
    }*/

    public static Boolean isPickUp(Context context){
        return context.getSharedPreferences(AppConstants.PREFERENCE_USER,Context.MODE_PRIVATE).getBoolean(AppConstants.PICK_UP,false);
    }

    /*public static Boolean getOrderLater(Context context){
        return context.getSharedPreferences(AppConstants.PREFERENCE_USER,Context.MODE_PRIVATE).getBoolean(AppConstants.ORDER_LATER,false);
    }
*/
    public static String getUserName(Context context) {
        return context.getSharedPreferences(AppConstants.PREFERENCE_USER, Context.MODE_PRIVATE).getString(AppConstants.NAME, "");
    }

    public static String getBonusPts(Context context) {
        return context.getSharedPreferences(AppConstants.PREFERENCE_USER, Context.MODE_PRIVATE).getString(AppConstants.BONUS_PTS, "");
    }

    public static String getMobileNo(Context context) {
        return context.getSharedPreferences(AppConstants.PREFERENCE_USER, Context.MODE_PRIVATE).getString(AppConstants.MOBILE, "");
    }


    public static String getAuth_token(Context context) {
        return context.getSharedPreferences(AppConstants.PREFERENCE_USER, Context.MODE_PRIVATE).getString(AppConstants.AUTH, null);
    }

    public static void saveUserID(Context context, String userID) {

        SharedPreferences.Editor editor = context.getSharedPreferences(AppConstants.PREFERENCE_USER, Context.MODE_PRIVATE).edit();
        editor.putString(AppConstants.USER_ID, userID);
        editor.apply();

    }

    public static String getUser_id(Context context) {
        return context.getSharedPreferences(AppConstants.PREFERENCE_USER, Context.MODE_PRIVATE).getString(AppConstants.USER_ID, "");
    }

    public static void saveCategory_ids(Context context, String category_ids) {

        SharedPreferences.Editor editor = context.getSharedPreferences(AppConstants.PREFERENCE_USER, Context.MODE_PRIVATE).edit();
        editor.putString(AppConstants.CATEGORY_ID, category_ids);
        editor.apply();

    }

    public static SharedPreferences getSharedPreferencesForCategoryId(Context context){
        return context.getSharedPreferences(AppConstants.PREFERENCE_USER,Context.MODE_PRIVATE);
    }



    public static void saveLocation(Context context, String address, boolean withInKms,boolean session,String latLng) {

        SharedPreferences.Editor editor = context.getSharedPreferences(AppConstants.PREFERENCE_USER, Context.MODE_PRIVATE).edit();
        editor.putString(AppConstants.PLACE, address);
        editor.putBoolean(AppConstants.WITH_KMS, withInKms);
        editor.putBoolean(AppConstants.SESSION,session);
        editor.putString(AppConstants.LAT_LONG,latLng);
        editor.apply();
    }

    public static void setLatLong(Context context,String latLng){
        SharedPreferences.Editor editor = context.getSharedPreferences(AppConstants.PREFERENCE_USER, Context.MODE_PRIVATE).edit();
        editor.putString(AppConstants.LAT_LONG,latLng);
        editor.putBoolean(AppConstants.SESSION,true);
        editor.apply();
    }

    public static void setSessionFalse(Context context){
        SharedPreferences.Editor editor = context.getSharedPreferences(AppConstants.PREFERENCE_USER, Context.MODE_PRIVATE).edit();
        editor.putBoolean(AppConstants.SESSION,false);
        editor.putBoolean(AppConstants.WITH_KMS,false);
        editor.apply();
    }

    public static String getLocation(Context context) {
        return context.getSharedPreferences(AppConstants.PREFERENCE_USER, Context.MODE_PRIVATE).getString(AppConstants.PLACE, null);
    }

    public static String getLatLong(Context context) {
        return context.getSharedPreferences(AppConstants.PREFERENCE_USER, Context.MODE_PRIVATE).getString(AppConstants.LAT_LONG, null);
    }


    public static boolean isWithinKms(Context context) {
        return context.getSharedPreferences(AppConstants.PREFERENCE_USER, Context.MODE_PRIVATE).getBoolean(AppConstants.WITH_KMS, false);
    }

    public static boolean getLocationSession(Context context) {
        return context.getSharedPreferences(AppConstants.PREFERENCE_USER, Context.MODE_PRIVATE).getBoolean(AppConstants.SESSION, false);
    }

    public static SharedPreferences getSharedPreferencesForAddress(Context context){
        return context.getSharedPreferences(AppConstants.PREFERENCE_USER,Context.MODE_PRIVATE);
    }



    public static void saveFirebaseToken(Context context, String userData) {
        SharedPreferences.Editor editor = context.getSharedPreferences(AppConstants.PREFERENCE_USER, Context.MODE_PRIVATE).edit();
        editor.putString(AppConstants.FIREBASE_TOKEN, userData);
        editor.apply();
    }

    public static String getFirebaseToken(Context context) {
        return context.getSharedPreferences(AppConstants.PREFERENCE_USER, Context.MODE_PRIVATE).getString(AppConstants.FIREBASE_TOKEN, null);
    }


    public static void saveRefreshFirebaseToken(Context context, String userData) {
        SharedPreferences.Editor editor = context.getSharedPreferences(AppConstants.PREFERENCE_USER, Context.MODE_PRIVATE).edit();
        editor.putString(AppConstants.REFERSH_FIREBASE_TOKEN, userData);
        editor.apply();
    }

    public static String getRefreshFirebaseToken(Context context) {
        return context.getSharedPreferences(AppConstants.PREFERENCE_USER, Context.MODE_PRIVATE).getString(AppConstants.REFERSH_FIREBASE_TOKEN, null);
    }

    public static void saveBonusPts(Context context, ArrayList<UserDetailsResponse.Datum> datumArrayList ) {

        SharedPreferences.Editor editor = context.getSharedPreferences(AppConstants.PREFERENCE_USER, Context.MODE_PRIVATE).edit();
        editor.putString(AppConstants.BONUS_PTS, datumArrayList.get(0).getPoints());
        editor.apply();
    }

     public static void clearData(Context context){
         SharedPreferences preferences = context.getSharedPreferences(AppConstants.IS_LOGIN, Context.MODE_PRIVATE);
         SharedPreferences.Editor editor = preferences.edit();
         editor.clear();
         editor.apply();
     }

}
