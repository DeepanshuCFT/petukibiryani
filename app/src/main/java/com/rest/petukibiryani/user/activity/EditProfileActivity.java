package com.rest.petukibiryani.user.activity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.EditText;

import com.rest.petukibiryani.user.R;
import com.rest.petukibiryani.user.helper.AppPreferences;
import com.rest.petukibiryani.user.network.ApiInterface;
import com.rest.petukibiryani.user.network.ServiceGenerator;
import com.rest.petukibiryani.user.responseModel.UpdateResponse;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends BaseActivity {

    @BindView(R.id.edit_username)
    EditText editUsername;
    @BindView(R.id.edit_phone)
    EditText editPhoneNo;
    @BindView(R.id.edit_email)
    EditText editEmailId;
    @BindView(R.id.edit_birthday)
    EditText editBirthday;
    @BindView(R.id.edit_anniversary)
    EditText editAnniversary;
    /*@BindView(R.id.check_veg)
    CheckBox checkVeg;
    @BindView(R.id.check_non_veg)
    CheckBox checkNonVeg;*/
    private HashMap<String, RequestBody> map = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        init();
    }

    private void init() {
        ButterKnife.bind(this);
        initializeToolbar(getResources().getString(R.string.profile), R.drawable.back);
        findViewById(R.id.save_profile).setOnClickListener(v -> updateProfile());
        setDateField(editBirthday);
        setDateField(editAnniversary);
    }

    private void updateProfile() {

        String username = getTrimmedString(editUsername);
        String email = getTrimmedString(editEmailId);
        String mobile = getTrimmedString(editPhoneNo);
        String userId = AppPreferences.getUser_id(this);
        /*String preferences = null;

        if (checkVeg.isChecked() && checkNonVeg.isChecked()) {
            preferences = "Vegetarian,Non-Vegetarian";
        } else if (checkVeg.isChecked())
            preferences = "Vegetarian";
        else if (checkNonVeg.isChecked())
            preferences = "Non-Vegetarian";*/

        String birthday = formatDate(getTrimmedString(editBirthday));
        String anniversary = formatDate(getTrimmedString(editAnniversary));
        boolean flag = true;

        if (username.isEmpty()) {
            displayToast(getString(R.string.please_enter_your_name));
            flag = false;
        }
        else if (!username.matches("^[a-zA-Z\\s]+$")) {
            displayToast(getString(R.string.enter_valid_name));
            flag = false;
        }else if (email.isEmpty()) {
            displayToast(getString(R.string.please_enter_your_email));
            flag = false;
        } else if (!email.matches("^([a-zA-Z0-9_\\-.]+)@([a-zA-Z0-9_\\-.]+)\\.([a-zA-Z]{2,5})$")) {
            displayToast(getString(R.string.please_enter_valid_email));
            flag = false;
        } /*else if (preferences == null) {
            displayToast(getString(R.string.please_select_any_preference));
            flag = false;
        } */else if (birthday.equals("0000-00-00")) {
            displayToast(getString(R.string.please_submit_your_birthday));
            flag = false;
        }

        if (flag) {

            String name = ("" + username.charAt(0)).toUpperCase() + username.substring(1);
            RequestBody rb_name = createPartFromString(name);
            RequestBody rb_email = createPartFromString(email);
            RequestBody rb_mobile = createPartFromString(mobile);
            RequestBody rb_user_id = createPartFromString(userId);
            RequestBody rb_birthday = createPartFromString(birthday);
            RequestBody rb_anniversary = createPartFromString(anniversary);
            //RequestBody rb_preferences = createPartFromString(preferences);

            map.put("name", rb_name);
            map.put("mobile", rb_mobile);
            map.put("email", rb_email);
            map.put("user_id", rb_user_id);
            //map.put("preferences", rb_preferences);
            map.put("birthday", rb_birthday);
            map.put("anniversary", rb_anniversary);

            showProgressDialog(getString(R.string.please_wait), getString(R.string.updating_data));
            ApiInterface apiInterface = ServiceGenerator.createLoginRequest(ApiInterface.class);
            Call<UpdateResponse> call = apiInterface.setUpdateResponse(map);
            if (call != null) {
                call.enqueue(new Callback<UpdateResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<UpdateResponse> call, @NonNull Response<UpdateResponse> response) {
                        hideProgressDialog();
                        UpdateResponse updateResponse = response.body();
                        if (updateResponse != null && updateResponse.isStatus()) {
                            AppPreferences.setUserName(EditProfileActivity.this, name);
                            displayToast(updateResponse.getMsg());
                            EditProfileActivity.super.onBackPressed();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<UpdateResponse> call, @NonNull Throwable t) {
                        hideProgressDialog();
                        displayToast(getString(R.string.unsuccessful));
                    }
                });
            }
        }

    }


    @SuppressLint("SimpleDateFormat")
    private String formatDate(String date) {

        if (date.isEmpty())
            return "0000-00-00";

        String newDate = null;
        try {
            DateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
            Date oldDate = formatter.parse(date);

            SimpleDateFormat nFormat = new SimpleDateFormat("yyyy-MM-dd");
            newDate = nFormat.format(oldDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDate;
    }

    private String getTrimmedString(EditText editText) {
        return editText.getText().toString().trim();
    }

    private void setDateField(final EditText dateEditText) {
        Calendar newCalendar = Calendar.getInstance();
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, (view, year, monthOfYear, dayOfMonth) -> {
            Calendar newDate = Calendar.getInstance();
            newDate.set(year, monthOfYear, dayOfMonth);
            String myFormat = "dd-MMM-yyyy"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
            dateEditText.setText(sdf.format(newDate.getTime()));
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), (dialog, which) -> {
            if (which == DialogInterface.BUTTON_NEGATIVE) {
                dialog.cancel();
                dateEditText.setText("");
            }
        });

        long now = System.currentTimeMillis() - 1000;
        datePickerDialog.getDatePicker().setMaxDate(now);
        dateEditText.setOnClickListener(view -> datePickerDialog.show());
    }

    private void setProfileDetails() {
        Bundle bundle = getIntent().getExtras();
        editUsername.setText(Objects.requireNonNull(bundle).getString("name"));
        editPhoneNo.setText(bundle.getString("mobile"));
        editEmailId.setText(bundle.getString("email"));
       /* String prefs[] = Objects.requireNonNull(bundle.getString("preferences")).split("&");
        if (prefs.length == 2) {
            checkNonVeg.setChecked(true);
            checkVeg.setChecked(true);
        } else if (prefs.length == 1 && prefs[0].contains("Non-Vegetarian")) {
            checkNonVeg.setChecked(true);
        } else if (prefs.length == 1 && prefs[0].contains("Vegetarian")) {
            checkVeg.setChecked(true);
        }*/
        editBirthday.setText(bundle.getString("birthday"));
        editAnniversary.setText(bundle.getString("anniversary"));
    }

    @Override
    protected void onResume() {
        super.onResume();
        setProfileDetails();
    }


}
