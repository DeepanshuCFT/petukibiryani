package com.rest.petukibiryani.user.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rest.petukibiryani.user.R;
import com.rest.petukibiryani.user.listeners.MyClickListener;
import com.rest.petukibiryani.user.models.Locations;


import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LocationListAdapter extends RecyclerView.Adapter<LocationListAdapter.LocationViewBinder> {


    private Context context;
    private List<Locations> locationsList;
    private MyClickListener myClickListener;

    public LocationListAdapter(Context context, List<Locations> locationsList, MyClickListener myClickListener) {
        this.context = context;
        this.locationsList = locationsList;
        this.myClickListener = myClickListener;
    }

    @NonNull
    @Override
    public LocationViewBinder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.layout_locations,viewGroup,false);
        return new LocationViewBinder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LocationViewBinder locationViewBinder, int i) {
        Locations locations = locationsList.get(i);
        locationViewBinder.prev_address.setText(locations.getAddress());
        locationViewBinder.layout_click.setOnClickListener(v -> {
            String latLng = String.valueOf(locations.getAddress()+":"+locationsList.get(i).getLatitude()+":"+
                    locationsList.get(i).getLongitude());
                myClickListener.myOnClick(latLng,i);
        });
    }

    @Override
    public int getItemCount() {
        return locationsList.size();
    }

    class LocationViewBinder extends RecyclerView.ViewHolder {

        @BindView(R.id.prev_address)
        TextView prev_address;
        @BindView(R.id.layout_location_click)
        LinearLayout layout_click;

        LocationViewBinder(@NonNull View view) {
            super(view);
            ButterKnife.bind(this,view);
        }
    }
}
