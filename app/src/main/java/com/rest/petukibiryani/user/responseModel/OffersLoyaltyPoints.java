package com.rest.petukibiryani.user.responseModel;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OffersLoyaltyPoints {
    @SerializedName("status")
    @Expose
    private boolean status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("data")
    @Expose
    private List<OffersLoyaltyPoints.Datum> data;

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isStatus() {
        return status;
    }

    public List<OffersLoyaltyPoints.Datum> getData() {
        return data;
    }

    public void setData(List<OffersLoyaltyPoints.Datum> data) {
        this.data = data;
    }

    public class Datum {
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("offer_name")
        @Expose
        private String offerName;
        @SerializedName("offer_type")
        @Expose
        private String offerType;
        @SerializedName("offer_value")
        @Expose
        private String offerValue;
        @SerializedName("minimum_order_value")
        @Expose
        private String minimumOrderValue;
        @SerializedName("max_redeem")
        @Expose
        private String maxRedeem;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("start_date")
        @Expose
        private String startDate;
        @SerializedName("end_date")
        @Expose
        private String endDate;
        @SerializedName("start_time")
        @Expose
        private String startTime;
        @SerializedName("end_time")
        @Expose
        private String endTime;
        @SerializedName("is_block")
        @Expose
        private String isBlock;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("created_on")
        @Expose
        private String createdOn;
        @SerializedName("updated_on")
        @Expose
        private String updatedOn;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getOfferName() {
            return offerName;
        }

        public void setOfferName(String offerName) {
            this.offerName = offerName;
        }

        public String getOfferType() {
            return offerType;
        }

        public void setOfferType(String offerType) {
            this.offerType = offerType;
        }

        public String getOfferValue() {
            return offerValue;
        }

        public void setOfferValue(String offerValue) {
            this.offerValue = offerValue;
        }

        public String getMinimumOrderValue() {
            return minimumOrderValue;
        }

        public void setMinimumOrderValue(String minimumOrderValue) {
            this.minimumOrderValue = minimumOrderValue;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public String getIsBlock() {
            return isBlock;
        }

        public void setIsBlock(String isBlock) {
            this.isBlock = isBlock;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }

        public String getUpdatedOn() {
            return updatedOn;
        }

        public void setUpdatedOn(String updatedOn) {
            this.updatedOn = updatedOn;
        }

        public String getMaxRedeem() {
            return maxRedeem;
        }

        public void setMaxRedeem(String maxRedeem) {
            this.maxRedeem = maxRedeem;
        }
    }
}
