package com.rest.petukibiryani.user.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rest.petukibiryani.user.R;
import com.rest.petukibiryani.user.activity.FeedBackActivity;
import com.rest.petukibiryani.user.activity.OrderDetails;
import com.rest.petukibiryani.user.helper.AppConstants;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RateOrderFragment extends BottomSheetDialogFragment {

    @BindView(R.id.order_number_to_rate)
    TextView tv_order_no;

    private String orderNo = null, orderId = null;

    @Override
    public int getTheme() {
        return R.style.BaseThemeDialog;
    }

    public static RateOrderFragment getInstance(String orderNo, String orderId) {
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.ORDER_NO, orderNo);
        bundle.putString(AppConstants.ORDER_ID, orderId);
        RateOrderFragment rateOrderFragment = new RateOrderFragment();
        rateOrderFragment.setArguments(bundle);
        return rateOrderFragment;
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_rate_order, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        ButterKnife.bind(this, view);
        if(getArguments()!=null){
            orderNo = getArguments().getString(AppConstants.ORDER_NO);
            orderId = getArguments().getString(AppConstants.ORDER_ID);
            tv_order_no.append(orderNo);
        }
        view.findViewById(R.id.bt_rate_us).setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), FeedBackActivity.class);
            intent.putExtra(AppConstants.ORDER_NO, orderNo);
            intent.putExtra(AppConstants.ORDER_ID, orderId);
            Objects.requireNonNull(getContext()).startActivity(intent);
            getDialog().dismiss();
        });

        view.findViewById(R.id.bt_order_details).setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), OrderDetails.class);
            intent.putExtra(AppConstants.ORDER_NO, orderNo);
            intent.putExtra(AppConstants.ORDER_ID, orderId);
            Objects.requireNonNull(getContext()).startActivity(intent);
        });
    }
}
