package com.rest.petukibiryani.user.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "LocationList")
public class LocationsList {

    @PrimaryKey(autoGenerate = true)
    private int sno;

    @NonNull
    private String address;
    private Double latitude,longitude;

    public LocationsList(@NonNull String address, Double latitude, Double longitude) {
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public void setSno(int sno) {
        this.sno = sno;
    }

    public void setAddress(@NonNull String address) {
        this.address = address;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public int getSno() {
        return sno;
    }

    @NonNull
    public String getAddress() {
        return address;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }
}
