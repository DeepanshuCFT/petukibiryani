package com.rest.petukibiryani.user.responseModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderListResponse {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("msg")
    @Expose
    private String msg;

    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }


    public class Datum {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("order_no")
        @Expose
        private String orderNo;
        @SerializedName("purchase_method")
        @Expose
        private String purchaseMethod;
        @SerializedName("delivery_type")
        @Expose
        private String deliveryType;
        @SerializedName("cgst")
        @Expose
        private String cgst;
        @SerializedName("sgst")
        @Expose
        private String sgst;
        @SerializedName("offer_id")
        @Expose
        private String offerId;
        @SerializedName("points")
        @Expose
        private String points;
        @SerializedName("delivery_charge")
        @Expose
        private String deliveryCharge;
        @SerializedName("total_price")
        @Expose
        private String totalPrice;
        @SerializedName("payable_price")
        @Expose
        private String payablePrice;
        @SerializedName("grand_total")
        @Expose
        private String grandTotal;
        @SerializedName("earn_points")
        @Expose
        private String earnPoints;
        @SerializedName("special_note")
        @Expose
        private String specialNote;
        @SerializedName("delivery_address")
        @Expose
        private String deliveryAddress;
        @SerializedName("created_on")
        @Expose
        private String createdOn;
        @SerializedName("updated_on")
        @Expose
        private String updatedOn;
        @SerializedName("order_note")
        @Expose
        private String orderNote;
        @SerializedName("discount")
        @Expose
        private String discount;
        @SerializedName("discount_by")
        @Expose
        private String discountBy;
        @SerializedName("cancellation_reason")
        @Expose
        private String cancellationReason;
        @SerializedName("order_status")
        @Expose
        private String orderStatus;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("feedback_id")
        @Expose
        private String feedbackId;
        @SerializedName("feedback_rating")
        @Expose
        private String feedbackRating;

        public String getFeedbackId() {
            return feedbackId;
        }

        public void setFeedbackId(String feedbackId) {
            this.feedbackId = feedbackId;
        }

        public String getFeedbackRating() {
            return feedbackRating;
        }

        public void setFeedbackRating(String feedbackRating) {
            this.feedbackRating = feedbackRating;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getOrderNo() {
            return orderNo;
        }

        public void setOrderNo(String orderNo) {
            this.orderNo = orderNo;
        }

        public String getPurchaseMethod() {
            return purchaseMethod;
        }

        public void setPurchaseMethod(String purchaseMethod) {
            this.purchaseMethod = purchaseMethod;
        }

        public String getDeliveryType() {
            return deliveryType;
        }

        public void setDeliveryType(String deliveryType) {
            this.deliveryType = deliveryType;
        }

        public String getCgst() {
            return cgst;
        }

        public void setCgst(String cgst) {
            this.cgst = cgst;
        }

        public String getSgst() {
            return sgst;
        }

        public void setSgst(String sgst) {
            this.sgst = sgst;
        }

        public String getOfferId() {
            return offerId;
        }

        public void setOfferId(String offerId) {
            this.offerId = offerId;
        }

        public String getPoints() {
            return points;
        }

        public void setPoints(String points) {
            this.points = points;
        }

        public String getDeliveryCharge() {
            return deliveryCharge;
        }

        public void setDeliveryCharge(String deliveryCharge) {
            this.deliveryCharge = deliveryCharge;
        }

        public String getTotalPrice() {
            return totalPrice;
        }

        public void setTotalPrice(String totalPrice) {
            this.totalPrice = totalPrice;
        }

        public String getPayablePrice() {
            return payablePrice;
        }

        public void setPayablePrice(String payablePrice) {
            this.payablePrice = payablePrice;
        }

        public String getGrandTotal() {
            return grandTotal;
        }

        public void setGrandTotal(String grandTotal) {
            this.grandTotal = grandTotal;
        }

        public String getEarnPoints() {
            return earnPoints;
        }

        public void setEarnPoints(String earnPoints) {
            this.earnPoints = earnPoints;
        }

        public String getSpecialNote() {
            return specialNote;
        }

        public void setSpecialNote(String specialNote) {
            this.specialNote = specialNote;
        }

        public String getDeliveryAddress() {
            return deliveryAddress;
        }

        public void setDeliveryAddress(String deliveryAddress) {
            this.deliveryAddress = deliveryAddress;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }

        public String getUpdatedOn() {
            return updatedOn;
        }

        public void setUpdatedOn(String updatedOn) {
            this.updatedOn = updatedOn;
        }

        public String getOrderNote() {
            return orderNote;
        }

        public void setOrderNote(String orderNote) {
            this.orderNote = orderNote;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public String getDiscountBy() {
            return discountBy;
        }

        public void setDiscountBy(String discountBy) {
            this.discountBy = discountBy;
        }

        public String getCancellationReason() {
            return cancellationReason;
        }

        public void setCancellationReason(String cancellationReason) {
            this.cancellationReason = cancellationReason;
        }

        public String getOrderStatus() {
            return orderStatus;
        }

        public void setOrderStatus(String orderStatus) {
            this.orderStatus = orderStatus;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
        }
}
