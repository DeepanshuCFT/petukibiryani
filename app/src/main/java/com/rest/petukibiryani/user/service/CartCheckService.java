package com.rest.petukibiryani.user.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.util.Log;

import com.rest.petukibiryani.user.helper.AppDatabases;
import com.rest.petukibiryani.user.helper.AppPreferences;
import com.rest.petukibiryani.user.network.ApiInterface;
import com.rest.petukibiryani.user.network.ServiceGenerator;
import com.rest.petukibiryani.user.responseModel.CartReminderResponse;

import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartCheckService extends Service {

    public CartCheckService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startTimer();
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private static Timer timer;
    private static TimerTask timerTask;
    public void startTimer() {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();
        timer.schedule(timerTask,15*60*1000 , 15*60*1000);
    }

    /**
     * it sets the timer to print the counter every x seconds
     */
    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                Log.i("service","i am called");
                if(AppDatabases.getOrderItemsDAO(getApplicationContext()).getOrderedItems().size()>0){
                    String user_id = AppPreferences.getUser_id(getApplicationContext());
                    ApiInterface apiInterface = ServiceGenerator.createLoginRequest(ApiInterface.class);
                    Call<CartReminderResponse> call = apiInterface.setReminder(user_id);
                    call.enqueue(new Callback<CartReminderResponse>() {
                        @Override
                        public void onResponse(@NonNull Call<CartReminderResponse> call, @NonNull Response<CartReminderResponse> response) {
                            Log.i("status",""+ Objects.requireNonNull(response.body()).getStatus());
                        }

                        @Override
                        public void onFailure(@NonNull Call<CartReminderResponse> call, @NonNull Throwable t) {
                            Log.i("status","unsuccessful");
                        }
                    });
                }
                //stopTimerTask();
            }
        };
    }

    /**
     * not needed
     */
    public static void stopTimerTask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopTimerTask();
    }

}
