package com.rest.petukibiryani.user.responseModel;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReferCodeResponse {

    @SerializedName("status")
    @Expose
    private boolean status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("data")
    @Expose
    private Data DataObject;

    public boolean getStatus() {
        return status;
    }

    public String getMsg() {
        return msg;
    }

    public Data getData() {
        return DataObject;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setData(Data dataObject) {
        this.DataObject = dataObject;
    }
    public class Data {

        @SerializedName("refer_code")
        @Expose
        private String refer_code;


        public String getRefer_code() {
            return refer_code;
        }

        public void setRefer_code(String refer_code) {
            this.refer_code = refer_code;
        }
    }
}



