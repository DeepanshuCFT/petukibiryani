package com.rest.petukibiryani.user.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rest.petukibiryani.user.R;
import com.rest.petukibiryani.user.responseModel.OrderDetailsResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderDetailsListAdapter extends RecyclerView.Adapter<OrderDetailsListAdapter.OrderViewHolder> {

    private Context context;
    private List<OrderDetailsResponse.Data.OrderedItem> orderedItemList;


    public OrderDetailsListAdapter(Context context, List<OrderDetailsResponse.Data.OrderedItem> orderListResponseslist) {
        this.context = context;
        this.orderedItemList = orderListResponseslist;
    }

    @NonNull
    @Override
    public OrderViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View layoutView = LayoutInflater.from(context).inflate(R.layout.order_details_list_item, viewGroup, false);
        return new OrderViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderViewHolder holder, int postion) {
        holder.tv_food_name.setText(orderedItemList.get(postion).getSubCategoryName());
        holder.tv_food_price.setText(("₹ " + orderedItemList.get(postion).getTotalPrice()));
        holder.tv_food_decription.setText(orderedItemList.get(postion).getDescription());
        holder.tv_food_qty.setText(orderedItemList.get(postion).getQuantity());
        if (orderedItemList.get(postion).getIsVariant().equals("1")) {
            holder.tv_food_plateSize.setVisibility(View.VISIBLE);
            if (orderedItemList.get(postion).getQuantityType().equals("0"))
                holder.tv_food_plateSize.setText(("Half"));
            else
                holder.tv_food_plateSize.setText(("Full"));
        } else {
            holder.tv_food_plateSize.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return orderedItemList.size();
    }

    class OrderViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.tv_food_name)
        TextView tv_food_name;
        @BindView(R.id.tv_food_plateSize)
        TextView tv_food_plateSize;
        @BindView(R.id.tv_food_qty)
        TextView tv_food_qty;
        @BindView(R.id.tv_food_price)
        TextView tv_food_price;
        @BindView(R.id.tv_food_description)
        TextView tv_food_decription;

        OrderViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
