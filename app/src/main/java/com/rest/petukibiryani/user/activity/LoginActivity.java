package com.rest.petukibiryani.user.activity;

import android.os.Bundle;

import com.rest.petukibiryani.user.R;
import com.rest.petukibiryani.user.fragment.LoginFragment;

public class LoginActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        addFragment(new LoginFragment());
    }
}
