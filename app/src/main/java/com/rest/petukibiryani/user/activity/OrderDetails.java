package com.rest.petukibiryani.user.activity;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rest.petukibiryani.user.R;
import com.rest.petukibiryani.user.adapter.OrderDetailsListAdapter;
import com.rest.petukibiryani.user.helper.AppConstants;
import com.rest.petukibiryani.user.network.ApiInterface;
import com.rest.petukibiryani.user.network.ServiceGenerator;
import com.rest.petukibiryani.user.responseModel.OrderDetailsResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderDetails extends BaseActivity {

    @BindView(R.id.tv_bouns)
    TextView tv_bonus;
    @BindView(R.id.tv_subtotal)
    TextView tv_subtotal;
    @BindView(R.id.tv_delivery_charge)
    TextView tv_deliveryCharge;
    @BindView(R.id.tv_CGST)
    TextView tv_CGST;
    @BindView(R.id.tv_SGST)
    TextView tv_SGST;
    @BindView(R.id.tv_totalBill)
    TextView tv_totalBill;
    @BindView(R.id.tv_order_no)
    TextView tv_orderNo;
    @BindView(R.id.tv_order_status)
    TextView orderStatus;
    @BindView(R.id.ll_pkb_wallet)
    LinearLayout pkbwalletLL;
    @BindView(R.id.view_pkb)
    View pkbView;
    @BindView(R.id.rv_orderDetails)
    RecyclerView recyclerView;
    private String orderId;
    private OrderDetailsListAdapter mOrderDetailsListAdapter;
    private OrderDetailsResponse orderDetailsResponse;
    private List<OrderDetailsResponse.Data.OrderedItem> orderedItemList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        init();
        getOrderDetails();
    }

    /***
     * Initializing all the views
     */
    private void init() {
        ButterKnife.bind(this);
        initializeToolbar(getResources().getString(R.string.order_details), R.drawable.back);
        orderedItemList = new ArrayList<>();
        orderId = getIntent().getStringExtra(AppConstants.ORDER_ID);
        String orderNo = getIntent().getStringExtra(AppConstants.ORDER_NO);

        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder("Order No: " + orderNo);
        spannableStringBuilder.setSpan(new ForegroundColorSpan(Color.BLACK), 10, orderNo.length() + 10, 0);
        tv_orderNo.setText(spannableStringBuilder);

        tv_subtotal.setText(AppConstants.RS_00);
        tv_deliveryCharge.setText(AppConstants.RS_00);
        tv_CGST.setText(AppConstants.RS_00);
        tv_SGST.setText(AppConstants.RS_00);
        tv_totalBill.setText(AppConstants.RS_00);

        mOrderDetailsListAdapter = new OrderDetailsListAdapter(this, orderedItemList);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mOrderDetailsListAdapter);
    }

    /***
     * Fetching data from ParticularOrdersList api and setting data
     */
    public void getOrderDetails() {
        showProgressDialog(getString(R.string.loading), getString(R.string.please_wait));
        orderedItemList.clear();
        ApiInterface apiInterface = ServiceGenerator.createLoginRequest(ApiInterface.class);
        Call<OrderDetailsResponse> call = apiInterface.getOrderDetails(orderId);
        call.enqueue(new Callback<OrderDetailsResponse>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<OrderDetailsResponse> call, @NonNull Response<OrderDetailsResponse> response) {
                hideProgressDialog();
                orderDetailsResponse = response.body();
                if (Objects.requireNonNull(orderDetailsResponse).getStatus() && orderDetailsResponse.getData() != null) {

                    switch (orderDetailsResponse.getData().getOrderStatus()) {
                        case "0":
                            orderStatus.setText(getString(R.string.your_order_is_under_process_we_will_notify_once_your_order_is_confirmed));
                            break;
                        case "1":
                            orderStatus.setText(getString(R.string.your_order_is_confirmed_and_it_will_be_delivered_at_your_given_address));
                            break;
                        case "3":
                            orderStatus.setText(getString(R.string.your_order_has_been_cancelled));
                            break;
                        case "4":
                            orderStatus.setText(getString(R.string.this_order_has_been_delivered));
                    }

                    if (orderDetailsResponse.getData().getPoints().equals("0")) {
                        pkbwalletLL.setVisibility(View.GONE);
                        pkbView.setVisibility(View.GONE);
                    } else {
                        pkbwalletLL.setVisibility(View.VISIBLE);
                        pkbView.setVisibility(View.VISIBLE);
                        tv_bonus.setText("₹ " + orderDetailsResponse.getData().getPoints());
                    }
                    tv_subtotal.setText("₹ " + orderDetailsResponse.getData().getPayablePrice());
                    tv_deliveryCharge.setText("₹ " + orderDetailsResponse.getData().getDeliveryCharge());
                    tv_CGST.setText("₹ " + orderDetailsResponse.getData().getCgst());
                    tv_SGST.setText("₹ " + orderDetailsResponse.getData().getSgst());
                    tv_totalBill.setText("₹ " + orderDetailsResponse.getData().getGrandTotal());
                    orderedItemList.addAll(orderDetailsResponse.getData().getOrderedItems());
                    mOrderDetailsListAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(@NonNull Call<OrderDetailsResponse> call, @NonNull Throwable t) {
                hideProgressDialog();
                displayToast(R.string.some_thing_wrong);
            }
        });
    }

}