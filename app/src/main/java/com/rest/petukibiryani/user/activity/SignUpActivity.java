package com.rest.petukibiryani.user.activity;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.rest.petukibiryani.user.R;
import com.rest.petukibiryani.user.network.ApiInterface;
import com.rest.petukibiryani.user.network.ServiceGenerator;
import com.rest.petukibiryani.user.responseModel.RegistrationResponse;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends BaseActivity {

    @BindView(R.id.et_phoneNo)
    EditText et_phoneNo;
    @BindView(R.id.et_name)
    EditText et_name;
    @BindView(R.id.et_refer_code)
    EditText et_refer_code;
    @BindView(R.id.checkbox)
    CheckBox checkBox;
    @BindView(R.id.terms_condition)
    TextView terms_conditions;
    private String mobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        mobile = getIntent().getStringExtra("mobile");
        init();
    }

    private void init() {
        ButterKnife.bind(this);
        terms_conditions.setOnClickListener(v -> {
            String url = "http://www.petukibiryani.com/privacy-policy.html";
            Uri uri = Uri.parse(url);
            Intent urlIntent = new Intent();
            urlIntent.setData(uri);
            urlIntent.setAction(Intent.ACTION_VIEW);
            startActivity(urlIntent);
        });
        findViewById(R.id.bt_submit).setOnClickListener(v -> {
            String phoneNo = et_phoneNo.getText().toString().trim();
            String userName = et_name.getText().toString().trim();
            String referCode = et_refer_code.getText().toString().trim();

            if (!phoneNo.isEmpty() && phoneNo.length() == 10 && !userName.isEmpty() && checkBox.isChecked() &&
                    userName.matches("^[a-zA-Z\\s]+$") && (referCode.isEmpty() || referCode.length()==11)) {
                String name = ("" + userName.charAt(0)).toUpperCase() + userName.substring(1);
                userRegistration(phoneNo, name, referCode);
            } else if (userName.isEmpty()) {
                displayToast(R.string.name);
            } else if (!checkBox.isChecked()) {
                displayToast(R.string.please_agree);
            } else {
                displayToast(R.string.enter_valid_information);
                //((BaseActivity) getApplicationContext()).displayToast(R.string.valid_number);
                // displayToast(R.string.valid_number);
            }
        });
        SpannableString text = new SpannableString(getResources().getString(R.string.i_agree_with_terms_and_conditions));
        text.setSpan(new ForegroundColorSpan(Color.BLACK), 0, 12, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        text.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), 13, 31, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        text.setSpan(new UnderlineSpan(), 13, 31, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        terms_conditions.setText(text);
        et_name.requestFocus();
        et_phoneNo.setText(mobile);
    }


    public void userRegistration(String phoneNo, String userName, String referCode) {

        showProgressDialog(getString(R.string.loading), getString(R.string.please_wait));
        ApiInterface apiInterface = ServiceGenerator.createLoginRequest(ApiInterface.class);
        Call<RegistrationResponse> call = apiInterface.register_User(phoneNo, userName, referCode);
        call.enqueue(new Callback<RegistrationResponse>() {
            @Override
            public void onResponse(@NonNull Call<RegistrationResponse> call, @NonNull Response<RegistrationResponse> response) {
                hideProgressDialog();
                RegistrationResponse registrationResponse = response.body();
                if(registrationResponse!=null){
                    if (registrationResponse.getStatus()) {     // if new user registered successfully
                        // here we are getting correct OTP in LOGCAT
                        Log.i("OTP", registrationResponse.getOtp());
                        //displayToast(registrationResponse.getOtp());
                        // displayToast(registrationResponse.getOtp());
                        Intent intent = new Intent(SignUpActivity.this, OtpActivity.class);
                        intent.putExtra("user_id", registrationResponse.getUser_id());
                        intent.putExtra("mobile", phoneNo);
                        startActivity(intent);
                        finish();
                    } else if (!registrationResponse.getStatus()) {     // if new user not registered successfully
                        displayToast(registrationResponse.getMsg());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<RegistrationResponse> call, @NonNull Throwable t) {
                hideProgressDialog();
                displayToast(R.string.some_thing_wrong);
            }
        });
    }
}
