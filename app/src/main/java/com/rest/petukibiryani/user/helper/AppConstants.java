package com.rest.petukibiryani.user.helper;


public interface AppConstants {

    Double pkbLat = 28.650329;
    Double pkbLong = 77.3694173;
    String PREFERENCE_USER = "preference_user";
    String FIREBASE_TOKEN = "firebase_token";
    String REFERSH_FIREBASE_TOKEN = "referesh_firebase_token";

    //String USER_TOKEN = "user_token";

    String IS_LOGIN = "isLogIn";
    //String ADDRESS = "address";
    String NAME = "name";
    String BONUS_PTS = "bonus_pts";
    String MOBILE = "mobile";
    String AUTH = "auth";
    String EXIT = "exit";
    String PLACE = "place";
    String WITH_KMS = "withKms";
    String USER_ID = "user_id";
    String CATEGORY_ID = "category_ids";
    String PKB_ADDRESS = "Shop No. A-12, Plot No. 729, Aman Plaza, Shakti Khand-4, Indirapuram, Ghaziabad, Uttar Pradesh 201014";
    String RS_00 = "₹ 00";
    String ORDER_NO = "orderNo";
    String ORDER_ID = "orderId";


    String INTENT_FILTER_SMS_RECIEVED = "android.provider.Telephony.SMS_RECEIVED";

    //String DATE = "date";
    //String TIME = "time";
    String PICK_UP = "isPickUp";
    //String ORDER_LATER = "orderLater";
    String OPEN_CLOSE = "open_or_close";
    String DESCRIPTION = "description";
    String SESSION = "session";
    String LAT_LONG = "latLong";
}
