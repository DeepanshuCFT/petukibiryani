package com.rest.petukibiryani.user.utilities;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.rest.petukibiryani.user.listeners.LocationsListDAO;
import com.rest.petukibiryani.user.models.LocationsList;

@Database(entities = {LocationsList.class}, version = 1, exportSchema = false)
public abstract class LocationsListDatabase extends RoomDatabase {
        public abstract LocationsListDAO getLocationsListDAO();
}
