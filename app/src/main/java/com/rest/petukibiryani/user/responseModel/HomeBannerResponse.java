package com.rest.petukibiryani.user.responseModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HomeBannerResponse {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("data")
    @Expose
    private List<HomeBannerResponse.Datum> data = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<HomeBannerResponse.Datum> getData() {
        return data;
    }

    public void setData(List<HomeBannerResponse.Datum> data) {
        this.data = data;
    }

    public class Datum {
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("isBlock")
        @Expose
        private String is_block;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("created_on")
        @Expose
        private String createdOn;
        @SerializedName("updated_on")
        @Expose
        private String updatedOn;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getIs_block() {
            return is_block;
        }

        public void setIs_block(String is_block) {
            this.is_block = is_block;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }

        public String getUpdatedOn() {
            return updatedOn;
        }

        public void setUpdatedOn(String updatedOn) {
            this.updatedOn = updatedOn;
        }
    }

    @SerializedName("restaurant_status")
    @Expose
    private RestStatus restaurantStatus = null;

    public RestStatus getRestaurantStatus() {
        return restaurantStatus;
    }

    public void setRestaurantStatus(RestStatus restaurantStatus) {
        this.restaurantStatus = restaurantStatus;
    }

    public class RestStatus {
        @SerializedName("store_status")
        @Expose
        private Boolean store_status;
        @SerializedName("description")
        @Expose
        private String description;

        public Boolean getStore_status() {
            return store_status;
        }

        public void setStore_status(Boolean store_status) {
            this.store_status = store_status;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }
}
