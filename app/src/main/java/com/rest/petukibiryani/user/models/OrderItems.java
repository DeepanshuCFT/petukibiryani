package com.rest.petukibiryani.user.models;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "OrderItems")
public class OrderItems {

    @PrimaryKey(autoGenerate = true)
    private int sno;
    @NonNull
    private String itemId,itemName,description,plate,categoryName;
    private int price,counter;

    public OrderItems(@NonNull String itemId, @NonNull String itemName, @NonNull String description, @NonNull String plate, @NonNull String categoryName, int price, int counter) {
        this.itemId = itemId;
        this.itemName = itemName;
        this.description = description;
        this.plate = plate;
        this.categoryName = categoryName;
        this.price = price;
        this.counter = counter;
    }

    @NonNull
    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(@NonNull String categoryName) {
        this.categoryName = categoryName;
    }

    public void setItemId(@NonNull String itemId) {
        this.itemId = itemId;
    }

    public void setItemName(@NonNull String itemName) {
        this.itemName = itemName;
    }

    public void setDescription(@NonNull String description) {
        this.description = description;
    }

    @NonNull
    public String getDescription() {
        return description;
    }

    public void setPlate(@NonNull String plate) {
        this.plate = plate;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    @NonNull
    public String getItemId() {

        return itemId;
    }

    @NonNull
    public String getItemName() {
        return itemName;
    }

    public void setSno(int sno) {
        this.sno = sno;
    }

    public int getSno() {

        return sno;
    }

    @NonNull
    public String getPlate() {

        return plate;
    }

    public int getPrice() {
        return price;
    }

    public int getCounter() {
        return counter;
    }
}
