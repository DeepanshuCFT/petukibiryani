package com.rest.petukibiryani.user.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;


import com.rest.petukibiryani.user.R;
import com.rest.petukibiryani.user.adapter.OrderListAdapter;
import com.rest.petukibiryani.user.helper.AppPreferences;
import com.rest.petukibiryani.user.listeners.OnConnectionChangeListener;
import com.rest.petukibiryani.user.network.ApiInterface;
import com.rest.petukibiryani.user.network.ServiceGenerator;
import com.rest.petukibiryani.user.responseModel.OrderListResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderActivity extends BaseActivity implements OnConnectionChangeListener {

    private OrderListAdapter mOrderListAdapter;
    private List<OrderListResponse.Datum> mOrderListResponses;
    @BindView(R.id.ll_null_screen)
    LinearLayout null_layout;
    @BindView(R.id.rv_order)
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        init();
    }

    /***
     * Initializing all the views
     */
    private void init() {
        ButterKnife.bind(this);
        initializeToolbar(getResources().getString(R.string.your_orders), R.drawable.back);
        Objects.requireNonNull(noConnectionLayout).setOnRefreshListener(() -> {
            findViewById(R.id.layout_progressBar).setVisibility(View.VISIBLE);
            new CheckInternetConnection(this, getApplicationContext()).execute();
            noConnectionLayout.setRefreshing(false);
        });
        null_layout.setVisibility(View.VISIBLE);
        mOrderListResponses = new ArrayList<>();
        mOrderListAdapter = new OrderListAdapter(this, mOrderListResponses);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mOrderListAdapter);
    }

    @Override
    public void onConnectionChange(boolean isConnected) {

        if (noConnectionLayout != null) {
            if (isConnected) {
                getOrderList();
                noConnectionLayout.setVisibility(View.GONE);
            } else {
                noConnectionLayout.setVisibility(View.VISIBLE);
                findViewById(R.id.layout_progressBar).setVisibility(View.GONE);
            }
        }
    }

    /***
     * Fetching data from OrdersList api and setting in recyclerView
     */
    private void getOrderList() {
        //findViewById(R.id.layout_progressBar).setVisibility(View.VISIBLE);
        String userId = AppPreferences.getUser_id(this);
        mOrderListResponses.clear();
        ApiInterface apiInterface = ServiceGenerator.createLoginRequest(ApiInterface.class);
        Call<OrderListResponse> call = apiInterface.getOrderList(userId);
        call.enqueue(new Callback<OrderListResponse>() {
            @Override
            public void onResponse(@NonNull Call<OrderListResponse> call, @NonNull Response<OrderListResponse> response) {
                OrderListResponse orderListResponse = response.body();
                if (orderListResponse != null && orderListResponse.getStatus() && orderListResponse.getData() != null) {
                    if (orderListResponse.getData().size() > 0) {
                        null_layout.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                    }
                    mOrderListResponses.addAll(orderListResponse.getData());
                    mOrderListAdapter.notifyDataSetChanged();

                }
                findViewById(R.id.layout_progressBar).setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<OrderListResponse> call, @NonNull Throwable t) {
                findViewById(R.id.layout_progressBar).setVisibility(View.GONE);
                displayToast(R.string.some_thing_wrong);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        findViewById(R.id.layout_progressBar).setVisibility(View.VISIBLE);
        new CheckInternetConnection(this,getApplicationContext()).execute();
    }
}
