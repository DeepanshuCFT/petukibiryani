package com.rest.petukibiryani.user.views;

import android.content.Context;
import android.widget.ImageView;

import com.rest.petukibiryani.user.helper.AppPreferences;
import com.squareup.picasso.Picasso;

import ss.com.bannerslider.ImageLoadingService;

public class PicassoImageLoadingService implements ImageLoadingService {
    public Context context;

    public PicassoImageLoadingService(Context context) {
        this.context = context;
    }

    @Override
    public void loadImage(String url, ImageView imageView) {
        if (AppPreferences.isRestOpen(context))
            Picasso.with(context).load(url).into(imageView);
        else
            Picasso.with(context).load(url).transform(new GrayScaleTransformation()).into(imageView);
    }

    @Override
    public void loadImage(int resource, ImageView imageView) {
        if (AppPreferences.isRestOpen(context))
            Picasso.with(context).load(resource).into(imageView);
        else
            Picasso.with(context).load(resource).transform(new GrayScaleTransformation()).into(imageView);
    }

    @Override
    public void loadImage(String url, int placeHolder, int errorDrawable, ImageView imageView) {
        if (AppPreferences.isRestOpen(context))
            Picasso.with(context).load(url).placeholder(placeHolder).error(errorDrawable).into(imageView);
        else
            Picasso.with(context).load(url).placeholder(placeHolder).transform(new GrayScaleTransformation()).error(errorDrawable).into(imageView);
    }
}