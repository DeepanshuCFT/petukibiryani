package com.rest.petukibiryani.user.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.rest.petukibiryani.user.R;

public class CustomLocationAddressAdapter implements GoogleMap.InfoWindowAdapter {

    private View mView;

    @SuppressLint("InflateParams")
    public CustomLocationAddressAdapter(Context context) {
        mView = LayoutInflater.from(context).inflate(R.layout.custom_address_layout,null);
    }

    private void windowText(Marker marker, View view){

        String address = marker.getSnippet(), title = marker.getTitle();
        TextView tvTitle = view.findViewById(R.id.address_title);
        TextView tvAddress = view.findViewById(R.id.current_address);

        if(!title.isEmpty())
            tvTitle.setText(title);

        if(!address.isEmpty())
            tvAddress.setText(address);

    }


    @Override
    public View getInfoWindow(Marker marker) {
        windowText(marker, mView);
        return mView;
    }

    @Override
    public View getInfoContents(Marker marker) {
        windowText(marker, mView);
        return mView;
    }
}
