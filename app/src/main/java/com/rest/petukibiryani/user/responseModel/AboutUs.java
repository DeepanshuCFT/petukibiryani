package com.rest.petukibiryani.user.responseModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class AboutUs implements Serializable {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("data")
    @Expose
    private List<AboutUs.Datum> data = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<AboutUs.Datum> getData() {
        return data;
    }

    public void setData(List<AboutUs.Datum> data) {
        this.data = data;
    }

    public class Datum {
        @SerializedName("description")
        @Expose
        private String description;

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        @SerializedName("content")
        @Expose
        private String content;

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }
}
