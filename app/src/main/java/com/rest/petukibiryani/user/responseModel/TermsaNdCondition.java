package com.rest.petukibiryani.user.responseModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TermsaNdCondition {

    private Boolean status;
    private String msg;

    private List<Datum> data = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        private String id;
        private String content;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getContent_1() {
            return content_1;
        }

        public void setContent_1(String content_1) {
            this.content_1 = content_1;
        }

        public String getContent_2() {
            return content_2;
        }

        public void setContent_2(String content_2) {
            this.content_2 = content_2;
        }

        private String content_1;
        private String content_2;



    }
}
