package com.rest.petukibiryani.user.network.interceptor;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;


public class NormalApiInterceptor implements Interceptor {
    private String token;

    public NormalApiInterceptor(String token) {
        this.token = token;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Request compressedRequest = request.newBuilder()
                .header("Content-Type", "application/json")
                .header("Authorization", token)
                .method(request.method(), request.body())
                .build();

        return chain.proceed(compressedRequest);
    }
}
