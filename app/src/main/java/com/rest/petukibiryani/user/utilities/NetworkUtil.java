package com.rest.petukibiryani.user.utilities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkUtil {

    public static boolean getConnectivityStatus(Context context) {
        final ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (cm != null) {
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                return (activeNetwork != null && activeNetwork.isConnected());
        }
        return false;
    }
}
