package com.rest.petukibiryani.user.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.Button;

import com.rest.petukibiryani.user.R;
import com.rest.petukibiryani.user.helper.AppPreferences;
import com.rest.petukibiryani.user.network.ApiInterface;
import com.rest.petukibiryani.user.network.ServiceGenerator;
import com.rest.petukibiryani.user.responseModel.ReferCodeResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReferNEarnActivity extends BaseActivity {

    @BindView(R.id.bt_share)
    Button bt_share;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refer_n_earn);
        init();
    }

    private void init() {
        ButterKnife.bind(this);
        initializeToolbar(getResources().getString(R.string.refer_and_earn), R.drawable.back);
        bt_share.setOnClickListener(v -> getReferCode());
    }

    private void getReferCode() {
        showProgressDialog(getString(R.string.please_wait), getString(R.string.loading));
        String user_id = AppPreferences.getUser_id(this);
        ApiInterface apiInterface = ServiceGenerator.createLoginRequest(ApiInterface.class);
        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("user_id", user_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Call<ReferCodeResponse> call = apiInterface.getReferCode(paramObject.toString());
        call.enqueue(new Callback<ReferCodeResponse>() {
            @Override
            public void onResponse(@NonNull Call<ReferCodeResponse> call, @NonNull Response<ReferCodeResponse> response) {
                ReferCodeResponse referCodeResponse = response.body();
                Intent sendIntent = new Intent();
                String referCode = Objects.requireNonNull(referCodeResponse).getData().getRefer_code();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "Sign Up for Petu Ki Biryani app and get 100 points and earn upto 25% points on every order." +
                        " Use my refer code "+referCode+" Download \nhttps://bit.ly/2GNRV04");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                hideProgressDialog();
            }

            @Override
            public void onFailure(@NonNull Call<ReferCodeResponse> call, @NonNull Throwable t) {
                displayToast(R.string.unsuccessful);
                hideProgressDialog();
            }
        });

    }
}
