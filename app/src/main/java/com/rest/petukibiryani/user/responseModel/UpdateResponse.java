package com.rest.petukibiryani.user.responseModel;

public class UpdateResponse {

    private  boolean status;
    private String msg;
    private boolean user_profile;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isUser_profile() {
        return user_profile;
    }

    public void setUser_profile(boolean user_profile) {
        this.user_profile = user_profile;
    }
}
