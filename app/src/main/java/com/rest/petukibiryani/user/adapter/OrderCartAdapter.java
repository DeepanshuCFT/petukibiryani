package com.rest.petukibiryani.user.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.rest.petukibiryani.user.R;
import com.rest.petukibiryani.user.helper.AppDatabases;
import com.rest.petukibiryani.user.listeners.BillNotify;
import com.rest.petukibiryani.user.listeners.OrderItemsDAO;
import com.rest.petukibiryani.user.models.OrderItems;
import com.rest.petukibiryani.user.models.OrderItemsList;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderCartAdapter extends RecyclerView.Adapter<OrderCartAdapter.ViewHolder> {

    private Context context;
    private ArrayList<OrderItemsList> orders;
    private int price, counter, new_price;
    private BillNotify listener;
    private OrderItemsDAO orderItemsDAO;

    public OrderCartAdapter(Context context, ArrayList<OrderItemsList> orders, BillNotify listener) {
        this.context = context;
        this.orders = orders;
        this.listener = listener;
        orderItemsDAO = AppDatabases.getOrderItemsDAO(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.my_cart_item_layout, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        price = orders.get(position).getPrice();
        counter = orders.get(position).getCounter();
        viewHolder.itemName.setText(orders.get(position).getItemName());
        viewHolder.itemDescription.setText(orders.get(position).getDescription());
        if (!orders.get(position).getPlate().equals("normal")) {
            viewHolder.plate.setVisibility(View.VISIBLE);
            viewHolder.plate.setText(orders.get(position).getPlate());
        } else
            viewHolder.plate.setVisibility(View.GONE);
        viewHolder.counter.setText(String.valueOf(counter));
        new_price = price * counter;
        viewHolder.price.setText(String.valueOf(new_price));

        viewHolder.iv_add.setOnClickListener(v -> {
            counter = orders.get(position).getCounter();
            if (counter == 2 && (orders.get(position).getItemName().equalsIgnoreCase(context.getResources().getString(R.string.cold_drinks))
                    || orders.get(position).getItemName().equalsIgnoreCase(context.getResources().getString(R.string.mineral_bottles)))
                    ) {
                Toast.makeText(context,context.getString(R.string.this_item_cannot_be_added_more_than_two_times), Toast.LENGTH_SHORT).show();
            } else {
                counter++;
                viewHolder.counter.setText(String.valueOf(counter));
                orders.get(position).setCounter(counter);
                new_price = price * counter;
                viewHolder.price.setText(String.valueOf(new_price));
                addItemToDatabase(position);
                notifyDataSetChanged();
                listener.cartNotify(calculateSubTotal());
            }
        });

        viewHolder.iv_minus.setOnClickListener(v -> {
            counter = orders.get(position).getCounter();
            if (counter > 1) {
                counter--;
                viewHolder.counter.setText(String.valueOf(counter));
                removeItemFromDatabase(position);
                orders.get(position).setCounter(counter);
                new_price = price * counter;
                viewHolder.price.setText(String.valueOf(new_price));
                notifyDataSetChanged();
                listener.cartNotify(calculateSubTotal());
            } else if (counter == 1) {
                removeItemFromDatabase(position);
                orders.remove(position);
                notifyDataSetChanged();
                listener.cartNotify(calculateSubTotal());
            }
        });

    }

    private float calculateSubTotal() {

        List<OrderItems> orderItems = orderItemsDAO.getOrderedItems();
        float subTotal = 0;
        for (OrderItems oI : orderItems) {
            subTotal += (oI.getPrice() * oI.getCounter());
        }
        return subTotal;
    }

    private void removeItemFromDatabase(int position) {

        OrderItemsList orderedItems = orders.get(position);
        String itemId = orderedItems.getItemId();
        String plate = orderedItems.getPlate();
        if (orderItemsDAO.isItemAlreadyAdded(itemId, plate) == 1 &&
                orderItemsDAO.getCounterOfItems(itemId, plate) == 1) {
            OrderItems orderItems = orderItemsDAO.getOrderItemWithId(itemId, plate);
            orderItemsDAO.delete(orderItems);
        } else if (orderItemsDAO.isItemAlreadyAdded(itemId, plate) == 1) {
            OrderItems orderItems = orderItemsDAO.getOrderItemWithId(itemId, plate);
            orderItems.setCounter(orderItems.getCounter() - 1);
            orderItemsDAO.update(orderItems);
        }
    }

    private void addItemToDatabase(int position) {

        OrderItemsList orderedItems = orders.get(position);
        String itemId = orderedItems.getItemId();
        String plate = orderedItems.getPlate();

        if (orderItemsDAO.isItemAlreadyAdded(itemId, plate) == 1) {
            OrderItems orderItems = orderItemsDAO.getOrderItemWithId(itemId, plate);
            orderItems.setCounter(orderItems.getCounter() + 1);
            orderItemsDAO.update(orderItems);
        } else {
            OrderItems orderItems = new OrderItems(itemId,
                    orderedItems.getItemName(), orderedItems.getDescription(), plate, orderedItems.getCategoryName()
                    , orderedItems.getPrice(), 1);
            orderItemsDAO.insert(orderItems);
        }
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_item_name)
        TextView itemName;
        @BindView(R.id.tv_plate)
        TextView plate;
        @BindView(R.id.tv_add_counter)
        TextView counter;
        @BindView(R.id.tv_price)
        TextView price;
        @BindView(R.id.tv_item_description)
        TextView itemDescription;
        @BindView(R.id.iv_add)
        ImageView iv_add;
        @BindView(R.id.iv_minus)
        ImageView iv_minus;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
