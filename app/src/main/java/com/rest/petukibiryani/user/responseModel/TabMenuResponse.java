package com.rest.petukibiryani.user.responseModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TabMenuResponse {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("sub_category_name")
        @Expose
        private String subCategoryName;
        @SerializedName("category_id")
        @Expose
        private String categoryId;
        @SerializedName("category_name")
        @Expose
        private String categoryName;
        @SerializedName("base_price")
        @Expose
        private String basePrice;
        @SerializedName("extra_price")
        @Expose
        private String extraPrice;
        @SerializedName("half_price")
        @Expose
        private String halfPrice;
        @SerializedName("full_price")
        @Expose
        private String fullPrice;
        @SerializedName("file")
        @Expose
        private Object file;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("is_extra")
        @Expose
        private String isExtra;
        @SerializedName("is_variant")
        @Expose
        private String isVariant;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("description")
        @Expose
        private String description;

        @SerializedName( "is_outof_stock")
        @Expose
        private String isOutOfStock;

        public String getIsOutOfStock() {
            return isOutOfStock;
        }

        public void setIsOutOfStock(String isOutOfStock) {
            this.isOutOfStock = isOutOfStock;
        }

        public String getType() {
            return type;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getExtraPrice() {
            return extraPrice;
        }

        public void setExtraPrice(String extraPrice) {
            this.extraPrice = extraPrice;
        }

        public String getHalfPrice() {
            return halfPrice;
        }

        public void setHalfPrice(String halfPrice) {
            this.halfPrice = halfPrice;
        }

        public String getFullPrice() {
            return fullPrice;
        }

        public void setFullPrice(String fullPrice) {
            this.fullPrice = fullPrice;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
        }

        public Object getFile() {
            return file;
        }

        public void setFile(Object file) {
            this.file = file;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }


        public String getSubCategoryName() {
            return subCategoryName;
        }

        public void setSubCategoryName(String subCategoryName) {
            this.subCategoryName = subCategoryName;
        }

        public String getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(String categoryId) {
            this.categoryId = categoryId;
        }

        public String getBasePrice() {
            return basePrice;
        }

        public void setBasePrice(String basePrice) {
            this.basePrice = basePrice;
        }

        public String getIsExtra() {
            return isExtra;
        }

        public void setIsExtra(String isExtra) {
            this.isExtra = isExtra;
        }

        public String getIsVariant() {
            return isVariant;
        }

        public void setIsVariant(String isVariant) {
            this.isVariant = isVariant;
        }
    }
}
