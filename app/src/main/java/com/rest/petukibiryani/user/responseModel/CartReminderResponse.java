package com.rest.petukibiryani.user.responseModel;

public class CartReminderResponse {


    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
