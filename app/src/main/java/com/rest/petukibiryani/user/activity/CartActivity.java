
package com.rest.petukibiryani.user.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rest.petukibiryani.user.R;
import com.rest.petukibiryani.user.adapter.OrderCartAdapter;
import com.rest.petukibiryani.user.helper.AppDatabases;
import com.rest.petukibiryani.user.helper.AppPreferences;
import com.rest.petukibiryani.user.listeners.BillNotify;
import com.rest.petukibiryani.user.listeners.OrderItemsDAO;
import com.rest.petukibiryani.user.models.OrderItems;
import com.rest.petukibiryani.user.models.OrderItemsList;
import com.rest.petukibiryani.user.network.ApiInterface;
import com.rest.petukibiryani.user.network.ServiceGenerator;
import com.rest.petukibiryani.user.responseModel.OffersLoyaltyPoints;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("SimpleDateFormat")
public class CartActivity extends BaseActivity implements BillNotify {

    @BindView(R.id.pkb_wallet)
    TextView tv_pkbWallet;
    @BindView(R.id.tv_subtotal)
    TextView tv_subtotal;
    @BindView(R.id.tv_total_bill)
    TextView tv_totalBill;
    @BindView(R.id.tv_note)
    TextView tv_note;
    @BindView(R.id.tv_loyal_points)
    TextView tv_loyalty_pts;
    @BindView(R.id.ll_cart)
    LinearLayout ll_cart;
    @BindView(R.id.ll_pkb_wallet)
    LinearLayout ll_pkb_wallet;
    @BindView(R.id.cb_pkb_wallet)
    CheckBox checkBox;
    @BindView(R.id.bt_checkout)
    Button checkout;
    @BindView(R.id.view_pkb)
    View viewPKB;
    @BindView(R.id.ll_texes_and_charges)
    LinearLayout ll_texes_and_charges;
    @BindView(R.id.tv_texes_and_charges)
    TextView tv_texes_and_charges;
    @BindView(R.id.special_note)
    EditText specialNote;
    @BindView(R.id.cb_pick_up)
    CheckBox pick_up;

    private ArrayList<OrderItemsList> orderItemsLists = new ArrayList<>();
    private int pkbWallet,maxRedeemPoints,minimumOrderValue,usedPoints;
    float subtotal = 0, cgst = 0, totalBill = 0, total = 0;
    private OrderItemsDAO orderItemsDAO;
    private float value = 0;
    private OffersLoyaltyPoints offersResponse;
    private List<OffersLoyaltyPoints.Datum> offersLoyaltyPoints = new ArrayList<>();
    private List<OrderItems> orderedItems;
    private String packagingCharges;


    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_cart);

        init();

        checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                pkbWallet = getBonus();
                cartNotify(calculateSubTotal());
            } else {
                pkbWallet = 0;
                cartNotify(calculateSubTotal());
            }
        });

        /*if (!AppPreferences.getSharedPreferencesForAddress(getApplicationContext()).getBoolean("withKms", false)) {
            checkout.setClickable(false);
            checkout.setBackgroundColor(getResources().getColor(R.color.color_grey));
            tv_note.setVisibility(View.VISIBLE);
            tv_note.setText(R.string.you_are_not_in_7kms);
        } else {*/
        checkout.setOnClickListener(v -> {
            AppPreferences.setPickUp(this, pick_up.isChecked());
            if (checkForTime()) {
                alertNoOrder();
            } else if (calculateSubTotal() < minimumOrderValue && checkForBeverages()) {
                tv_note.setVisibility(View.VISIBLE);
                tv_note.setText(getResources().getString(R.string.note)+minimumOrderValue);
                tv_note.append("\n" + getResources().getString(R.string.all_beverages));

            } else if (calculateSubTotal() < minimumOrderValue) {
                tv_note.setVisibility(View.VISIBLE);
                tv_note.setText(getResources().getString(R.string.note)+minimumOrderValue);
            } else if (checkForBeverages()) {
                tv_note.setVisibility(View.VISIBLE);
                tv_note.setText(getResources().getString(R.string.all_beverages));
            } else {
                tv_note.setVisibility(View.GONE);
                Bundle bundle = new Bundle();
                bundle.putString("cgst", String.valueOf(cgst));
                bundle.putString("sgst", String.valueOf(cgst));
                if (checkBox.isChecked()) bundle.putString("points", String.valueOf(usedPoints));
                else bundle.putString("points", "0");
                bundle.putString("totalPrice", String.valueOf(total));
                bundle.putString("payablePrice", String.valueOf(subtotal));
                bundle.putString("grandTotal", String.valueOf(totalBill));
                bundle.putString("special_note", specialNote.getText().toString());
                bundle.putString("packaging_charges", packagingCharges);
                switchActivity(CheckoutActivity.class, bundle);
            }
        });
        if (orderedItems.size() > 0)
            findViewById(R.id.ll_your_empty_cart).setVisibility(View.GONE);
    }


    /***
     * Initializing all the views
     */
    private void init() {
        ButterKnife.bind(this);
        minimumOrderValue = 250;
        initializeToolbar(getResources().getString(R.string.my_cart), R.drawable.close_no_order);
        updateUserDetails();
        orderItemsDAO = AppDatabases.getOrderItemsDAO(getApplicationContext());
        orderedItems = orderItemsDAO.getOrderedItems();
        for (OrderItems oI : orderedItems)
            orderItemsLists.add(new OrderItemsList(oI.getItemId(), oI.getItemName(), oI.getPlate(), oI.getDescription(), oI.getCategoryName(), oI.getPrice(), oI.getCounter()));

        RecyclerView rv = findViewById(R.id.rv_mycart);
        rv.setLayoutManager(new LinearLayoutManager(this));
        OrderCartAdapter orderCartAdapter = new OrderCartAdapter(this, orderItemsLists, this);
        rv.setAdapter(orderCartAdapter);
        tv_loyalty_pts.setVisibility(View.GONE);

        ll_texes_and_charges.setOnClickListener(v -> viewTexesAndCharges());
    }

    private void viewTexesAndCharges() {

        Dialog dialog = new Dialog(CartActivity.this);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.texes_n_charges);
        TextView dialog_tv_cgst = dialog.findViewById(R.id.tv_cgst);
        TextView dialog_tv_sgst = dialog.findViewById(R.id.tv_sgst);
        TextView dialog_tv_delivery_charge = dialog.findViewById(R.id.tv_delivery_charge);
        TextView dialog_tv_packagingCharge = dialog.findViewById(R.id.tv_packagingCharge);

        dialog_tv_packagingCharge.setText(("₹ " + packagingCharges));
        dialog_tv_cgst.setText(("₹ " + cgst));
        dialog_tv_sgst.setText(("₹ " + cgst));
        dialog_tv_delivery_charge.setText(("₹ " + 0));


        dialog.findViewById(R.id.iv_close).setOnClickListener(v -> dialog.dismiss());
        dialog.show();
    }

    /*
     *
     * @return true if current time is in between restaurant timings, otherwise false
     */
    private boolean checkForTime() {

        Calendar cal = Calendar.getInstance();
        Date date = cal.getTime();
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        String time = dateFormat.format(date);
        String hr_min[] = time.split(":");
        return (Integer.parseInt(hr_min[0]) < 11) || (Integer.parseInt(hr_min[0]) == 11 && Integer.parseInt(hr_min[1]) < 30) ||
                (Integer.parseInt(hr_min[0]) == 22 && Integer.parseInt(hr_min[1]) > 30) ||
                (Integer.parseInt(hr_min[0]) == 23);
    }

    /*
     * Display dialog box showing restaurant timings
     */
    private void alertNoOrder() {
        final Dialog dialog = new Dialog(CartActivity.this);
        dialog.setContentView(R.layout.layout_no_order);
        dialog.setCancelable(false);
        dialog.findViewById(R.id.close_no_order).setOnClickListener(v -> dialog.dismiss());
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
    }

    /***
     *
     * @return the bonus points of pkbWallet
     */
    private int getBonus() {
        return Integer.parseInt(Objects.requireNonNull(AppPreferences.getBonusPts(this)));
    }


    /***
     * @return true if all the orders in the cart contains beverages only, otherwise false
     */
    private boolean checkForBeverages() {
        List<OrderItems> orderItems = AppDatabases.getOrderItemsDAO(getApplicationContext()).getOrderedItems();

        for (OrderItems oI : orderItems) {
            if (!oI.getCategoryName().equalsIgnoreCase("BEVERAGES"))
                return false;
        }

        return true;
    }


    /***
     * If database is not empty ,cart layout will be visible ,otherwise ,cart layout will not be visible
     * @param subTotalOfItems contains subtotal price of the orders
     */
    @Override
    public void cartNotify(float subTotalOfItems) {

        subtotal = subTotalOfItems;
        total = subTotalOfItems;
        cgst = 0;
        totalBill = 0;

        if (orderItemsDAO.getTotalOrderItems() != 0) {
            ll_cart.setVisibility(View.VISIBLE);

            checkForPKBWallet(subtotal);
        } else {
            ll_cart.setVisibility(View.GONE);
            findViewById(R.id.ll_your_empty_cart).setVisibility(View.VISIBLE);
        }

    }


    /***
     * Calculating subtotal price
     * @return subtotal price
     */
    private float calculateSubTotal() {

        List<OrderItems> orderItems = orderItemsDAO.getOrderedItems();
        float subTotal = 0;
        for (OrderItems oI : orderItems) {
            subTotal += (oI.getPrice() * oI.getCounter());
        }
        return subTotal;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isCheck();
        if (getBonus() == 0)
            ll_pkb_wallet.setVisibility(View.GONE);

        tv_pkbWallet.setText(("₹ " + pkbWallet));
        cartNotify(calculateSubTotal());
    }

    /***
     * Calculating and setting values of subtotal,cgst,sgst and totalBill
     * @param tempTotal contains initial subtotal price
     */
    private void checkForPKBWallet(float tempTotal) {
        isCheck();
        if(!checkBox.isChecked()) tv_pkbWallet.setText(("₹ " + getBonus()));
        else{
            if(pkbWallet - maxRedeemPoints < 0) { // max redeem is more than pkb wallet
                subtotal = tempTotal - pkbWallet;
                tv_pkbWallet.setText(("₹ 0"));
                usedPoints = pkbWallet;
            }
            else{
                subtotal = tempTotal - maxRedeemPoints;
                tv_pkbWallet.setText(("₹ "+(pkbWallet - maxRedeemPoints)));
                usedPoints = maxRedeemPoints;
            }

        }
        /*if (subtotal < 0) {
            pkbWallet = (int) tempTotal;
            subtotal = 0;
            viewPKB.setVisibility(View.GONE);
        } else {
            viewPKB.setVisibility(View.VISIBLE);
            pkbWallet = getBonus();
        }*/
        tv_subtotal.setText(("₹ " + subtotal));

        getOfferLoyaltyPts(subtotal);

        cgst = (float) ((2.5 * subtotal) / 100);

        float total_tnc = cgst + cgst;
        tv_texes_and_charges.setText(("₹ " + total_tnc));
        totalBill = subtotal + (2 * cgst);
        tv_totalBill.setText(("₹ " + totalBill));
        if (totalBill <= 300) {
            packagingCharges = String.valueOf(10);
        } else if (totalBill < 600) {
            packagingCharges = String.valueOf(20);
        } else if (totalBill >= 600) {
            packagingCharges = String.valueOf(30);
        } else {
            packagingCharges = String.valueOf(0);
        }
    }

    /***
     * Setting pkbWallet according to the checkbox
     */
    private void isCheck() {
        if (checkBox.isChecked()) pkbWallet = getBonus();
        else pkbWallet = 0;
    }


    /***
     * Fetching the percentage of loyalty points that will be given on the order from Api and setting it
     * @param subtotal contains subtotal price
     */
    private void getOfferLoyaltyPts(float subtotal) {
        value = 0;
        offersLoyaltyPoints.clear();
        Calendar cal = Calendar.getInstance();
        Date currentTime = cal.getTime();
        DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        String time = timeFormat.format(currentTime);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String date = dateFormat.format(currentTime);


        ApiInterface apiInterface = ServiceGenerator.createLoginRequest(ApiInterface.class);
        Call<OffersLoyaltyPoints> call = apiInterface.getOffer(date, time);
        call.enqueue(new Callback<OffersLoyaltyPoints>() {
            @Override
            public void onResponse(@NonNull Call<OffersLoyaltyPoints> call, @NonNull Response<OffersLoyaltyPoints> response) {
                offersResponse = response.body();
                if (offersResponse != null) {
                    if (offersResponse.getData() != null && Objects.requireNonNull(offersResponse).getStatus()) {
                        if (offersResponse.getData().size() > 0) {
                            offersLoyaltyPoints.addAll(offersResponse.getData());
                            String tempVal = offersLoyaltyPoints.get(0).getMinimumOrderValue();
                            if(tempVal!=null) minimumOrderValue = Integer.parseInt(tempVal);
                            tempVal = offersLoyaltyPoints.get(0).getMaxRedeem();
                            if(tempVal!=null) maxRedeemPoints = Integer.parseInt(tempVal);
                            tv_loyalty_pts.setVisibility(View.VISIBLE);
                            value = Float.parseFloat(offersLoyaltyPoints.get(0).getOfferValue());
                            if(value == 0)
                                tv_loyalty_pts.setVisibility(View.GONE);
                            else {
                                value = value / 100;
                                double loyalty_pts = value * subtotal;
                                String end = String.valueOf((int) loyalty_pts);
                                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder("Complete this order to get " + String.valueOf((int) loyalty_pts) + " points");
                                spannableStringBuilder.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorAccent)), 27, end.length() + 27, 0);
                                tv_loyalty_pts.setText(spannableStringBuilder);
                            }
                        } else {
                            tv_loyalty_pts.setVisibility(View.GONE);
                        }
                    } else {
                        tv_loyalty_pts.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<OffersLoyaltyPoints> call, @NonNull Throwable t) {
                displayToast(R.string.some_thing_wrong);
            }
        });


    }



}
