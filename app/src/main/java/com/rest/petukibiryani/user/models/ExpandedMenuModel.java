package com.rest.petukibiryani.user.models;

public class ExpandedMenuModel {

    private String iconName = "";
    private int iconImg; // menu icon resource id

    public ExpandedMenuModel(String iconName, int iconImg) {
        this.iconName = iconName;
        this.iconImg = iconImg;

    }

    public String getIconName() {
        return iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    public int getIconImg() {
        return iconImg;
    }

    public void setIconImg(int iconImg) {
        this.iconImg = iconImg;
    }
}
