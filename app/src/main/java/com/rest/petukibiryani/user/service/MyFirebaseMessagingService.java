package com.rest.petukibiryani.user.service;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.rest.petukibiryani.user.activity.MainActivity;
import com.rest.petukibiryani.user.activity.NotificationsActivity;
import com.rest.petukibiryani.user.helper.AppPreferences;
import com.rest.petukibiryani.user.models.FirebaseNotificationModel;
import com.rest.petukibiryani.user.R;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;


@SuppressLint("SimpleDateFormat")
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static DateFormat dateFormat = new SimpleDateFormat("d MMM yyyy");
    public static final String INTENT_FILTER = "INTENT_FILTER";

    @Override
    public void onNewToken(String s) {
        AppPreferences.saveRefreshFirebaseToken(this,s);
        Log.e("New Token", s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.d("FirebaseMessagService", "FROM :" + remoteMessage.getFrom());
        MainActivity.totalItems++;
        if (remoteMessage.getData().size() > 0) {
            Log.d("FirebaseMessagService", "Message data :" + remoteMessage.getData());
        }
        if (remoteMessage.getNotification() != null) {
        FirebaseNotificationModel firebaseNotificationModel = new FirebaseNotificationModel();

        firebaseNotificationModel.setbody(Objects.requireNonNull(remoteMessage.getNotification()).getBody());
        firebaseNotificationModel.setDate(getCurrentDate());

        String body = firebaseNotificationModel.getbody();
        String date = firebaseNotificationModel.getDate();

        Intent intent = new Intent(this,NotificationsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notifiBuilder = new NotificationCompat.Builder(this,"default")
                .setSmallIcon(R.mipmap.pkb)
                .setContentText(remoteMessage.getNotification().getTitle())
                .setContentText(body)
                .setAutoCancel(true)
                .setSound(notification)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notifiBuilder.build());
            Log.d("FirebaseMessagService", "Message body :" + remoteMessage.getNotification().getBody());
            Intent broadcast = new Intent(INTENT_FILTER);
            broadcast.putExtra("Body", body);
            broadcast.putExtra("Date", date);
            sendBroadcast(broadcast);

        }

    }



    public static String getCurrentDate() {
        Date today = Calendar.getInstance().getTime();
        return dateFormat.format(today);
    }
}