package com.rest.petukibiryani.user.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rest.petukibiryani.user.helper.AppPreferences;
import com.rest.petukibiryani.user.views.GrayScaleTransformation;
import com.squareup.picasso.Picasso;
import com.rest.petukibiryani.user.R;
import com.rest.petukibiryani.user.listeners.MyClickListener;
import com.rest.petukibiryani.user.network.ServiceGenerator;
import com.rest.petukibiryani.user.responseModel.GridMenuResponse;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GridMenuAdapter extends RecyclerView.Adapter<GridMenuAdapter.GridViewHolder> {

    private Context mCtx;
    private ArrayList<GridMenuResponse.Datum> gridMenuItems;
    private MyClickListener listener;

    public GridMenuAdapter(Context mCtx, ArrayList<GridMenuResponse.Datum> gridMenuItemList, MyClickListener myCilckListener) {
        this.mCtx = mCtx;
        this.listener = myCilckListener;
        this.gridMenuItems = gridMenuItemList;
    }

    @NonNull
    @Override
    public GridViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_menu_layout, parent,false);
        return new GridViewHolder(layoutView);
    }


    @Override
    public void onBindViewHolder(@NonNull GridViewHolder holder, final int position) {

        holder.tv_item_name.setText(gridMenuItems.get(position).getCategoryName());
        holder.tv_item_details.setText(gridMenuItems.get(position).getDescription());

        if (AppPreferences.isRestOpen(mCtx))
            Picasso.with(mCtx)
                    .load(ServiceGenerator.BASE_URL + gridMenuItems.get(position).getFile())
                    .into(holder.iv_item_image);
        else
        {
            Picasso.with(mCtx)
                    .load(ServiceGenerator.BASE_URL + gridMenuItems.get(position).getFile())
                    .transform(new GrayScaleTransformation())
                    .into(holder.iv_item_image);
            holder.bt_order_now.setBackgroundColor(mCtx.getResources().getColor(R.color.color_gry));
            holder.bt_order_now.setTextColor(mCtx.getResources().getColor(R.color.color_white));
        }


        holder.l1.setOnClickListener(v -> listener.myOnClick(gridMenuItems.get(position).getId(), position));
    }

    @Override
    public int getItemCount() {
        return gridMenuItems.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    class GridViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.card_view)
        LinearLayout l1;
        @BindView(R.id.tv_item_name)
        TextView tv_item_name;
        @BindView(R.id.tv_item_details)
        TextView tv_item_details;
        @BindView(R.id.iv_item_image)
        ImageView iv_item_image;
        @BindView(R.id.bt_order_now)
        TextView bt_order_now;

        GridViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


    }




}
