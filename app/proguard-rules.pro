## Add project specific ProGuard rules here.
## You can control the set of applied configuration files using the
## proguardFiles setting in build.gradle.
##
## For more details, see
##   http://developer.android.com/guide/developing/tools/proguard.html
#
## If your project uses WebView with JS, uncomment the following
## and specify the fully qualified class name to the JavaScript interface
## class:
##-keepclassmembers class fqcn.of.javascript.interface.for.webview {
##   public *;
##}
#
## Uncomment this to preserve the line number information for
## debugging stack traces.
##-keepattributes SourceFile,LineNumberTable
#
## If you keep the line number information, uncomment this to
## hide the original source file name.
##-renamesourcefileattribute SourceFile
#
##-ignorewarnings
##-dontwarn com.squareup.okhttp.**
##-dontwarn okhttp3.**
##-dontwarn okio.**
##-dontwarn ss.com.bannerslider.**
#
#
##for retrofit progaurd warnings
#-keep class com.squareup.** { *; }
#-keep interface com.squareup.** { *; }
#-keep class retrofit2.** { *; }
#-keep interface retrofit2.** { *;}
#-keep interface com.squareup.** { *; }
#
#
#-keepclasseswithmembers class * {
#    @retrofit2.http.* <methods>;
#}
#
#
#-dontwarn rx.**
#-dontwarn retrofit2.**
#-dontwarn okhttp3.**
#-dontwarn okio.**
#
#-keep class com.squareup.okhttp.** { *; }
#-keep interface com.squareup.okhttp.** { *; }
#
#-dontwarn com.squareup.okhttp.**
#-dontwarn okio.*
#
#-keepattributes Signature
#-keepattributes *Annotation*
#-keep class okhttp3.** { *; }
#-keep interface okhttp3.** { *; }
#
#-dontwarn okhttp3.**
#
## end here
#
##for picasso proguard warnings
#
#-dontwarn com.squareup.okhttp.**
#-keep class com.squareup.okhttp.* { *;}
#-keep class com.squareup.okhttp.** { *;}
#-keep class com.squareup.okhttp.*** { *;}
#
#
#####
#-dontwarn okhttp3.internal.platform.*
#-keep class okhttp3.internal.platform.* { *;}
#-keep class okhttp3.internal.platform.** { *;}
#-keep class okhttp3.internal.platform.*** { *;}
#
##for slidere adaper progaurd warnings
#
#-dontwarn ss.com.bannerslider.adapters.*
#
#-keep class ss.com.bannerslider.adapters.* { *;}
#-keep class ss.com.bannerslider.adapters.** { *;}
#-keep class ss.com.bannerslider.adapters.*** { *;}
